import { __spread, __extends } from 'tslib';
import { Component, EventEmitter, Input, Output, NgModule } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { BehaviorSubject } from 'rxjs';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlatDataSource, MatTreeFlattener, MatBadgeModule, MatButtonModule, MatCheckboxModule, MatIconModule, MatProgressBarModule, MatTreeModule } from '@angular/material';
import { Options, Ng5SliderModule } from 'ng5-slider';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * Bucket response for a given Elasticsearch aggregation.
 */
var  /**
 * Bucket response for a given Elasticsearch aggregation.
 */
EsBucket = /** @class */ (function () {
    function EsBucket() {
        this.key = '';
        this.count = 0;
        this.proportion = 0;
        this.buckets = [];
    }
    /**
     * Initialization of the bucket from a backend response.
     * Used to keep the functions of this class.
     * @param bucket Response from backend used for initialization.
     */
    /**
     * Initialization of the bucket from a backend response.
     * Used to keep the functions of this class.
     * @param {?} bucket Response from backend used for initialization.
     * @return {?}
     */
    EsBucket.prototype.initBucket = /**
     * Initialization of the bucket from a backend response.
     * Used to keep the functions of this class.
     * @param {?} bucket Response from backend used for initialization.
     * @return {?}
     */
    function (bucket) {
        this.key = bucket.key;
        this.count = bucket.count;
        this.proportion = bucket.proportion;
        this.buckets = bucket.buckets.map(function (item) {
            /** @type {?} */
            var result = new EsBucket();
            result.initBucket(item);
            return result;
        });
    };
    /**
     * Update counts for buckets and all sub-buckets given a bucket from backend response.
     * Used to keep the functions of this class.
     * @param bucket Response from backend used for count update.
     * @param maxDocCount Maximum Doc Count for the current bucket level, used to compute proportion
     */
    /**
     * Update counts for buckets and all sub-buckets given a bucket from backend response.
     * Used to keep the functions of this class.
     * @param {?} bucket Response from backend used for count update.
     * @param {?} maxDocCount Maximum Doc Count for the current bucket level, used to compute proportion
     * @return {?}
     */
    EsBucket.prototype.updateBucketCounts = /**
     * Update counts for buckets and all sub-buckets given a bucket from backend response.
     * Used to keep the functions of this class.
     * @param {?} bucket Response from backend used for count update.
     * @param {?} maxDocCount Maximum Doc Count for the current bucket level, used to compute proportion
     * @return {?}
     */
    function (bucket, maxDocCount) {
        var _this = this;
        if (bucket === undefined) {
            this.count = 0;
            this.proportion = 0;
            this.buckets.forEach(function (sub) { return sub.updateBucketCounts(undefined, 0); });
        }
        else {
            this.count = bucket.count;
            this.proportion = bucket.count / maxDocCount;
            this.buckets.forEach(function (sub) {
                /** @type {?} */
                var subBucketsmaxDocCount = Math.max.apply(null, bucket.buckets.map(function (item) { return item.count; }));
                /** @type {?} */
                var matching = bucket.buckets.filter(function (item) { return item.key === sub.key; });
                if (matching.length <= 1) {
                    sub.updateBucketCounts(matching[0], subBucketsmaxDocCount);
                }
                else {
                    console.error('Multiple buckets found for key ' + _this.key);
                }
            });
        }
    };
    return EsBucket;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var EsBucketAggregation = /** @class */ (function () {
    function EsBucketAggregation() {
        /**
         * List of buckets
         */
        this.buckets = [];
    }
    /**
     * @param {?} aggregation
     * @return {?}
     */
    EsBucketAggregation.prototype.initAggregation = /**
     * @param {?} aggregation
     * @return {?}
     */
    function (aggregation) {
        this.buckets = aggregation.buckets.map(function (item) {
            /** @type {?} */
            var result = new EsBucket();
            result.initBucket(item);
            return result;
        });
    };
    /**
     * @param {?} aggregation
     * @return {?}
     */
    EsBucketAggregation.prototype.updateAggregationCounts = /**
     * @param {?} aggregation
     * @return {?}
     */
    function (aggregation) {
        this.buckets.forEach(function (bucket) {
            /** @type {?} */
            var maxDocCount = Math.max.apply(null, aggregation.buckets.map(function (item) { return item.count; }));
            /** @type {?} */
            var matching = aggregation.buckets.filter(function (item) { return bucket.key === item.key; });
            if (matching.length <= 1) {
                bucket.updateBucketCounts(matching[0], maxDocCount);
            }
            else {
                console.error('Multiple Elasticsearch buckets found!');
            }
        });
    };
    return EsBucketAggregation;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * Categorized filter used to build Elasticsearch query.
 * The key is associated with a level and optional parents in case of optional parents.
 */
var  /**
 * Categorized filter used to build Elasticsearch query.
 * The key is associated with a level and optional parents in case of optional parents.
 */
CategorizedFilter = /** @class */ (function () {
    function CategorizedFilter(esBucketFlatNode) {
        /**
         * Optional parents filters if this filter is a leaf.
         */
        this.parents = [];
        this.key = esBucketFlatNode.key;
        this.level = esBucketFlatNode.level;
    }
    return CategorizedFilter;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * Extension of ES bucket to flat node.
 */
var  /**
 * Extension of ES bucket to flat node.
 */
EsBucketFlatNode = /** @class */ (function (_super) {
    __extends(EsBucketFlatNode, _super);
    function EsBucketFlatNode() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return EsBucketFlatNode;
}(EsBucket));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var BootstartChecklistFilterComponent = /** @class */ (function () {
    function BootstartChecklistFilterComponent() {
        var _this = this;
        /**
         * Categorized filter emitter
         */
        this.onChange = new EventEmitter();
        /**
         * Selection for checklist
         */
        this.checklistSelection = new SelectionModel(true);
        /**
         * Selection for emitter
         */
        this.emitterSelection = new SelectionModel(true);
        /**
         * Elements discarded from selection (e.g. children of a node if all children are checked)
         */
        this.discardedEmitterSelection = new SelectionModel(true);
        /**
         * Map from flat node to nested node. This helps us finding the nested node to be modified
         */
        this.flatNodeMap = new Map();
        /**
         * Map from nested node to flattened node. This helps us to keep the same object for selection
         */
        this.nestedNodeMap = new Map();
        this.dataChange = new BehaviorSubject([]);
        this.getLevel = function (node) { return node.level; };
        this.isExpandable = function (node) { return node.buckets.length > 0; };
        this.getChildren = function (node) { return node.buckets; };
        this.hasChild = function (_, _nodeData) { return _nodeData.buckets.length > 0; };
        this.treeFlattener = new MatTreeFlattener(this._transformer, this.getLevel, this.isExpandable, this.getChildren);
        this.treeControl = new FlatTreeControl(this.getLevel, this.isExpandable);
        this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
        this.dataChange.subscribe(function (data) { return _this.dataSource.data = data; });
    }
    /**
     * @return {?}
     */
    BootstartChecklistFilterComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        if (this.translationPrefix === undefined) {
            this.translationPrefix = '';
        }
    };
    /**
     * @return {?}
     */
    BootstartChecklistFilterComponent.prototype.ngDoCheck = /**
     * @return {?}
     */
    function () {
        this.dataChange.next(this.aggregation.buckets);
    };
    /** Whether all the descendants of the node are selected */
    /**
     * Whether all the descendants of the node are selected
     * @param {?} node
     * @return {?}
     */
    BootstartChecklistFilterComponent.prototype.descendantsAllSelected = /**
     * Whether all the descendants of the node are selected
     * @param {?} node
     * @return {?}
     */
    function (node) {
        var _this = this;
        /** @type {?} */
        var descendants = this.treeControl.getDescendants(node);
        if (descendants.length === 0) {
            return false;
        }
        return descendants.every(function (child) { return _this.checklistSelection.isSelected(child); });
    };
    /** Whether part of descendants are selected */
    /**
     * Whether part of descendants are selected
     * @param {?} node
     * @return {?}
     */
    BootstartChecklistFilterComponent.prototype.descendantsPartiallySelected = /**
     * Whether part of descendants are selected
     * @param {?} node
     * @return {?}
     */
    function (node) {
        var _this = this;
        /** @type {?} */
        var descendants = this.treeControl.getDescendants(node);
        /** @type {?} */
        var result = descendants.some(function (child) { return _this.checklistSelection.isSelected(child); });
        return result && !this.descendantsAllSelected(node);
    };
    /** Toggle the Elasticsearch bucket node item. Select/deselect all the descendants node */
    /**
     * Toggle the Elasticsearch bucket node item. Select/deselect all the descendants node
     * @param {?} node
     * @return {?}
     */
    BootstartChecklistFilterComponent.prototype.bucketSelectionToggle = /**
     * Toggle the Elasticsearch bucket node item. Select/deselect all the descendants node
     * @param {?} node
     * @return {?}
     */
    function (node) {
        this.checklistSelection.toggle(node);
        /** @type {?} */
        var descendants = this.treeControl.getDescendants(node);
        this.checklistSelection.isSelected(node)
            ? (_a = this.checklistSelection).select.apply(_a, __spread(descendants)) : (_b = this.checklistSelection).deselect.apply(_b, __spread(descendants));
        this._checkAllParentsSelection(node);
        this._emitChange();
        var _a, _b;
    };
    /** Toggle a leaf Elasticsearch bucket item selection. Check all the parents to see if they changed */
    /**
     * Toggle a leaf Elasticsearch bucket item selection. Check all the parents to see if they changed
     * @param {?} node
     * @return {?}
     */
    BootstartChecklistFilterComponent.prototype.bucketLeafSelectionToggle = /**
     * Toggle a leaf Elasticsearch bucket item selection. Check all the parents to see if they changed
     * @param {?} node
     * @return {?}
     */
    function (node) {
        this.checklistSelection.toggle(node);
        this._checkAllParentsSelection(node);
        this._emitChange();
    };
    /**
     * @return {?}
     */
    BootstartChecklistFilterComponent.prototype._emitChange = /**
     * @return {?}
     */
    function () {
        this.emitterSelection.clear();
        this.discardedEmitterSelection.clear();
        this._addToEmitSelector(this.checklistSelection.selected);
        this.onChange.emit(this.emitterSelection.selected);
    };
    /**
     * @param {?} buckets
     * @return {?}
     */
    BootstartChecklistFilterComponent.prototype._addToEmitSelector = /**
     * @param {?} buckets
     * @return {?}
     */
    function (buckets) {
        var _this = this;
        buckets.forEach(function (element) {
            /** @type {?} */
            var flatElement = /** @type {?} */ (element);
            if (_this.checklistSelection.isSelected(element)) {
                if (_this.descendantsAllSelected(flatElement)) {
                    /** @type {?} */
                    var descendants = _this.treeControl.getDescendants(flatElement);
                    (_a = _this.discardedEmitterSelection).select.apply(_a, __spread(descendants));
                }
                /** @type {?} */
                var parent_1 = _this._getParentNode(flatElement);
                while (parent_1) {
                    _this._addToEmitSelector([parent_1]);
                    parent_1 = _this._getParentNode(parent_1);
                }
                if (!_this.discardedEmitterSelection.isSelected(element)) {
                    /** @type {?} */
                    var filter = new CategorizedFilter(flatElement);
                    /** @type {?} */
                    var eltParent = _this._getParentNode(flatElement);
                    while (eltParent) {
                        filter.parents.push(new CategorizedFilter(eltParent));
                        eltParent = _this._getParentNode(eltParent);
                    }
                    _this.emitterSelection.select(filter);
                    _this.discardedEmitterSelection.select(element);
                }
            }
            var _a;
        });
    };
    /**
     * Transformer to convert nested node to flat node. Record the nodes in maps for later use.
     * @param {?} node
     * @param {?} level
     * @return {?}
     */
    BootstartChecklistFilterComponent.prototype._transformer = /**
     * Transformer to convert nested node to flat node. Record the nodes in maps for later use.
     * @param {?} node
     * @param {?} level
     * @return {?}
     */
    function (node, level) {
        if (!this.nestedNodeMap) {
            this.nestedNodeMap = new Map();
        }
        if (!this.flatNodeMap) {
            this.flatNodeMap = new Map();
        }
        /** @type {?} */
        var existingNode = this.nestedNodeMap.get(node);
        /** @type {?} */
        var flatNode = existingNode && existingNode.key === node.key
            ? existingNode
            : new EsBucketFlatNode();
        flatNode.key = node.key;
        flatNode.count = node.count;
        flatNode.buckets = node.buckets;
        flatNode.proportion = node.proportion;
        flatNode.level = level;
        this.flatNodeMap.set(flatNode, node);
        this.nestedNodeMap.set(node, flatNode);
        return flatNode;
    };
    /**
     * Checks all the parents when a node is selected/unselected
     * @param {?} node
     * @return {?}
     */
    BootstartChecklistFilterComponent.prototype._checkAllParentsSelection = /**
     * Checks all the parents when a node is selected/unselected
     * @param {?} node
     * @return {?}
     */
    function (node) {
        /** @type {?} */
        var parent = this._getParentNode(node);
        while (parent) {
            this._checkRootNodeSelection(parent);
            parent = this._getParentNode(parent);
        }
    };
    /**
     * Check root node checked state and change it accordingly
     * @param {?} node
     * @return {?}
     */
    BootstartChecklistFilterComponent.prototype._checkRootNodeSelection = /**
     * Check root node checked state and change it accordingly
     * @param {?} node
     * @return {?}
     */
    function (node) {
        /** @type {?} */
        var nodeSelected = this.checklistSelection.isSelected(node);
        if (nodeSelected && !this.descendantsAllSelected(node)) {
            this.checklistSelection.deselect(node);
        }
        else if (!nodeSelected && this.descendantsAllSelected(node)) {
            this.checklistSelection.select(node);
        }
    };
    /**
     * Get the parent node of a node
     * @param {?} node
     * @return {?}
     */
    BootstartChecklistFilterComponent.prototype._getParentNode = /**
     * Get the parent node of a node
     * @param {?} node
     * @return {?}
     */
    function (node) {
        /** @type {?} */
        var currentLevel = this.getLevel(node);
        if (currentLevel < 1) {
            return null;
        }
        /** @type {?} */
        var startIndex = this.treeControl.dataNodes.indexOf(node) - 1;
        for (var i = startIndex; i >= 0; i--) {
            /** @type {?} */
            var currentNode = this.treeControl.dataNodes[i];
            if (this.getLevel(currentNode) < currentLevel) {
                return currentNode;
            }
        }
        return null;
    };
    BootstartChecklistFilterComponent.decorators = [
        { type: Component, args: [{
                    selector: 'bootstart-checklist-filter',
                    template: "<mat-tree [dataSource]=\"dataSource\" [treeControl]=\"treeControl\" class=\"bootstart-checklist-tree\">\n\n  <!--Nested tree node-->\n  <mat-tree-node *matTreeNodeDef=\"let node; when: hasChild\"\n                 matTreeNodePadding\n                 class=\"row\"\n                 title=\"{{translationPrefix+node.key|translate}} ({{node.count}})\">\n\n    <div class=\"col-md-10 row\">\n\n      <mat-checkbox color=\"primary\"\n                    class=\"col-md-12\"\n                    [checked]=\"descendantsAllSelected(node)\"\n                    [indeterminate]=\"descendantsPartiallySelected(node)\"\n                    (change)=\"bucketSelectionToggle(node)\">\n\n        <div class=\"row\">\n          <div class=\"col-md-12 checkbox-label\"\n               [class.checkbox-label-zero-count]=\"node.count === 0\">\n            <span class=\"bucket-key\">{{translationPrefix+node.key|translate}}</span>\n            <span class=\"bucket-count\">&nbsp;&bull;&nbsp;{{node.count}}</span>\n          </div>\n        </div>\n\n      </mat-checkbox>\n\n      <mat-progress-bar class=\"col-md-12\"\n                        mode=\"determinate\"\n                        value=\"{{node.proportion*100 || 0}}\"></mat-progress-bar>\n\n    </div>\n\n    <button mat-icon-button matTreeNodeToggle class=\"col-md-2\">\n      <mat-icon class=\"mat-icon-rtl-mirror\">\n        {{treeControl.isExpanded(node) ? 'expand_more' : 'chevron_right'}}\n      </mat-icon>\n    </button>\n\n    <ul [class.checklist-tree-invisible]=\"!treeControl.isExpanded(node)\"\n        class=\"col-md-12 bootstart-tree-child-node\">\n      <ng-container matTreeNodeOutlet></ng-container>\n    </ul>\n\n  </mat-tree-node>\n\n\n  <!--Leaf node-->\n  <mat-tree-node *matTreeNodeDef=\"let node\"\n                 class=\"row\"\n                 matTreeNodeToggle matTreeNodePadding\n                 title=\"{{translationPrefix+node.key|translate}} ({{node.count}})\">\n\n    <div class=\"row col-md-12\">\n\n      <mat-checkbox color=\"primary\"\n                    class=\"col-md-12\"\n                    [checked]=\"checklistSelection.isSelected(node)\"\n                    (change)=\"bucketLeafSelectionToggle(node)\">\n\n        <div class=\"row\">\n          <div class=\"col-md-12 checkbox-label\"\n               [class.checkbox-label-zero-count]=\"node.count === 0\">\n            <span class=\"bucket-key\">{{translationPrefix+node.key|translate}}</span>\n            <span class=\"bucket-count\">&nbsp;&bull;&nbsp;{{node.count}}</span>\n          </div>\n        </div>\n\n      </mat-checkbox>\n\n      <mat-progress-bar class=\"col-md-12\"\n                        mode=\"determinate\"\n                        value=\"{{node.proportion*100 || 0}}\"></mat-progress-bar>\n\n    </div>\n\n  </mat-tree-node>\n\n</mat-tree>\n",
                    styles: [".bootstart-checklist-tree-invisible{display:none}.bootstart-checklist-tree{margin-top:15px}.bootstart-checklist-tree mat-tree-node{margin-top:-5px}.bootstart-checklist-tree mat-checkbox{cursor:pointer}.bootstart-checklist-tree button:focus{outline:0}.bootstart-checklist-tree mat-progress-bar{margin-top:-7px}.bootstart-tree-child-node{margin-bottom:-15px}.checkbox-label{white-space:initial!important}.bucket-key{font-size:small}.bucket-count{font-size:small;font-weight:700}.checkbox-label-zero-count span{color:gray!important}.checkbox-label-zero-count .bucket-count{font-size:small!important;font-weight:400!important}"]
                },] },
    ];
    /** @nocollapse */
    BootstartChecklistFilterComponent.ctorParameters = function () { return []; };
    BootstartChecklistFilterComponent.propDecorators = {
        aggregation: [{ type: Input }],
        translationPrefix: [{ type: Input }],
        onChange: [{ type: Output }]
    };
    return BootstartChecklistFilterComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var HistogramIntervalData = /** @class */ (function () {
    function HistogramIntervalData() {
    }
    return HistogramIntervalData;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var Interval = /** @class */ (function () {
    function Interval() {
    }
    return Interval;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var BootstartHistogramSliderFilterComponent = /** @class */ (function () {
    function BootstartHistogramSliderFilterComponent() {
        var _this = this;
        this.minRange = 1;
        this.dataChange = new BehaviorSubject([]);
        this.breakpoints = [];
        this.lowerBounded = true;
        this.upperBounded = true;
        this.histogramData = [];
        this.sliderOptions = new Options();
        this.sliderInitialized = false;
        this.rangeChange = new EventEmitter();
        this.dataSource = [];
        this.dataChange.subscribe(function (data) {
            if (!_this.sliderInitialized) {
                _this.dataSource = data;
                _this._init();
            }
        });
    }
    /**
     * @return {?}
     */
    BootstartHistogramSliderFilterComponent.prototype.ngDoCheck = /**
     * @return {?}
     */
    function () {
        this.dataChange.next(this.bucketAggregation.buckets);
    };
    /**
     * Emits range event when value changes.
     * If min or max is infinite, returns null.
     */
    /**
     * Emits range event when value changes.
     * If min or max is infinite, returns null.
     * @return {?}
     */
    BootstartHistogramSliderFilterComponent.prototype.valueChange = /**
     * Emits range event when value changes.
     * If min or max is infinite, returns null.
     * @return {?}
     */
    function () {
        /** @type {?} */
        var interval = new Interval();
        interval.min = (!this.lowerBounded && this.minSelected === this.sliderOptions.floor) ? null : this.breakpoints[this.minSelected];
        interval.max = (!this.upperBounded && this.maxSelected === this.sliderOptions.ceil) ? null : this.breakpoints[this.maxSelected];
        this.rangeChange.emit(interval);
    };
    /**
     * Initialization from bucket aggregation
     * @return {?}
     */
    BootstartHistogramSliderFilterComponent.prototype._init = /**
     * Initialization from bucket aggregation
     * @return {?}
     */
    function () {
        var _this = this;
        if (this.dataSource.length > 0) {
            for (var i = 0; i < this.dataSource.length; i++) {
                /** @type {?} */
                var bucket = this.dataSource[i];
                /** @type {?} */
                var bucketRangeValues = bucket.key.split('-');
                if (bucketRangeValues.length === 1) {
                    // Aggregation is an histogram
                    this._addBreakpointValue(-Infinity);
                    this._addBreakpointValue(+bucketRangeValues[0]);
                    this.lowerBounded = false;
                }
                else {
                    // Aggregation is a list of ranges
                    if (bucketRangeValues[0] === '*') {
                        this._addBreakpointValue(-Infinity);
                        this._addBreakpointValue(+bucketRangeValues[1]);
                        this.lowerBounded = false;
                    }
                    else if (bucketRangeValues[1] === '*') {
                        this._addBreakpointValue(+bucketRangeValues[0]);
                        this._addBreakpointValue(Infinity);
                        this.upperBounded = false;
                    }
                    else {
                        this._addBreakpointValue(+bucketRangeValues[0]);
                        this._addBreakpointValue(+bucketRangeValues[1]);
                    }
                }
                /** @type {?} */
                var breakpointsFiniteValues = this.breakpoints.filter(function (item) { return item !== -Infinity && item !== Infinity; });
                this.breakpointFiniteMinValue = Math.min.apply(null, breakpointsFiniteValues);
                this.breakpointFiniteMaxValue = Math.max.apply(null, breakpointsFiniteValues);
                this.histogramData.push({
                    x: i,
                    y: bucket.count
                });
            }
            this.minSelected = 0;
            this.sliderOptions.floor = 0;
            this.maxSelected = this.histogramData.length;
            this.sliderOptions.ceil = this.histogramData.length;
            this.sliderOptions.translate = function (value, label) {
                if (_this.breakpoints[value] === -Infinity) {
                    return '< ' + _this.breakpointFiniteMinValue;
                }
                if (_this.breakpoints[value] === Infinity) {
                    return '> ' + _this.breakpointFiniteMaxValue;
                }
                return _this.breakpoints[value] + '';
            };
            // Slider can only be initialized once.
            this.sliderInitialized = true;
        }
        this.sliderOptions.minRange = this.minRange;
        this.sliderOptions.animate = false;
        this.sliderOptions.noSwitching = true;
        this.sliderOptions.pushRange = true;
        this.sliderOptions.hideLimitLabels = true;
    };
    /**
     * Adds value to breakpoints array.
     *  Checks first if value is already registered.
     * @param {?} value
     * @return {?}
     */
    BootstartHistogramSliderFilterComponent.prototype._addBreakpointValue = /**
     * Adds value to breakpoints array.
     *  Checks first if value is already registered.
     * @param {?} value
     * @return {?}
     */
    function (value) {
        if (this.breakpoints.indexOf(value) === -1) {
            this.breakpoints.push(value);
        }
    };
    BootstartHistogramSliderFilterComponent.decorators = [
        { type: Component, args: [{
                    selector: 'bootstart-histogram-slider-filter',
                    template: "<div class=\"bootstart-range-slider\" *ngIf=\"sliderInitialized\">\n  <ng5-slider [(value)]=\"minSelected\"\n              [(highValue)]=\"maxSelected\"\n              [options]=\"sliderOptions\"\n              (valueChange)=\"valueChange()\"\n              (highValueChange)=\"valueChange()\"></ng5-slider>\n</div>\n",
                    styles: [""]
                },] },
    ];
    /** @nocollapse */
    BootstartHistogramSliderFilterComponent.ctorParameters = function () { return []; };
    BootstartHistogramSliderFilterComponent.propDecorators = {
        bucketAggregation: [{ type: Input }],
        minRange: [{ type: Input }],
        rangeChange: [{ type: Output }]
    };
    return BootstartHistogramSliderFilterComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var BootstartFilterModule = /** @class */ (function () {
    function BootstartFilterModule() {
    }
    BootstartFilterModule.decorators = [
        { type: NgModule, args: [{
                    imports: [
                        CommonModule,
                        MatTreeModule,
                        MatCheckboxModule,
                        MatProgressBarModule,
                        MatIconModule,
                        MatButtonModule,
                        MatBadgeModule,
                        TranslateModule,
                        Ng5SliderModule
                    ],
                    declarations: [
                        BootstartChecklistFilterComponent,
                        BootstartHistogramSliderFilterComponent
                    ],
                    exports: [
                        BootstartChecklistFilterComponent,
                        BootstartHistogramSliderFilterComponent
                    ]
                },] },
    ];
    return BootstartFilterModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */

export { BootstartFilterModule, EsBucketAggregation, EsBucket, EsBucketFlatNode, CategorizedFilter, HistogramIntervalData, Interval, BootstartChecklistFilterComponent as ɵa, BootstartHistogramSliderFilterComponent as ɵb };

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LWZpbHRlci5qcy5tYXAiLCJzb3VyY2VzIjpbIm5nOi8vYm9vdHN0YXJ0LWZpbHRlci9saWIvbW9kZWxzL2FnZ3JlZ2F0aW9uL2J1Y2tldC9lcy1idWNrZXQudHMiLCJuZzovL2Jvb3RzdGFydC1maWx0ZXIvbGliL21vZGVscy9hZ2dyZWdhdGlvbi9idWNrZXQvZXMtYnVja2V0LWFnZ3JlZ2F0aW9uLnRzIiwibmc6Ly9ib290c3RhcnQtZmlsdGVyL2xpYi9tb2RlbHMvZmlsdGVyL2NhdGVnb3JpemVkLWZpbHRlci50cyIsIm5nOi8vYm9vdHN0YXJ0LWZpbHRlci9saWIvbW9kZWxzL2FnZ3JlZ2F0aW9uL2J1Y2tldC9lcy1idWNrZXQtZmxhdC1ub2RlLnRzIiwibmc6Ly9ib290c3RhcnQtZmlsdGVyL2xpYi9jb21wb25lbnRzL2Jvb3RzdGFydC1jaGVja2xpc3QtZmlsdGVyL2Jvb3RzdGFydC1jaGVja2xpc3QtZmlsdGVyLmNvbXBvbmVudC50cyIsIm5nOi8vYm9vdHN0YXJ0LWZpbHRlci9saWIvbW9kZWxzL3V0aWxzL2hpc3RvZ3JhbS1pbnRlcnZhbC1kYXRhLnRzIiwibmc6Ly9ib290c3RhcnQtZmlsdGVyL2xpYi9tb2RlbHMvdXRpbHMvaW50ZXJ2YWwudHMiLCJuZzovL2Jvb3RzdGFydC1maWx0ZXIvbGliL2NvbXBvbmVudHMvYm9vdHN0YXJ0LWhpc3RvZ3JhbS1zbGlkZXItZmlsdGVyL2Jvb3RzdGFydC1oaXN0b2dyYW0tc2xpZGVyLWZpbHRlci5jb21wb25lbnQudHMiLCJuZzovL2Jvb3RzdGFydC1maWx0ZXIvbGliL2Jvb3RzdGFydC1maWx0ZXIubW9kdWxlLnRzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQnVja2V0IHJlc3BvbnNlIGZvciBhIGdpdmVuIEVsYXN0aWNzZWFyY2ggYWdncmVnYXRpb24uXG4gKi9cbmV4cG9ydCBjbGFzcyBFc0J1Y2tldCB7XG5cbiAgLyoqIEJ1Y2tldCBrZXkgKi9cbiAga2V5OiBzdHJpbmc7XG4gIC8qKiBEb2MgY291bnQgZm9yIHRoZSBidWNrZXQgKi9cbiAgY291bnQ6IG51bWJlcjtcbiAgLyoqIFByb3BvcnRpb24gdXNlZCBmb3IgcHJvZ3Jlc3MgYmFyICovXG4gIHByb3BvcnRpb246IG51bWJlcjtcbiAgLyoqIExpc3Qgb2Ygc3ViIGJ1Y2tldHMgKi9cbiAgYnVja2V0czogRXNCdWNrZXRbXTtcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgICB0aGlzLmtleSA9ICcnO1xuICAgIHRoaXMuY291bnQgPSAwO1xuICAgIHRoaXMucHJvcG9ydGlvbiA9IDA7XG4gICAgdGhpcy5idWNrZXRzID0gW107XG4gIH1cblxuXG4gIC8qKlxuICAgKiBJbml0aWFsaXphdGlvbiBvZiB0aGUgYnVja2V0IGZyb20gYSBiYWNrZW5kIHJlc3BvbnNlLlxuICAgKiBVc2VkIHRvIGtlZXAgdGhlIGZ1bmN0aW9ucyBvZiB0aGlzIGNsYXNzLlxuICAgKiBAcGFyYW0gYnVja2V0IFJlc3BvbnNlIGZyb20gYmFja2VuZCB1c2VkIGZvciBpbml0aWFsaXphdGlvbi5cbiAgICovXG4gIGluaXRCdWNrZXQoYnVja2V0OiBFc0J1Y2tldCk6IHZvaWQge1xuICAgIHRoaXMua2V5ID0gYnVja2V0LmtleTtcbiAgICB0aGlzLmNvdW50ID0gYnVja2V0LmNvdW50O1xuICAgIHRoaXMucHJvcG9ydGlvbiA9IGJ1Y2tldC5wcm9wb3J0aW9uO1xuICAgIHRoaXMuYnVja2V0cyA9IGJ1Y2tldC5idWNrZXRzLm1hcChpdGVtID0+IHtcbiAgICAgIGNvbnN0IHJlc3VsdCA9IG5ldyBFc0J1Y2tldCgpO1xuICAgICAgcmVzdWx0LmluaXRCdWNrZXQoaXRlbSk7XG4gICAgICByZXR1cm4gcmVzdWx0O1xuICAgIH0pO1xuICB9XG5cbiAgLyoqXG4gICAqIFVwZGF0ZSBjb3VudHMgZm9yIGJ1Y2tldHMgYW5kIGFsbCBzdWItYnVja2V0cyBnaXZlbiBhIGJ1Y2tldCBmcm9tIGJhY2tlbmQgcmVzcG9uc2UuXG4gICAqIFVzZWQgdG8ga2VlcCB0aGUgZnVuY3Rpb25zIG9mIHRoaXMgY2xhc3MuXG4gICAqIEBwYXJhbSBidWNrZXQgUmVzcG9uc2UgZnJvbSBiYWNrZW5kIHVzZWQgZm9yIGNvdW50IHVwZGF0ZS5cbiAgICogQHBhcmFtIG1heERvY0NvdW50IE1heGltdW0gRG9jIENvdW50IGZvciB0aGUgY3VycmVudCBidWNrZXQgbGV2ZWwsIHVzZWQgdG8gY29tcHV0ZSBwcm9wb3J0aW9uXG4gICAqL1xuICB1cGRhdGVCdWNrZXRDb3VudHMoYnVja2V0OiBFc0J1Y2tldCwgbWF4RG9jQ291bnQ6IG51bWJlcik6IHZvaWQge1xuICAgIGlmIChidWNrZXQgPT09IHVuZGVmaW5lZCkge1xuICAgICAgdGhpcy5jb3VudCA9IDA7XG4gICAgICB0aGlzLnByb3BvcnRpb24gPSAwO1xuICAgICAgdGhpcy5idWNrZXRzLmZvckVhY2goc3ViID0+IHN1Yi51cGRhdGVCdWNrZXRDb3VudHModW5kZWZpbmVkLCAwKSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuY291bnQgPSBidWNrZXQuY291bnQ7XG4gICAgICB0aGlzLnByb3BvcnRpb24gPSBidWNrZXQuY291bnQgLyBtYXhEb2NDb3VudDtcbiAgICAgIHRoaXMuYnVja2V0cy5mb3JFYWNoKHN1YiA9PiB7XG4gICAgICAgIGNvbnN0IHN1YkJ1Y2tldHNtYXhEb2NDb3VudCA9IE1hdGgubWF4LmFwcGx5KG51bGwsIGJ1Y2tldC5idWNrZXRzLm1hcChpdGVtID0+IGl0ZW0uY291bnQpKTtcbiAgICAgICAgY29uc3QgbWF0Y2hpbmcgPSBidWNrZXQuYnVja2V0cy5maWx0ZXIoaXRlbSA9PiBpdGVtLmtleSA9PT0gc3ViLmtleSk7XG4gICAgICAgIGlmIChtYXRjaGluZy5sZW5ndGggPD0gMSkge1xuICAgICAgICAgIHN1Yi51cGRhdGVCdWNrZXRDb3VudHMobWF0Y2hpbmdbMF0sIHN1YkJ1Y2tldHNtYXhEb2NDb3VudCk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgY29uc29sZS5lcnJvcignTXVsdGlwbGUgYnVja2V0cyBmb3VuZCBmb3Iga2V5ICcgKyB0aGlzLmtleSk7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH1cbiAgfVxuXG59XG4iLCJpbXBvcnQge0VzQnVja2V0fSBmcm9tICcuL2VzLWJ1Y2tldCc7XG5pbXBvcnQge0VzQWdncmVnYXRpb259IGZyb20gJy4uL2VzLWFnZ3JlZ2F0aW9uJztcbmltcG9ydCB7YX0gZnJvbSAnQGFuZ3VsYXIvY29yZS9zcmMvcmVuZGVyMyc7XG5cbmV4cG9ydCBjbGFzcyBFc0J1Y2tldEFnZ3JlZ2F0aW9uIGltcGxlbWVudHMgRXNBZ2dyZWdhdGlvbiB7XG5cbiAgLyoqIExpc3Qgb2YgYnVja2V0cyAqL1xuICBidWNrZXRzOiBFc0J1Y2tldFtdID0gW107XG5cblxuICBpbml0QWdncmVnYXRpb24oYWdncmVnYXRpb246IEVzQnVja2V0QWdncmVnYXRpb24pOiB2b2lkIHtcbiAgICB0aGlzLmJ1Y2tldHMgPSBhZ2dyZWdhdGlvbi5idWNrZXRzLm1hcChpdGVtID0+IHtcbiAgICAgIGNvbnN0IHJlc3VsdCA9IG5ldyBFc0J1Y2tldCgpO1xuICAgICAgcmVzdWx0LmluaXRCdWNrZXQoaXRlbSk7XG4gICAgICByZXR1cm4gcmVzdWx0O1xuICAgIH0pO1xuICB9XG5cbiAgdXBkYXRlQWdncmVnYXRpb25Db3VudHMoYWdncmVnYXRpb246IEVzQnVja2V0QWdncmVnYXRpb24pOiB2b2lkIHtcbiAgICB0aGlzLmJ1Y2tldHMuZm9yRWFjaChidWNrZXQgPT4ge1xuICAgICAgY29uc3QgbWF4RG9jQ291bnQgPSBNYXRoLm1heC5hcHBseShudWxsLCBhZ2dyZWdhdGlvbi5idWNrZXRzLm1hcChpdGVtID0+IGl0ZW0uY291bnQpKTtcbiAgICAgIGNvbnN0IG1hdGNoaW5nID0gYWdncmVnYXRpb24uYnVja2V0cy5maWx0ZXIoaXRlbSA9PiBidWNrZXQua2V5ID09PSBpdGVtLmtleSk7XG4gICAgICBpZiAobWF0Y2hpbmcubGVuZ3RoIDw9IDEpIHtcbiAgICAgICAgYnVja2V0LnVwZGF0ZUJ1Y2tldENvdW50cyhtYXRjaGluZ1swXSwgbWF4RG9jQ291bnQpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgY29uc29sZS5lcnJvcignTXVsdGlwbGUgRWxhc3RpY3NlYXJjaCBidWNrZXRzIGZvdW5kIScpO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbn1cbiIsImltcG9ydCB7RXNCdWNrZXRGbGF0Tm9kZX0gZnJvbSAnLi4vLi4vbW9kZWxzL2FnZ3JlZ2F0aW9uL2J1Y2tldC9lcy1idWNrZXQtZmxhdC1ub2RlJztcblxuLyoqXG4gKiBDYXRlZ29yaXplZCBmaWx0ZXIgdXNlZCB0byBidWlsZCBFbGFzdGljc2VhcmNoIHF1ZXJ5LlxuICogVGhlIGtleSBpcyBhc3NvY2lhdGVkIHdpdGggYSBsZXZlbCBhbmQgb3B0aW9uYWwgcGFyZW50cyBpbiBjYXNlIG9mIG9wdGlvbmFsIHBhcmVudHMuXG4gKi9cbmV4cG9ydCBjbGFzcyBDYXRlZ29yaXplZEZpbHRlciB7XG4gIC8qKiBUaGUga2V5IG9mIHRoZSBmaWx0ZXIuICovXG4gIGtleTogc3RyaW5nO1xuXG4gIC8qKiBMZXZlbCBvZiB0aGUgdHJlZSBmaWx0ZXIgaW4gY2hlY2tsaXN0IGNvbXBvbmVudC4gKi9cbiAgbGV2ZWw6IG51bWJlcjtcblxuICAvKiogT3B0aW9uYWwgcGFyZW50cyBmaWx0ZXJzIGlmIHRoaXMgZmlsdGVyIGlzIGEgbGVhZi4gKi9cbiAgcGFyZW50czogQ2F0ZWdvcml6ZWRGaWx0ZXJbXSA9IFtdO1xuXG4gIGNvbnN0cnVjdG9yKGVzQnVja2V0RmxhdE5vZGU6IEVzQnVja2V0RmxhdE5vZGUpIHtcbiAgICB0aGlzLmtleSA9IGVzQnVja2V0RmxhdE5vZGUua2V5O1xuICAgIHRoaXMubGV2ZWwgPSBlc0J1Y2tldEZsYXROb2RlLmxldmVsO1xuICB9XG59XG4iLCJpbXBvcnQge0VzQnVja2V0fSBmcm9tICcuL2VzLWJ1Y2tldCc7XG5cbi8qKlxuICogRXh0ZW5zaW9uIG9mIEVTIGJ1Y2tldCB0byBmbGF0IG5vZGUuXG4gKi9cbmV4cG9ydCBjbGFzcyBFc0J1Y2tldEZsYXROb2RlIGV4dGVuZHMgRXNCdWNrZXQge1xuICAvKiogVGhlIG5vZGUgbGV2ZWwgKi9cbiAgbGV2ZWw6IG51bWJlcjtcbn1cbiIsImltcG9ydCB7Q29tcG9uZW50LCBEb0NoZWNrLCBFdmVudEVtaXR0ZXIsIElucHV0LCBPbkluaXQsIE91dHB1dH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0VzQnVja2V0QWdncmVnYXRpb259IGZyb20gJy4uLy4uL21vZGVscy9hZ2dyZWdhdGlvbi9idWNrZXQvZXMtYnVja2V0LWFnZ3JlZ2F0aW9uJztcbmltcG9ydCB7Q2F0ZWdvcml6ZWRGaWx0ZXJ9IGZyb20gJy4uLy4uL21vZGVscy9maWx0ZXIvY2F0ZWdvcml6ZWQtZmlsdGVyJztcbmltcG9ydCB7U2VsZWN0aW9uTW9kZWx9IGZyb20gJ0Bhbmd1bGFyL2Nkay9jb2xsZWN0aW9ucyc7XG5pbXBvcnQge0JlaGF2aW9yU3ViamVjdH0gZnJvbSAncnhqcyc7XG5pbXBvcnQge0ZsYXRUcmVlQ29udHJvbH0gZnJvbSAnQGFuZ3VsYXIvY2RrL3RyZWUnO1xuaW1wb3J0IHtNYXRUcmVlRmxhdERhdGFTb3VyY2UsIE1hdFRyZWVGbGF0dGVuZXJ9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcbmltcG9ydCB7RXNCdWNrZXR9IGZyb20gJy4uLy4uL21vZGVscy9hZ2dyZWdhdGlvbi9idWNrZXQvZXMtYnVja2V0JztcbmltcG9ydCB7RXNCdWNrZXRGbGF0Tm9kZX0gZnJvbSAnLi4vLi4vbW9kZWxzL2FnZ3JlZ2F0aW9uL2J1Y2tldC9lcy1idWNrZXQtZmxhdC1ub2RlJztcblxuQENvbXBvbmVudCh7XG4gICAgICAgICAgICAgc2VsZWN0b3IgICA6ICdib290c3RhcnQtY2hlY2tsaXN0LWZpbHRlcicsXG4gICAgICAgICAgICAgdGVtcGxhdGU6IGA8bWF0LXRyZWUgW2RhdGFTb3VyY2VdPVwiZGF0YVNvdXJjZVwiIFt0cmVlQ29udHJvbF09XCJ0cmVlQ29udHJvbFwiIGNsYXNzPVwiYm9vdHN0YXJ0LWNoZWNrbGlzdC10cmVlXCI+XG5cbiAgPCEtLU5lc3RlZCB0cmVlIG5vZGUtLT5cbiAgPG1hdC10cmVlLW5vZGUgKm1hdFRyZWVOb2RlRGVmPVwibGV0IG5vZGU7IHdoZW46IGhhc0NoaWxkXCJcbiAgICAgICAgICAgICAgICAgbWF0VHJlZU5vZGVQYWRkaW5nXG4gICAgICAgICAgICAgICAgIGNsYXNzPVwicm93XCJcbiAgICAgICAgICAgICAgICAgdGl0bGU9XCJ7e3RyYW5zbGF0aW9uUHJlZml4K25vZGUua2V5fHRyYW5zbGF0ZX19ICh7e25vZGUuY291bnR9fSlcIj5cblxuICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtMTAgcm93XCI+XG5cbiAgICAgIDxtYXQtY2hlY2tib3ggY29sb3I9XCJwcmltYXJ5XCJcbiAgICAgICAgICAgICAgICAgICAgY2xhc3M9XCJjb2wtbWQtMTJcIlxuICAgICAgICAgICAgICAgICAgICBbY2hlY2tlZF09XCJkZXNjZW5kYW50c0FsbFNlbGVjdGVkKG5vZGUpXCJcbiAgICAgICAgICAgICAgICAgICAgW2luZGV0ZXJtaW5hdGVdPVwiZGVzY2VuZGFudHNQYXJ0aWFsbHlTZWxlY3RlZChub2RlKVwiXG4gICAgICAgICAgICAgICAgICAgIChjaGFuZ2UpPVwiYnVja2V0U2VsZWN0aW9uVG9nZ2xlKG5vZGUpXCI+XG5cbiAgICAgICAgPGRpdiBjbGFzcz1cInJvd1wiPlxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtMTIgY2hlY2tib3gtbGFiZWxcIlxuICAgICAgICAgICAgICAgW2NsYXNzLmNoZWNrYm94LWxhYmVsLXplcm8tY291bnRdPVwibm9kZS5jb3VudCA9PT0gMFwiPlxuICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJidWNrZXQta2V5XCI+e3t0cmFuc2xhdGlvblByZWZpeCtub2RlLmtleXx0cmFuc2xhdGV9fTwvc3Bhbj5cbiAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiYnVja2V0LWNvdW50XCI+Jm5ic3A7JmJ1bGw7Jm5ic3A7e3tub2RlLmNvdW50fX08L3NwYW4+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuXG4gICAgICA8L21hdC1jaGVja2JveD5cblxuICAgICAgPG1hdC1wcm9ncmVzcy1iYXIgY2xhc3M9XCJjb2wtbWQtMTJcIlxuICAgICAgICAgICAgICAgICAgICAgICAgbW9kZT1cImRldGVybWluYXRlXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPVwie3tub2RlLnByb3BvcnRpb24qMTAwIHx8IDB9fVwiPjwvbWF0LXByb2dyZXNzLWJhcj5cblxuICAgIDwvZGl2PlxuXG4gICAgPGJ1dHRvbiBtYXQtaWNvbi1idXR0b24gbWF0VHJlZU5vZGVUb2dnbGUgY2xhc3M9XCJjb2wtbWQtMlwiPlxuICAgICAgPG1hdC1pY29uIGNsYXNzPVwibWF0LWljb24tcnRsLW1pcnJvclwiPlxuICAgICAgICB7e3RyZWVDb250cm9sLmlzRXhwYW5kZWQobm9kZSkgPyAnZXhwYW5kX21vcmUnIDogJ2NoZXZyb25fcmlnaHQnfX1cbiAgICAgIDwvbWF0LWljb24+XG4gICAgPC9idXR0b24+XG5cbiAgICA8dWwgW2NsYXNzLmNoZWNrbGlzdC10cmVlLWludmlzaWJsZV09XCIhdHJlZUNvbnRyb2wuaXNFeHBhbmRlZChub2RlKVwiXG4gICAgICAgIGNsYXNzPVwiY29sLW1kLTEyIGJvb3RzdGFydC10cmVlLWNoaWxkLW5vZGVcIj5cbiAgICAgIDxuZy1jb250YWluZXIgbWF0VHJlZU5vZGVPdXRsZXQ+PC9uZy1jb250YWluZXI+XG4gICAgPC91bD5cblxuICA8L21hdC10cmVlLW5vZGU+XG5cblxuICA8IS0tTGVhZiBub2RlLS0+XG4gIDxtYXQtdHJlZS1ub2RlICptYXRUcmVlTm9kZURlZj1cImxldCBub2RlXCJcbiAgICAgICAgICAgICAgICAgY2xhc3M9XCJyb3dcIlxuICAgICAgICAgICAgICAgICBtYXRUcmVlTm9kZVRvZ2dsZSBtYXRUcmVlTm9kZVBhZGRpbmdcbiAgICAgICAgICAgICAgICAgdGl0bGU9XCJ7e3RyYW5zbGF0aW9uUHJlZml4K25vZGUua2V5fHRyYW5zbGF0ZX19ICh7e25vZGUuY291bnR9fSlcIj5cblxuICAgIDxkaXYgY2xhc3M9XCJyb3cgY29sLW1kLTEyXCI+XG5cbiAgICAgIDxtYXQtY2hlY2tib3ggY29sb3I9XCJwcmltYXJ5XCJcbiAgICAgICAgICAgICAgICAgICAgY2xhc3M9XCJjb2wtbWQtMTJcIlxuICAgICAgICAgICAgICAgICAgICBbY2hlY2tlZF09XCJjaGVja2xpc3RTZWxlY3Rpb24uaXNTZWxlY3RlZChub2RlKVwiXG4gICAgICAgICAgICAgICAgICAgIChjaGFuZ2UpPVwiYnVja2V0TGVhZlNlbGVjdGlvblRvZ2dsZShub2RlKVwiPlxuXG4gICAgICAgIDxkaXYgY2xhc3M9XCJyb3dcIj5cbiAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEyIGNoZWNrYm94LWxhYmVsXCJcbiAgICAgICAgICAgICAgIFtjbGFzcy5jaGVja2JveC1sYWJlbC16ZXJvLWNvdW50XT1cIm5vZGUuY291bnQgPT09IDBcIj5cbiAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiYnVja2V0LWtleVwiPnt7dHJhbnNsYXRpb25QcmVmaXgrbm9kZS5rZXl8dHJhbnNsYXRlfX08L3NwYW4+XG4gICAgICAgICAgICA8c3BhbiBjbGFzcz1cImJ1Y2tldC1jb3VudFwiPiZuYnNwOyZidWxsOyZuYnNwO3t7bm9kZS5jb3VudH19PC9zcGFuPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cblxuICAgICAgPC9tYXQtY2hlY2tib3g+XG5cbiAgICAgIDxtYXQtcHJvZ3Jlc3MtYmFyIGNsYXNzPVwiY29sLW1kLTEyXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1vZGU9XCJkZXRlcm1pbmF0ZVwiXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT1cInt7bm9kZS5wcm9wb3J0aW9uKjEwMCB8fCAwfX1cIj48L21hdC1wcm9ncmVzcy1iYXI+XG5cbiAgICA8L2Rpdj5cblxuICA8L21hdC10cmVlLW5vZGU+XG5cbjwvbWF0LXRyZWU+XG5gLFxuICAgICAgICAgICAgIHN0eWxlczogW2AuYm9vdHN0YXJ0LWNoZWNrbGlzdC10cmVlLWludmlzaWJsZXtkaXNwbGF5Om5vbmV9LmJvb3RzdGFydC1jaGVja2xpc3QtdHJlZXttYXJnaW4tdG9wOjE1cHh9LmJvb3RzdGFydC1jaGVja2xpc3QtdHJlZSBtYXQtdHJlZS1ub2Rle21hcmdpbi10b3A6LTVweH0uYm9vdHN0YXJ0LWNoZWNrbGlzdC10cmVlIG1hdC1jaGVja2JveHtjdXJzb3I6cG9pbnRlcn0uYm9vdHN0YXJ0LWNoZWNrbGlzdC10cmVlIGJ1dHRvbjpmb2N1c3tvdXRsaW5lOjB9LmJvb3RzdGFydC1jaGVja2xpc3QtdHJlZSBtYXQtcHJvZ3Jlc3MtYmFye21hcmdpbi10b3A6LTdweH0uYm9vdHN0YXJ0LXRyZWUtY2hpbGQtbm9kZXttYXJnaW4tYm90dG9tOi0xNXB4fS5jaGVja2JveC1sYWJlbHt3aGl0ZS1zcGFjZTppbml0aWFsIWltcG9ydGFudH0uYnVja2V0LWtleXtmb250LXNpemU6c21hbGx9LmJ1Y2tldC1jb3VudHtmb250LXNpemU6c21hbGw7Zm9udC13ZWlnaHQ6NzAwfS5jaGVja2JveC1sYWJlbC16ZXJvLWNvdW50IHNwYW57Y29sb3I6Z3JheSFpbXBvcnRhbnR9LmNoZWNrYm94LWxhYmVsLXplcm8tY291bnQgLmJ1Y2tldC1jb3VudHtmb250LXNpemU6c21hbGwhaW1wb3J0YW50O2ZvbnQtd2VpZ2h0OjQwMCFpbXBvcnRhbnR9YF1cbiAgICAgICAgICAgfSlcbmV4cG9ydCBjbGFzcyBCb290c3RhcnRDaGVja2xpc3RGaWx0ZXJDb21wb25lbnQgaW1wbGVtZW50cyBEb0NoZWNrLCBPbkluaXQge1xuXG4gIC8qKiBJbnB1dCBidWNrZXQgYWdncmVnYXRpb24gKi9cbiAgQElucHV0KCkgYWdncmVnYXRpb246IEVzQnVja2V0QWdncmVnYXRpb247XG5cbiAgLyoqIFRyYW5zbGF0aW9uIHByZWZpeCAqL1xuICBASW5wdXQoKSB0cmFuc2xhdGlvblByZWZpeDogc3RyaW5nO1xuXG4gIC8qKiBDYXRlZ29yaXplZCBmaWx0ZXIgZW1pdHRlciAqL1xuICBAT3V0cHV0KCkgb25DaGFuZ2UgPSBuZXcgRXZlbnRFbWl0dGVyPENhdGVnb3JpemVkRmlsdGVyW10+KCk7XG5cbiAgLyoqIFNlbGVjdGlvbiBmb3IgY2hlY2tsaXN0ICovXG4gIGNoZWNrbGlzdFNlbGVjdGlvbiA9IG5ldyBTZWxlY3Rpb25Nb2RlbDxFc0J1Y2tldD4odHJ1ZSk7XG5cbiAgLyoqIFNlbGVjdGlvbiBmb3IgZW1pdHRlciAqL1xuICBlbWl0dGVyU2VsZWN0aW9uID0gbmV3IFNlbGVjdGlvbk1vZGVsPENhdGVnb3JpemVkRmlsdGVyPih0cnVlKTtcblxuICAvKiogRWxlbWVudHMgZGlzY2FyZGVkIGZyb20gc2VsZWN0aW9uIChlLmcuIGNoaWxkcmVuIG9mIGEgbm9kZSBpZiBhbGwgY2hpbGRyZW4gYXJlIGNoZWNrZWQpICovXG4gIGRpc2NhcmRlZEVtaXR0ZXJTZWxlY3Rpb24gPSBuZXcgU2VsZWN0aW9uTW9kZWw8RXNCdWNrZXQ+KHRydWUpO1xuXG4gIC8qKiBNYXAgZnJvbSBmbGF0IG5vZGUgdG8gbmVzdGVkIG5vZGUuIFRoaXMgaGVscHMgdXMgZmluZGluZyB0aGUgbmVzdGVkIG5vZGUgdG8gYmUgbW9kaWZpZWQgKi9cbiAgZmxhdE5vZGVNYXAgPSBuZXcgTWFwPEVzQnVja2V0RmxhdE5vZGUsIEVzQnVja2V0PigpO1xuXG4gIC8qKiBNYXAgZnJvbSBuZXN0ZWQgbm9kZSB0byBmbGF0dGVuZWQgbm9kZS4gVGhpcyBoZWxwcyB1cyB0byBrZWVwIHRoZSBzYW1lIG9iamVjdCBmb3Igc2VsZWN0aW9uICovXG4gIG5lc3RlZE5vZGVNYXAgPSBuZXcgTWFwPEVzQnVja2V0LCBFc0J1Y2tldEZsYXROb2RlPigpO1xuXG4gIGRhdGFDaGFuZ2U6IEJlaGF2aW9yU3ViamVjdDxFc0J1Y2tldFtdPiA9IG5ldyBCZWhhdmlvclN1YmplY3Q8RXNCdWNrZXRbXT4oW10pO1xuXG4gIHRyZWVDb250cm9sOiBGbGF0VHJlZUNvbnRyb2w8RXNCdWNrZXRGbGF0Tm9kZT47XG5cbiAgdHJlZUZsYXR0ZW5lcjogTWF0VHJlZUZsYXR0ZW5lcjxFc0J1Y2tldCwgRXNCdWNrZXRGbGF0Tm9kZT47XG5cbiAgZGF0YVNvdXJjZTogTWF0VHJlZUZsYXREYXRhU291cmNlPEVzQnVja2V0LCBFc0J1Y2tldEZsYXROb2RlPjtcblxuICBnZXRMZXZlbCA9IChub2RlOiBFc0J1Y2tldEZsYXROb2RlKSA9PiBub2RlLmxldmVsO1xuICBpc0V4cGFuZGFibGUgPSAobm9kZTogRXNCdWNrZXRGbGF0Tm9kZSkgPT4gbm9kZS5idWNrZXRzLmxlbmd0aCA+IDA7XG4gIGdldENoaWxkcmVuID0gKG5vZGU6IEVzQnVja2V0KTogRXNCdWNrZXRbXSA9PiBub2RlLmJ1Y2tldHM7XG4gIGhhc0NoaWxkID0gKF86IG51bWJlciwgX25vZGVEYXRhOiBFc0J1Y2tldEZsYXROb2RlKSA9PiBfbm9kZURhdGEuYnVja2V0cy5sZW5ndGggPiAwO1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHRoaXMudHJlZUZsYXR0ZW5lciA9IG5ldyBNYXRUcmVlRmxhdHRlbmVyKHRoaXMuX3RyYW5zZm9ybWVyLCB0aGlzLmdldExldmVsLCB0aGlzLmlzRXhwYW5kYWJsZSwgdGhpcy5nZXRDaGlsZHJlbik7XG4gICAgdGhpcy50cmVlQ29udHJvbCA9IG5ldyBGbGF0VHJlZUNvbnRyb2w8RXNCdWNrZXRGbGF0Tm9kZT4odGhpcy5nZXRMZXZlbCwgdGhpcy5pc0V4cGFuZGFibGUpO1xuICAgIHRoaXMuZGF0YVNvdXJjZSA9IG5ldyBNYXRUcmVlRmxhdERhdGFTb3VyY2UodGhpcy50cmVlQ29udHJvbCwgdGhpcy50cmVlRmxhdHRlbmVyKTtcbiAgICB0aGlzLmRhdGFDaGFuZ2Uuc3Vic2NyaWJlKGRhdGEgPT4gdGhpcy5kYXRhU291cmNlLmRhdGEgPSBkYXRhKTtcbiAgfVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIGlmICh0aGlzLnRyYW5zbGF0aW9uUHJlZml4ID09PSB1bmRlZmluZWQpIHtcbiAgICAgIHRoaXMudHJhbnNsYXRpb25QcmVmaXggPSAnJztcbiAgICB9XG4gIH1cblxuICBuZ0RvQ2hlY2soKTogdm9pZCB7XG4gICAgdGhpcy5kYXRhQ2hhbmdlLm5leHQodGhpcy5hZ2dyZWdhdGlvbi5idWNrZXRzKTtcbiAgfVxuXG4gIC8qKiBXaGV0aGVyIGFsbCB0aGUgZGVzY2VuZGFudHMgb2YgdGhlIG5vZGUgYXJlIHNlbGVjdGVkICovXG4gIGRlc2NlbmRhbnRzQWxsU2VsZWN0ZWQobm9kZTogRXNCdWNrZXRGbGF0Tm9kZSk6IGJvb2xlYW4ge1xuICAgIGNvbnN0IGRlc2NlbmRhbnRzID0gdGhpcy50cmVlQ29udHJvbC5nZXREZXNjZW5kYW50cyhub2RlKTtcbiAgICBpZiAoZGVzY2VuZGFudHMubGVuZ3RoID09PSAwKSB7XG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuICAgIHJldHVybiBkZXNjZW5kYW50cy5ldmVyeShjaGlsZCA9PiB0aGlzLmNoZWNrbGlzdFNlbGVjdGlvbi5pc1NlbGVjdGVkKGNoaWxkKSk7XG4gIH1cblxuICAvKiogV2hldGhlciBwYXJ0IG9mIGRlc2NlbmRhbnRzIGFyZSBzZWxlY3RlZCAqL1xuICBkZXNjZW5kYW50c1BhcnRpYWxseVNlbGVjdGVkKG5vZGU6IEVzQnVja2V0RmxhdE5vZGUpOiBib29sZWFuIHtcbiAgICBjb25zdCBkZXNjZW5kYW50cyA9IHRoaXMudHJlZUNvbnRyb2wuZ2V0RGVzY2VuZGFudHMobm9kZSk7XG4gICAgY29uc3QgcmVzdWx0ID0gZGVzY2VuZGFudHMuc29tZShjaGlsZCA9PiB0aGlzLmNoZWNrbGlzdFNlbGVjdGlvbi5pc1NlbGVjdGVkKGNoaWxkKSk7XG4gICAgcmV0dXJuIHJlc3VsdCAmJiAhdGhpcy5kZXNjZW5kYW50c0FsbFNlbGVjdGVkKG5vZGUpO1xuICB9XG5cbiAgLyoqIFRvZ2dsZSB0aGUgRWxhc3RpY3NlYXJjaCBidWNrZXQgbm9kZSBpdGVtLiBTZWxlY3QvZGVzZWxlY3QgYWxsIHRoZSBkZXNjZW5kYW50cyBub2RlICovXG4gIGJ1Y2tldFNlbGVjdGlvblRvZ2dsZShub2RlOiBFc0J1Y2tldEZsYXROb2RlKTogdm9pZCB7XG4gICAgdGhpcy5jaGVja2xpc3RTZWxlY3Rpb24udG9nZ2xlKG5vZGUpO1xuICAgIGNvbnN0IGRlc2NlbmRhbnRzID0gdGhpcy50cmVlQ29udHJvbC5nZXREZXNjZW5kYW50cyhub2RlKTtcbiAgICB0aGlzLmNoZWNrbGlzdFNlbGVjdGlvbi5pc1NlbGVjdGVkKG5vZGUpXG4gICAgPyB0aGlzLmNoZWNrbGlzdFNlbGVjdGlvbi5zZWxlY3QoLi4uZGVzY2VuZGFudHMpXG4gICAgOiB0aGlzLmNoZWNrbGlzdFNlbGVjdGlvbi5kZXNlbGVjdCguLi5kZXNjZW5kYW50cyk7XG4gICAgdGhpcy5fY2hlY2tBbGxQYXJlbnRzU2VsZWN0aW9uKG5vZGUpO1xuICAgIHRoaXMuX2VtaXRDaGFuZ2UoKTtcbiAgfVxuXG4gIC8qKiBUb2dnbGUgYSBsZWFmIEVsYXN0aWNzZWFyY2ggYnVja2V0IGl0ZW0gc2VsZWN0aW9uLiBDaGVjayBhbGwgdGhlIHBhcmVudHMgdG8gc2VlIGlmIHRoZXkgY2hhbmdlZCAqL1xuICBidWNrZXRMZWFmU2VsZWN0aW9uVG9nZ2xlKG5vZGU6IEVzQnVja2V0RmxhdE5vZGUpOiB2b2lkIHtcbiAgICB0aGlzLmNoZWNrbGlzdFNlbGVjdGlvbi50b2dnbGUobm9kZSk7XG4gICAgdGhpcy5fY2hlY2tBbGxQYXJlbnRzU2VsZWN0aW9uKG5vZGUpO1xuICAgIHRoaXMuX2VtaXRDaGFuZ2UoKTtcbiAgfVxuXG4gIHByaXZhdGUgX2VtaXRDaGFuZ2UoKTogdm9pZCB7XG4gICAgdGhpcy5lbWl0dGVyU2VsZWN0aW9uLmNsZWFyKCk7XG4gICAgdGhpcy5kaXNjYXJkZWRFbWl0dGVyU2VsZWN0aW9uLmNsZWFyKCk7XG4gICAgdGhpcy5fYWRkVG9FbWl0U2VsZWN0b3IodGhpcy5jaGVja2xpc3RTZWxlY3Rpb24uc2VsZWN0ZWQpO1xuICAgIHRoaXMub25DaGFuZ2UuZW1pdCh0aGlzLmVtaXR0ZXJTZWxlY3Rpb24uc2VsZWN0ZWQpO1xuICB9XG5cbiAgcHJpdmF0ZSBfYWRkVG9FbWl0U2VsZWN0b3IoYnVja2V0czogRXNCdWNrZXRbXSk6IHZvaWQge1xuICAgIGJ1Y2tldHMuZm9yRWFjaChlbGVtZW50ID0+IHtcbiAgICAgIGNvbnN0IGZsYXRFbGVtZW50ID0gPEVzQnVja2V0RmxhdE5vZGU+ZWxlbWVudDtcbiAgICAgIGlmICh0aGlzLmNoZWNrbGlzdFNlbGVjdGlvbi5pc1NlbGVjdGVkKGVsZW1lbnQpKSB7XG4gICAgICAgIGlmICh0aGlzLmRlc2NlbmRhbnRzQWxsU2VsZWN0ZWQoZmxhdEVsZW1lbnQpKSB7XG4gICAgICAgICAgY29uc3QgZGVzY2VuZGFudHMgPSB0aGlzLnRyZWVDb250cm9sLmdldERlc2NlbmRhbnRzKGZsYXRFbGVtZW50KTtcbiAgICAgICAgICB0aGlzLmRpc2NhcmRlZEVtaXR0ZXJTZWxlY3Rpb24uc2VsZWN0KC4uLmRlc2NlbmRhbnRzKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGxldCBwYXJlbnQ6IEVzQnVja2V0RmxhdE5vZGUgfCBudWxsID0gdGhpcy5fZ2V0UGFyZW50Tm9kZShmbGF0RWxlbWVudCk7XG4gICAgICAgIHdoaWxlIChwYXJlbnQpIHtcbiAgICAgICAgICB0aGlzLl9hZGRUb0VtaXRTZWxlY3RvcihbcGFyZW50XSk7XG4gICAgICAgICAgcGFyZW50ID0gdGhpcy5fZ2V0UGFyZW50Tm9kZShwYXJlbnQpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKCF0aGlzLmRpc2NhcmRlZEVtaXR0ZXJTZWxlY3Rpb24uaXNTZWxlY3RlZChlbGVtZW50KSkge1xuICAgICAgICAgIGNvbnN0IGZpbHRlciA9IG5ldyBDYXRlZ29yaXplZEZpbHRlcihmbGF0RWxlbWVudCk7XG4gICAgICAgICAgbGV0IGVsdFBhcmVudDogRXNCdWNrZXRGbGF0Tm9kZSB8IG51bGwgPSB0aGlzLl9nZXRQYXJlbnROb2RlKGZsYXRFbGVtZW50KTtcbiAgICAgICAgICB3aGlsZSAoZWx0UGFyZW50KSB7XG4gICAgICAgICAgICBmaWx0ZXIucGFyZW50cy5wdXNoKG5ldyBDYXRlZ29yaXplZEZpbHRlcihlbHRQYXJlbnQpKTtcbiAgICAgICAgICAgIGVsdFBhcmVudCA9IHRoaXMuX2dldFBhcmVudE5vZGUoZWx0UGFyZW50KTtcbiAgICAgICAgICB9XG4gICAgICAgICAgdGhpcy5lbWl0dGVyU2VsZWN0aW9uLnNlbGVjdChmaWx0ZXIpO1xuICAgICAgICAgIHRoaXMuZGlzY2FyZGVkRW1pdHRlclNlbGVjdGlvbi5zZWxlY3QoZWxlbWVudCk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIC8qKiBUcmFuc2Zvcm1lciB0byBjb252ZXJ0IG5lc3RlZCBub2RlIHRvIGZsYXQgbm9kZS4gUmVjb3JkIHRoZSBub2RlcyBpbiBtYXBzIGZvciBsYXRlciB1c2UuICovXG4gIHByaXZhdGUgX3RyYW5zZm9ybWVyKG5vZGU6IEVzQnVja2V0LCBsZXZlbDogbnVtYmVyKTogRXNCdWNrZXRGbGF0Tm9kZSB7XG4gICAgaWYgKCF0aGlzLm5lc3RlZE5vZGVNYXApIHtcbiAgICAgIHRoaXMubmVzdGVkTm9kZU1hcCA9IG5ldyBNYXA8RXNCdWNrZXQsIEVzQnVja2V0RmxhdE5vZGU+KCk7XG4gICAgfVxuICAgIGlmICghdGhpcy5mbGF0Tm9kZU1hcCkge1xuICAgICAgdGhpcy5mbGF0Tm9kZU1hcCA9IG5ldyBNYXA8RXNCdWNrZXRGbGF0Tm9kZSwgRXNCdWNrZXQ+KCk7XG4gICAgfVxuICAgIGNvbnN0IGV4aXN0aW5nTm9kZSA9IHRoaXMubmVzdGVkTm9kZU1hcC5nZXQobm9kZSk7XG4gICAgY29uc3QgZmxhdE5vZGUgPSBleGlzdGluZ05vZGUgJiYgZXhpc3RpbmdOb2RlLmtleSA9PT0gbm9kZS5rZXlcbiAgICAgICAgICAgICAgICAgICAgID8gZXhpc3RpbmdOb2RlXG4gICAgICAgICAgICAgICAgICAgICA6IG5ldyBFc0J1Y2tldEZsYXROb2RlKCk7XG4gICAgZmxhdE5vZGUua2V5ID0gbm9kZS5rZXk7XG4gICAgZmxhdE5vZGUuY291bnQgPSBub2RlLmNvdW50O1xuICAgIGZsYXROb2RlLmJ1Y2tldHMgPSBub2RlLmJ1Y2tldHM7XG4gICAgZmxhdE5vZGUucHJvcG9ydGlvbiA9IG5vZGUucHJvcG9ydGlvbjtcbiAgICBmbGF0Tm9kZS5sZXZlbCA9IGxldmVsO1xuICAgIHRoaXMuZmxhdE5vZGVNYXAuc2V0KGZsYXROb2RlLCBub2RlKTtcbiAgICB0aGlzLm5lc3RlZE5vZGVNYXAuc2V0KG5vZGUsIGZsYXROb2RlKTtcbiAgICByZXR1cm4gZmxhdE5vZGU7XG4gIH1cblxuICAvKiogQ2hlY2tzIGFsbCB0aGUgcGFyZW50cyB3aGVuIGEgbm9kZSBpcyBzZWxlY3RlZC91bnNlbGVjdGVkICovXG4gIHByaXZhdGUgX2NoZWNrQWxsUGFyZW50c1NlbGVjdGlvbihub2RlOiBFc0J1Y2tldEZsYXROb2RlKTogdm9pZCB7XG4gICAgbGV0IHBhcmVudDogRXNCdWNrZXRGbGF0Tm9kZSB8IG51bGwgPSB0aGlzLl9nZXRQYXJlbnROb2RlKG5vZGUpO1xuICAgIHdoaWxlIChwYXJlbnQpIHtcbiAgICAgIHRoaXMuX2NoZWNrUm9vdE5vZGVTZWxlY3Rpb24ocGFyZW50KTtcbiAgICAgIHBhcmVudCA9IHRoaXMuX2dldFBhcmVudE5vZGUocGFyZW50KTtcbiAgICB9XG4gIH1cblxuICAvKiogQ2hlY2sgcm9vdCBub2RlIGNoZWNrZWQgc3RhdGUgYW5kIGNoYW5nZSBpdCBhY2NvcmRpbmdseSAqL1xuICBwcml2YXRlIF9jaGVja1Jvb3ROb2RlU2VsZWN0aW9uKG5vZGU6IEVzQnVja2V0RmxhdE5vZGUpOiB2b2lkIHtcbiAgICBjb25zdCBub2RlU2VsZWN0ZWQgPSB0aGlzLmNoZWNrbGlzdFNlbGVjdGlvbi5pc1NlbGVjdGVkKG5vZGUpO1xuICAgIGlmIChub2RlU2VsZWN0ZWQgJiYgIXRoaXMuZGVzY2VuZGFudHNBbGxTZWxlY3RlZChub2RlKSkge1xuICAgICAgdGhpcy5jaGVja2xpc3RTZWxlY3Rpb24uZGVzZWxlY3Qobm9kZSk7XG4gICAgfSBlbHNlIGlmICghbm9kZVNlbGVjdGVkICYmIHRoaXMuZGVzY2VuZGFudHNBbGxTZWxlY3RlZChub2RlKSkge1xuICAgICAgdGhpcy5jaGVja2xpc3RTZWxlY3Rpb24uc2VsZWN0KG5vZGUpO1xuICAgIH1cbiAgfVxuXG4gIC8qKiBHZXQgdGhlIHBhcmVudCBub2RlIG9mIGEgbm9kZSAqL1xuICBwcml2YXRlIF9nZXRQYXJlbnROb2RlKG5vZGU6IEVzQnVja2V0RmxhdE5vZGUpOiBFc0J1Y2tldEZsYXROb2RlIHwgbnVsbCB7XG4gICAgY29uc3QgY3VycmVudExldmVsID0gdGhpcy5nZXRMZXZlbChub2RlKTtcblxuICAgIGlmIChjdXJyZW50TGV2ZWwgPCAxKSB7XG4gICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG5cbiAgICBjb25zdCBzdGFydEluZGV4ID0gdGhpcy50cmVlQ29udHJvbC5kYXRhTm9kZXMuaW5kZXhPZihub2RlKSAtIDE7XG5cbiAgICBmb3IgKGxldCBpID0gc3RhcnRJbmRleDsgaSA+PSAwOyBpLS0pIHtcbiAgICAgIGNvbnN0IGN1cnJlbnROb2RlID0gdGhpcy50cmVlQ29udHJvbC5kYXRhTm9kZXNbaV07XG5cbiAgICAgIGlmICh0aGlzLmdldExldmVsKGN1cnJlbnROb2RlKSA8IGN1cnJlbnRMZXZlbCkge1xuICAgICAgICByZXR1cm4gY3VycmVudE5vZGU7XG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiBudWxsO1xuICB9XG59XG4iLCJleHBvcnQgY2xhc3MgSGlzdG9ncmFtSW50ZXJ2YWxEYXRhIHtcbiAgeDogbnVtYmVyO1xuICB5OiBudW1iZXI7XG59XG4iLCJleHBvcnQgY2xhc3MgSW50ZXJ2YWwge1xuICBtaW46IG51bWJlcjtcbiAgbWF4OiBudW1iZXI7XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gIH1cblxufVxuIiwiaW1wb3J0IHtDb21wb25lbnQsIERvQ2hlY2ssIEV2ZW50RW1pdHRlciwgSW5wdXQsIE91dHB1dH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0VzQnVja2V0LCBFc0J1Y2tldEFnZ3JlZ2F0aW9ufSBmcm9tICcuLi8uLi9tb2RlbHMnO1xuaW1wb3J0IHtMYWJlbFR5cGUsIE9wdGlvbnN9IGZyb20gJ25nNS1zbGlkZXInO1xuaW1wb3J0IHtIaXN0b2dyYW1JbnRlcnZhbERhdGF9IGZyb20gJy4uLy4uL21vZGVscy91dGlscy9oaXN0b2dyYW0taW50ZXJ2YWwtZGF0YSc7XG5pbXBvcnQge0ludGVydmFsfSBmcm9tICcuLi8uLi9tb2RlbHMvdXRpbHMvaW50ZXJ2YWwnO1xuaW1wb3J0IHtCZWhhdmlvclN1YmplY3R9IGZyb20gJ3J4anMnO1xuXG5AQ29tcG9uZW50KHtcbiAgICAgICAgICAgICBzZWxlY3RvciAgIDogJ2Jvb3RzdGFydC1oaXN0b2dyYW0tc2xpZGVyLWZpbHRlcicsXG4gICAgICAgICAgICAgdGVtcGxhdGU6IGA8ZGl2IGNsYXNzPVwiYm9vdHN0YXJ0LXJhbmdlLXNsaWRlclwiICpuZ0lmPVwic2xpZGVySW5pdGlhbGl6ZWRcIj5cbiAgPG5nNS1zbGlkZXIgWyh2YWx1ZSldPVwibWluU2VsZWN0ZWRcIlxuICAgICAgICAgICAgICBbKGhpZ2hWYWx1ZSldPVwibWF4U2VsZWN0ZWRcIlxuICAgICAgICAgICAgICBbb3B0aW9uc109XCJzbGlkZXJPcHRpb25zXCJcbiAgICAgICAgICAgICAgKHZhbHVlQ2hhbmdlKT1cInZhbHVlQ2hhbmdlKClcIlxuICAgICAgICAgICAgICAoaGlnaFZhbHVlQ2hhbmdlKT1cInZhbHVlQ2hhbmdlKClcIj48L25nNS1zbGlkZXI+XG48L2Rpdj5cbmAsXG4gICAgICAgICAgICAgc3R5bGVzOiBbYGBdXG4gICAgICAgICAgIH0pXG5leHBvcnQgY2xhc3MgQm9vdHN0YXJ0SGlzdG9ncmFtU2xpZGVyRmlsdGVyQ29tcG9uZW50IGltcGxlbWVudHMgRG9DaGVjayB7XG5cbiAgQElucHV0KCkgYnVja2V0QWdncmVnYXRpb246IEVzQnVja2V0QWdncmVnYXRpb247XG4gIEBJbnB1dCgpIG1pblJhbmdlOiBudW1iZXIgPSAxO1xuXG4gIGJyZWFrcG9pbnRzOiBudW1iZXJbXTtcbiAgYnJlYWtwb2ludEZpbml0ZU1pblZhbHVlOiBudW1iZXI7XG4gIGJyZWFrcG9pbnRGaW5pdGVNYXhWYWx1ZTogbnVtYmVyO1xuICBsb3dlckJvdW5kZWQ6IGJvb2xlYW47XG4gIHVwcGVyQm91bmRlZDogYm9vbGVhbjtcbiAgaGlzdG9ncmFtRGF0YTogSGlzdG9ncmFtSW50ZXJ2YWxEYXRhW107XG4gIG1pblNlbGVjdGVkOiBudW1iZXI7XG4gIG1heFNlbGVjdGVkOiBudW1iZXI7XG4gIHNsaWRlck9wdGlvbnM6IE9wdGlvbnM7XG4gIHNsaWRlckluaXRpYWxpemVkOiBib29sZWFuO1xuXG4gIGRhdGFTb3VyY2U6IEVzQnVja2V0W107XG4gIGRhdGFDaGFuZ2U6IEJlaGF2aW9yU3ViamVjdDxFc0J1Y2tldFtdPiA9IG5ldyBCZWhhdmlvclN1YmplY3Q8RXNCdWNrZXRbXT4oW10pO1xuXG4gIEBPdXRwdXQoKSByYW5nZUNoYW5nZTogRXZlbnRFbWl0dGVyPEludGVydmFsPjtcblxuXG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHRoaXMuYnJlYWtwb2ludHMgPSBbXTtcbiAgICB0aGlzLmxvd2VyQm91bmRlZCA9IHRydWU7XG4gICAgdGhpcy51cHBlckJvdW5kZWQgPSB0cnVlO1xuICAgIHRoaXMuaGlzdG9ncmFtRGF0YSA9IFtdO1xuICAgIHRoaXMuc2xpZGVyT3B0aW9ucyA9IG5ldyBPcHRpb25zKCk7XG4gICAgdGhpcy5zbGlkZXJJbml0aWFsaXplZCA9IGZhbHNlO1xuICAgIHRoaXMucmFuZ2VDaGFuZ2UgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG5cbiAgICB0aGlzLmRhdGFTb3VyY2UgPSBbXTtcbiAgICB0aGlzLmRhdGFDaGFuZ2Uuc3Vic2NyaWJlKGRhdGEgPT4ge1xuICAgICAgaWYgKCF0aGlzLnNsaWRlckluaXRpYWxpemVkKSB7XG4gICAgICAgIHRoaXMuZGF0YVNvdXJjZSA9IGRhdGE7XG4gICAgICAgIHRoaXMuX2luaXQoKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIG5nRG9DaGVjaygpOiB2b2lkIHtcbiAgICB0aGlzLmRhdGFDaGFuZ2UubmV4dCh0aGlzLmJ1Y2tldEFnZ3JlZ2F0aW9uLmJ1Y2tldHMpO1xuICB9XG5cblxuICAvKipcbiAgICogRW1pdHMgcmFuZ2UgZXZlbnQgd2hlbiB2YWx1ZSBjaGFuZ2VzLlxuICAgKiBJZiBtaW4gb3IgbWF4IGlzIGluZmluaXRlLCByZXR1cm5zIG51bGwuXG4gICAqL1xuICB2YWx1ZUNoYW5nZSgpOiB2b2lkIHtcbiAgICBjb25zdCBpbnRlcnZhbCA9IG5ldyBJbnRlcnZhbCgpO1xuICAgIGludGVydmFsLm1pbiA9ICghdGhpcy5sb3dlckJvdW5kZWQgJiYgdGhpcy5taW5TZWxlY3RlZCA9PT0gdGhpcy5zbGlkZXJPcHRpb25zLmZsb29yKSA/IG51bGwgOiB0aGlzLmJyZWFrcG9pbnRzW3RoaXMubWluU2VsZWN0ZWRdO1xuICAgIGludGVydmFsLm1heCA9ICghdGhpcy51cHBlckJvdW5kZWQgJiYgdGhpcy5tYXhTZWxlY3RlZCA9PT0gdGhpcy5zbGlkZXJPcHRpb25zLmNlaWwpID8gbnVsbCA6IHRoaXMuYnJlYWtwb2ludHNbdGhpcy5tYXhTZWxlY3RlZF07XG4gICAgdGhpcy5yYW5nZUNoYW5nZS5lbWl0KGludGVydmFsKTtcbiAgfVxuXG4gIC8qKiBJbml0aWFsaXphdGlvbiBmcm9tIGJ1Y2tldCBhZ2dyZWdhdGlvbiAqL1xuICBwcml2YXRlIF9pbml0KCk6IHZvaWQge1xuICAgIGlmICh0aGlzLmRhdGFTb3VyY2UubGVuZ3RoID4gMCkge1xuICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLmRhdGFTb3VyY2UubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgY29uc3QgYnVja2V0ID0gdGhpcy5kYXRhU291cmNlW2ldO1xuICAgICAgICBjb25zdCBidWNrZXRSYW5nZVZhbHVlcyA9IGJ1Y2tldC5rZXkuc3BsaXQoJy0nKTtcblxuICAgICAgICBpZiAoYnVja2V0UmFuZ2VWYWx1ZXMubGVuZ3RoID09PSAxKSB7XG4gICAgICAgICAgLy8gQWdncmVnYXRpb24gaXMgYW4gaGlzdG9ncmFtXG4gICAgICAgICAgdGhpcy5fYWRkQnJlYWtwb2ludFZhbHVlKC1JbmZpbml0eSk7XG4gICAgICAgICAgdGhpcy5fYWRkQnJlYWtwb2ludFZhbHVlKCtidWNrZXRSYW5nZVZhbHVlc1swXSk7XG4gICAgICAgICAgdGhpcy5sb3dlckJvdW5kZWQgPSBmYWxzZTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAvLyBBZ2dyZWdhdGlvbiBpcyBhIGxpc3Qgb2YgcmFuZ2VzXG4gICAgICAgICAgaWYgKGJ1Y2tldFJhbmdlVmFsdWVzWzBdID09PSAnKicpIHtcbiAgICAgICAgICAgIHRoaXMuX2FkZEJyZWFrcG9pbnRWYWx1ZSgtSW5maW5pdHkpO1xuICAgICAgICAgICAgdGhpcy5fYWRkQnJlYWtwb2ludFZhbHVlKCtidWNrZXRSYW5nZVZhbHVlc1sxXSk7XG4gICAgICAgICAgICB0aGlzLmxvd2VyQm91bmRlZCA9IGZhbHNlO1xuICAgICAgICAgIH0gZWxzZSBpZiAoYnVja2V0UmFuZ2VWYWx1ZXNbMV0gPT09ICcqJykge1xuICAgICAgICAgICAgdGhpcy5fYWRkQnJlYWtwb2ludFZhbHVlKCtidWNrZXRSYW5nZVZhbHVlc1swXSk7XG4gICAgICAgICAgICB0aGlzLl9hZGRCcmVha3BvaW50VmFsdWUoSW5maW5pdHkpO1xuICAgICAgICAgICAgdGhpcy51cHBlckJvdW5kZWQgPSBmYWxzZTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5fYWRkQnJlYWtwb2ludFZhbHVlKCtidWNrZXRSYW5nZVZhbHVlc1swXSk7XG4gICAgICAgICAgICB0aGlzLl9hZGRCcmVha3BvaW50VmFsdWUoK2J1Y2tldFJhbmdlVmFsdWVzWzFdKTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBjb25zdCBicmVha3BvaW50c0Zpbml0ZVZhbHVlcyA9IHRoaXMuYnJlYWtwb2ludHMuZmlsdGVyKGl0ZW0gPT4gaXRlbSAhPT0gLUluZmluaXR5ICYmIGl0ZW0gIT09IEluZmluaXR5KTtcbiAgICAgICAgdGhpcy5icmVha3BvaW50RmluaXRlTWluVmFsdWUgPSBNYXRoLm1pbi5hcHBseShudWxsLCBicmVha3BvaW50c0Zpbml0ZVZhbHVlcyk7XG4gICAgICAgIHRoaXMuYnJlYWtwb2ludEZpbml0ZU1heFZhbHVlID0gTWF0aC5tYXguYXBwbHkobnVsbCwgYnJlYWtwb2ludHNGaW5pdGVWYWx1ZXMpO1xuICAgICAgICB0aGlzLmhpc3RvZ3JhbURhdGEucHVzaCh7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeDogaSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB5OiBidWNrZXQuY291bnRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICB9XG4gICAgICB0aGlzLm1pblNlbGVjdGVkID0gMDtcbiAgICAgIHRoaXMuc2xpZGVyT3B0aW9ucy5mbG9vciA9IDA7XG4gICAgICB0aGlzLm1heFNlbGVjdGVkID0gdGhpcy5oaXN0b2dyYW1EYXRhLmxlbmd0aDtcbiAgICAgIHRoaXMuc2xpZGVyT3B0aW9ucy5jZWlsID0gdGhpcy5oaXN0b2dyYW1EYXRhLmxlbmd0aDtcbiAgICAgIHRoaXMuc2xpZGVyT3B0aW9ucy50cmFuc2xhdGUgPSAodmFsdWU6IG51bWJlciwgbGFiZWw6IExhYmVsVHlwZSkgPT4ge1xuICAgICAgICBpZiAodGhpcy5icmVha3BvaW50c1t2YWx1ZV0gPT09IC1JbmZpbml0eSkge1xuICAgICAgICAgIHJldHVybiAnPCAnICsgdGhpcy5icmVha3BvaW50RmluaXRlTWluVmFsdWU7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHRoaXMuYnJlYWtwb2ludHNbdmFsdWVdID09PSBJbmZpbml0eSkge1xuICAgICAgICAgIHJldHVybiAnPiAnICsgdGhpcy5icmVha3BvaW50RmluaXRlTWF4VmFsdWU7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRoaXMuYnJlYWtwb2ludHNbdmFsdWVdICsgJyc7XG4gICAgICB9O1xuXG4gICAgICAvLyBTbGlkZXIgY2FuIG9ubHkgYmUgaW5pdGlhbGl6ZWQgb25jZS5cbiAgICAgIHRoaXMuc2xpZGVySW5pdGlhbGl6ZWQgPSB0cnVlO1xuICAgIH1cbiAgICB0aGlzLnNsaWRlck9wdGlvbnMubWluUmFuZ2UgPSB0aGlzLm1pblJhbmdlO1xuICAgIHRoaXMuc2xpZGVyT3B0aW9ucy5hbmltYXRlID0gZmFsc2U7XG4gICAgdGhpcy5zbGlkZXJPcHRpb25zLm5vU3dpdGNoaW5nID0gdHJ1ZTtcbiAgICB0aGlzLnNsaWRlck9wdGlvbnMucHVzaFJhbmdlID0gdHJ1ZTtcbiAgICB0aGlzLnNsaWRlck9wdGlvbnMuaGlkZUxpbWl0TGFiZWxzID0gdHJ1ZTtcbiAgfVxuXG4gIC8qKiBBZGRzIHZhbHVlIHRvIGJyZWFrcG9pbnRzIGFycmF5LlxuICAgKiAgQ2hlY2tzIGZpcnN0IGlmIHZhbHVlIGlzIGFscmVhZHkgcmVnaXN0ZXJlZC4qL1xuICBwcml2YXRlIF9hZGRCcmVha3BvaW50VmFsdWUodmFsdWU6IG51bWJlcik6IHZvaWQge1xuICAgIGlmICh0aGlzLmJyZWFrcG9pbnRzLmluZGV4T2YodmFsdWUpID09PSAtMSkge1xuICAgICAgdGhpcy5icmVha3BvaW50cy5wdXNoKHZhbHVlKTtcbiAgICB9XG4gIH1cblxuXG59XG5cblxuIiwiaW1wb3J0IHtOZ01vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0Jvb3RzdGFydENoZWNrbGlzdEZpbHRlckNvbXBvbmVudH0gZnJvbSAnLi9jb21wb25lbnRzL2Jvb3RzdGFydC1jaGVja2xpc3QtZmlsdGVyL2Jvb3RzdGFydC1jaGVja2xpc3QtZmlsdGVyLmNvbXBvbmVudCc7XG5pbXBvcnQge0Jvb3RzdGFydEhpc3RvZ3JhbVNsaWRlckZpbHRlckNvbXBvbmVudH0gZnJvbSAnLi9jb21wb25lbnRzL2Jvb3RzdGFydC1oaXN0b2dyYW0tc2xpZGVyLWZpbHRlci9ib290c3RhcnQtaGlzdG9ncmFtLXNsaWRlci1maWx0ZXIuY29tcG9uZW50JztcbmltcG9ydCB7TWF0QmFkZ2VNb2R1bGUsIE1hdEJ1dHRvbk1vZHVsZSwgTWF0Q2hlY2tib3hNb2R1bGUsIE1hdEljb25Nb2R1bGUsIE1hdFByb2dyZXNzQmFyTW9kdWxlLCBNYXRUcmVlTW9kdWxlfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XG5pbXBvcnQge0NvbW1vbk1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7VHJhbnNsYXRlTW9kdWxlfSBmcm9tICdAbmd4LXRyYW5zbGF0ZS9jb3JlJztcbmltcG9ydCB7Tmc1U2xpZGVyTW9kdWxlfSBmcm9tICduZzUtc2xpZGVyJztcblxuQE5nTW9kdWxlKHtcbiAgICAgICAgICAgIGltcG9ydHMgICAgIDogW1xuICAgICAgICAgICAgICBDb21tb25Nb2R1bGUsXG4gICAgICAgICAgICAgIE1hdFRyZWVNb2R1bGUsXG4gICAgICAgICAgICAgIE1hdENoZWNrYm94TW9kdWxlLFxuICAgICAgICAgICAgICBNYXRQcm9ncmVzc0Jhck1vZHVsZSxcbiAgICAgICAgICAgICAgTWF0SWNvbk1vZHVsZSxcbiAgICAgICAgICAgICAgTWF0QnV0dG9uTW9kdWxlLFxuICAgICAgICAgICAgICBNYXRCYWRnZU1vZHVsZSxcbiAgICAgICAgICAgICAgVHJhbnNsYXRlTW9kdWxlLFxuICAgICAgICAgICAgICBOZzVTbGlkZXJNb2R1bGVcbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBkZWNsYXJhdGlvbnM6IFtcbiAgICAgICAgICAgICAgQm9vdHN0YXJ0Q2hlY2tsaXN0RmlsdGVyQ29tcG9uZW50LFxuICAgICAgICAgICAgICBCb290c3RhcnRIaXN0b2dyYW1TbGlkZXJGaWx0ZXJDb21wb25lbnRcbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBleHBvcnRzICAgICA6IFtcbiAgICAgICAgICAgICAgQm9vdHN0YXJ0Q2hlY2tsaXN0RmlsdGVyQ29tcG9uZW50LFxuICAgICAgICAgICAgICBCb290c3RhcnRIaXN0b2dyYW1TbGlkZXJGaWx0ZXJDb21wb25lbnRcbiAgICAgICAgICAgIF1cbiAgICAgICAgICB9KVxuZXhwb3J0IGNsYXNzIEJvb3RzdGFydEZpbHRlck1vZHVsZSB7XG59XG4iXSwibmFtZXMiOlsidHNsaWJfMS5fX2V4dGVuZHMiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBR0E7OztBQUFBO0lBV0U7UUFDRSxJQUFJLENBQUMsR0FBRyxHQUFHLEVBQUUsQ0FBQztRQUNkLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDO1FBQ2YsSUFBSSxDQUFDLFVBQVUsR0FBRyxDQUFDLENBQUM7UUFDcEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUM7S0FDbkI7Ozs7Ozs7Ozs7OztJQVFELDZCQUFVOzs7Ozs7SUFBVixVQUFXLE1BQWdCO1FBQ3pCLElBQUksQ0FBQyxHQUFHLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQztRQUN0QixJQUFJLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUM7UUFDMUIsSUFBSSxDQUFDLFVBQVUsR0FBRyxNQUFNLENBQUMsVUFBVSxDQUFDO1FBQ3BDLElBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBQSxJQUFJOztZQUNwQyxJQUFNLE1BQU0sR0FBRyxJQUFJLFFBQVEsRUFBRSxDQUFDO1lBQzlCLE1BQU0sQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDeEIsT0FBTyxNQUFNLENBQUM7U0FDZixDQUFDLENBQUM7S0FDSjs7Ozs7Ozs7Ozs7Ozs7SUFRRCxxQ0FBa0I7Ozs7Ozs7SUFBbEIsVUFBbUIsTUFBZ0IsRUFBRSxXQUFtQjtRQUF4RCxpQkFrQkM7UUFqQkMsSUFBSSxNQUFNLEtBQUssU0FBUyxFQUFFO1lBQ3hCLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDO1lBQ2YsSUFBSSxDQUFDLFVBQVUsR0FBRyxDQUFDLENBQUM7WUFDcEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLENBQUMsa0JBQWtCLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQyxHQUFBLENBQUMsQ0FBQztTQUNuRTthQUFNO1lBQ0wsSUFBSSxDQUFDLEtBQUssR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDO1lBQzFCLElBQUksQ0FBQyxVQUFVLEdBQUcsTUFBTSxDQUFDLEtBQUssR0FBRyxXQUFXLENBQUM7WUFDN0MsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsVUFBQSxHQUFHOztnQkFDdEIsSUFBTSxxQkFBcUIsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxJQUFJLENBQUMsS0FBSyxHQUFBLENBQUMsQ0FBQyxDQUFDOztnQkFDM0YsSUFBTSxRQUFRLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxJQUFJLENBQUMsR0FBRyxLQUFLLEdBQUcsQ0FBQyxHQUFHLEdBQUEsQ0FBQyxDQUFDO2dCQUNyRSxJQUFJLFFBQVEsQ0FBQyxNQUFNLElBQUksQ0FBQyxFQUFFO29CQUN4QixHQUFHLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxFQUFFLHFCQUFxQixDQUFDLENBQUM7aUJBQzVEO3FCQUFNO29CQUNMLE9BQU8sQ0FBQyxLQUFLLENBQUMsaUNBQWlDLEdBQUcsS0FBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2lCQUM3RDthQUNGLENBQUMsQ0FBQztTQUNKO0tBQ0Y7bUJBOURIO0lBZ0VDOzs7Ozs7QUNoRUQsSUFJQTs7Ozs7dUJBR3dCLEVBQUU7Ozs7OztJQUd4Qiw2Q0FBZTs7OztJQUFmLFVBQWdCLFdBQWdDO1FBQzlDLElBQUksQ0FBQyxPQUFPLEdBQUcsV0FBVyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBQSxJQUFJOztZQUN6QyxJQUFNLE1BQU0sR0FBRyxJQUFJLFFBQVEsRUFBRSxDQUFDO1lBQzlCLE1BQU0sQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDeEIsT0FBTyxNQUFNLENBQUM7U0FDZixDQUFDLENBQUM7S0FDSjs7Ozs7SUFFRCxxREFBdUI7Ozs7SUFBdkIsVUFBd0IsV0FBZ0M7UUFDdEQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsVUFBQSxNQUFNOztZQUN6QixJQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsV0FBVyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxJQUFJLENBQUMsS0FBSyxHQUFBLENBQUMsQ0FBQyxDQUFDOztZQUN0RixJQUFNLFFBQVEsR0FBRyxXQUFXLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLE1BQU0sQ0FBQyxHQUFHLEtBQUssSUFBSSxDQUFDLEdBQUcsR0FBQSxDQUFDLENBQUM7WUFDN0UsSUFBSSxRQUFRLENBQUMsTUFBTSxJQUFJLENBQUMsRUFBRTtnQkFDeEIsTUFBTSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBRSxXQUFXLENBQUMsQ0FBQzthQUNyRDtpQkFBTTtnQkFDTCxPQUFPLENBQUMsS0FBSyxDQUFDLHVDQUF1QyxDQUFDLENBQUM7YUFDeEQ7U0FDRixDQUFDLENBQUM7S0FDSjs4QkE1Qkg7SUE4QkM7Ozs7Ozs7Ozs7QUN4QkQ7Ozs7QUFBQTtJQVVFLDJCQUFZLGdCQUFrQzs7Ozt1QkFGZixFQUFFO1FBRy9CLElBQUksQ0FBQyxHQUFHLEdBQUcsZ0JBQWdCLENBQUMsR0FBRyxDQUFDO1FBQ2hDLElBQUksQ0FBQyxLQUFLLEdBQUcsZ0JBQWdCLENBQUMsS0FBSyxDQUFDO0tBQ3JDOzRCQW5CSDtJQW9CQzs7Ozs7Ozs7O0FDZkQ7OztBQUFBO0lBQXNDQSxvQ0FBUTs7OzsyQkFMOUM7RUFLc0MsUUFBUSxFQUc3Qzs7Ozs7OztJQzRIQztRQUFBLGlCQUtDOzs7O3dCQW5Db0IsSUFBSSxZQUFZLEVBQXVCOzs7O2tDQUd2QyxJQUFJLGNBQWMsQ0FBVyxJQUFJLENBQUM7Ozs7Z0NBR3BDLElBQUksY0FBYyxDQUFvQixJQUFJLENBQUM7Ozs7eUNBR2xDLElBQUksY0FBYyxDQUFXLElBQUksQ0FBQzs7OzsyQkFHaEQsSUFBSSxHQUFHLEVBQThCOzs7OzZCQUduQyxJQUFJLEdBQUcsRUFBOEI7MEJBRVgsSUFBSSxlQUFlLENBQWEsRUFBRSxDQUFDO3dCQVFsRSxVQUFDLElBQXNCLElBQUssT0FBQSxJQUFJLENBQUMsS0FBSyxHQUFBOzRCQUNsQyxVQUFDLElBQXNCLElBQUssT0FBQSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxDQUFDLEdBQUE7MkJBQ3BELFVBQUMsSUFBYyxJQUFpQixPQUFBLElBQUksQ0FBQyxPQUFPLEdBQUE7d0JBQy9DLFVBQUMsQ0FBUyxFQUFFLFNBQTJCLElBQUssT0FBQSxTQUFTLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxDQUFDLEdBQUE7UUFHakYsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLGdCQUFnQixDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUNqSCxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksZUFBZSxDQUFtQixJQUFJLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUMzRixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUkscUJBQXFCLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDbEYsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxLQUFJLENBQUMsVUFBVSxDQUFDLElBQUksR0FBRyxJQUFJLEdBQUEsQ0FBQyxDQUFDO0tBQ2hFOzs7O0lBRUQsb0RBQVE7OztJQUFSO1FBQ0UsSUFBSSxJQUFJLENBQUMsaUJBQWlCLEtBQUssU0FBUyxFQUFFO1lBQ3hDLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxFQUFFLENBQUM7U0FDN0I7S0FDRjs7OztJQUVELHFEQUFTOzs7SUFBVDtRQUNFLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLENBQUM7S0FDaEQ7Ozs7Ozs7SUFHRCxrRUFBc0I7Ozs7O0lBQXRCLFVBQXVCLElBQXNCO1FBQTdDLGlCQU1DOztRQUxDLElBQU0sV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzFELElBQUksV0FBVyxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7WUFDNUIsT0FBTyxLQUFLLENBQUM7U0FDZDtRQUNELE9BQU8sV0FBVyxDQUFDLEtBQUssQ0FBQyxVQUFBLEtBQUssSUFBSSxPQUFBLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUEsQ0FBQyxDQUFDO0tBQzlFOzs7Ozs7O0lBR0Qsd0VBQTRCOzs7OztJQUE1QixVQUE2QixJQUFzQjtRQUFuRCxpQkFJQzs7UUFIQyxJQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQzs7UUFDMUQsSUFBTSxNQUFNLEdBQUcsV0FBVyxDQUFDLElBQUksQ0FBQyxVQUFBLEtBQUssSUFBSSxPQUFBLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUEsQ0FBQyxDQUFDO1FBQ3BGLE9BQU8sTUFBTSxJQUFJLENBQUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxDQUFDO0tBQ3JEOzs7Ozs7O0lBR0QsaUVBQXFCOzs7OztJQUFyQixVQUFzQixJQUFzQjtRQUMxQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDOztRQUNyQyxJQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMxRCxJQUFJLENBQUMsa0JBQWtCLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQztjQUN0QyxDQUFBLEtBQUEsSUFBSSxDQUFDLGtCQUFrQixFQUFDLE1BQU0sb0JBQUksV0FBVyxLQUM3QyxDQUFBLEtBQUEsSUFBSSxDQUFDLGtCQUFrQixFQUFDLFFBQVEsb0JBQUksV0FBVyxFQUFDLENBQUM7UUFDbkQsSUFBSSxDQUFDLHlCQUF5QixDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3JDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQzs7S0FDcEI7Ozs7Ozs7SUFHRCxxRUFBeUI7Ozs7O0lBQXpCLFVBQTBCLElBQXNCO1FBQzlDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDckMsSUFBSSxDQUFDLHlCQUF5QixDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3JDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztLQUNwQjs7OztJQUVPLHVEQUFXOzs7O1FBQ2pCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUM5QixJQUFJLENBQUMseUJBQXlCLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDdkMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUMxRCxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUM7Ozs7OztJQUc3Qyw4REFBa0I7Ozs7Y0FBQyxPQUFtQjs7UUFDNUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxVQUFBLE9BQU87O1lBQ3JCLElBQU0sV0FBVyxxQkFBcUIsT0FBTyxFQUFDO1lBQzlDLElBQUksS0FBSSxDQUFDLGtCQUFrQixDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsRUFBRTtnQkFDL0MsSUFBSSxLQUFJLENBQUMsc0JBQXNCLENBQUMsV0FBVyxDQUFDLEVBQUU7O29CQUM1QyxJQUFNLFdBQVcsR0FBRyxLQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQztvQkFDakUsQ0FBQSxLQUFBLEtBQUksQ0FBQyx5QkFBeUIsRUFBQyxNQUFNLG9CQUFJLFdBQVcsR0FBRTtpQkFDdkQ7O2dCQUVELElBQUksUUFBTSxHQUE0QixLQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUN2RSxPQUFPLFFBQU0sRUFBRTtvQkFDYixLQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxRQUFNLENBQUMsQ0FBQyxDQUFDO29CQUNsQyxRQUFNLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxRQUFNLENBQUMsQ0FBQztpQkFDdEM7Z0JBRUQsSUFBSSxDQUFDLEtBQUksQ0FBQyx5QkFBeUIsQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLEVBQUU7O29CQUN2RCxJQUFNLE1BQU0sR0FBRyxJQUFJLGlCQUFpQixDQUFDLFdBQVcsQ0FBQyxDQUFDOztvQkFDbEQsSUFBSSxTQUFTLEdBQTRCLEtBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLENBQUM7b0JBQzFFLE9BQU8sU0FBUyxFQUFFO3dCQUNoQixNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7d0JBQ3RELFNBQVMsR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxDQUFDO3FCQUM1QztvQkFDRCxLQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO29CQUNyQyxLQUFJLENBQUMseUJBQXlCLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2lCQUNoRDthQUNGOztTQUNGLENBQUMsQ0FBQzs7Ozs7Ozs7SUFJRyx3REFBWTs7Ozs7O2NBQUMsSUFBYyxFQUFFLEtBQWE7UUFDaEQsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUU7WUFDdkIsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLEdBQUcsRUFBOEIsQ0FBQztTQUM1RDtRQUNELElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFO1lBQ3JCLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxHQUFHLEVBQThCLENBQUM7U0FDMUQ7O1FBQ0QsSUFBTSxZQUFZLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7O1FBQ2xELElBQU0sUUFBUSxHQUFHLFlBQVksSUFBSSxZQUFZLENBQUMsR0FBRyxLQUFLLElBQUksQ0FBQyxHQUFHO2NBQzNDLFlBQVk7Y0FDWixJQUFJLGdCQUFnQixFQUFFLENBQUM7UUFDMUMsUUFBUSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDO1FBQ3hCLFFBQVEsQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztRQUM1QixRQUFRLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7UUFDaEMsUUFBUSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO1FBQ3RDLFFBQVEsQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNyQyxJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFDdkMsT0FBTyxRQUFRLENBQUM7Ozs7Ozs7SUFJVixxRUFBeUI7Ozs7O2NBQUMsSUFBc0I7O1FBQ3RELElBQUksTUFBTSxHQUE0QixJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2hFLE9BQU8sTUFBTSxFQUFFO1lBQ2IsSUFBSSxDQUFDLHVCQUF1QixDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ3JDLE1BQU0sR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1NBQ3RDOzs7Ozs7O0lBSUssbUVBQXVCOzs7OztjQUFDLElBQXNCOztRQUNwRCxJQUFNLFlBQVksR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzlELElBQUksWUFBWSxJQUFJLENBQUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ3RELElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDeEM7YUFBTSxJQUFJLENBQUMsWUFBWSxJQUFJLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUM3RCxJQUFJLENBQUMsa0JBQWtCLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3RDOzs7Ozs7O0lBSUssMERBQWM7Ozs7O2NBQUMsSUFBc0I7O1FBQzNDLElBQU0sWUFBWSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7UUFFekMsSUFBSSxZQUFZLEdBQUcsQ0FBQyxFQUFFO1lBQ3BCLE9BQU8sSUFBSSxDQUFDO1NBQ2I7O1FBRUQsSUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUVoRSxLQUFLLElBQUksQ0FBQyxHQUFHLFVBQVUsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFOztZQUNwQyxJQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUVsRCxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEdBQUcsWUFBWSxFQUFFO2dCQUM3QyxPQUFPLFdBQVcsQ0FBQzthQUNwQjtTQUNGO1FBQ0QsT0FBTyxJQUFJLENBQUM7OztnQkEzUWYsU0FBUyxTQUFDO29CQUNFLFFBQVEsRUFBSyw0QkFBNEI7b0JBQ3pDLFFBQVEsRUFBRSw2dkZBOEV0QjtvQkFDWSxNQUFNLEVBQUUsQ0FBQyxnbkJBQWduQixDQUFDO2lCQUMzbkI7Ozs7OzhCQUlULEtBQUs7b0NBR0wsS0FBSzsyQkFHTCxNQUFNOzs0Q0F0R1Q7Ozs7Ozs7QUNBQSxJQUFBOzs7Z0NBQUE7SUFHQzs7Ozs7O0FDSEQsSUFBQTtJQUlFO0tBQ0M7bUJBTEg7SUFPQzs7Ozs7Ozs7Ozs7QUNQRDtJQXlDRTtRQUFBLGlCQWdCQzt3QkFuQzJCLENBQUM7MEJBY2EsSUFBSSxlQUFlLENBQWEsRUFBRSxDQUFDO1FBTTNFLElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDO1FBQ3RCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQztRQUNuQyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDO1FBQy9CLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUV0QyxJQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztRQUNyQixJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxVQUFBLElBQUk7WUFDNUIsSUFBSSxDQUFDLEtBQUksQ0FBQyxpQkFBaUIsRUFBRTtnQkFDM0IsS0FBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7Z0JBQ3ZCLEtBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQzthQUNkO1NBQ0YsQ0FBQyxDQUFDO0tBQ0o7Ozs7SUFFRCwyREFBUzs7O0lBQVQ7UUFDRSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsT0FBTyxDQUFDLENBQUM7S0FDdEQ7Ozs7Ozs7Ozs7SUFPRCw2REFBVzs7Ozs7SUFBWDs7UUFDRSxJQUFNLFFBQVEsR0FBRyxJQUFJLFFBQVEsRUFBRSxDQUFDO1FBQ2hDLFFBQVEsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLElBQUksSUFBSSxDQUFDLFdBQVcsS0FBSyxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDakksUUFBUSxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUMsV0FBVyxLQUFLLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUNoSSxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztLQUNqQzs7Ozs7SUFHTyx1REFBSzs7Ozs7O1FBQ1gsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDOUIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFOztnQkFDL0MsSUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQzs7Z0JBQ2xDLElBQU0saUJBQWlCLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBRWhELElBQUksaUJBQWlCLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTs7b0JBRWxDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUNwQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNoRCxJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztpQkFDM0I7cUJBQU07O29CQUVMLElBQUksaUJBQWlCLENBQUMsQ0FBQyxDQUFDLEtBQUssR0FBRyxFQUFFO3dCQUNoQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQzt3QkFDcEMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDaEQsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7cUJBQzNCO3lCQUFNLElBQUksaUJBQWlCLENBQUMsQ0FBQyxDQUFDLEtBQUssR0FBRyxFQUFFO3dCQUN2QyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUNoRCxJQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxDQUFDLENBQUM7d0JBQ25DLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO3FCQUMzQjt5QkFBTTt3QkFDTCxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUNoRCxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO3FCQUNqRDtpQkFDRjs7Z0JBRUQsSUFBTSx1QkFBdUIsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLElBQUksS0FBSyxDQUFDLFFBQVEsSUFBSSxJQUFJLEtBQUssUUFBUSxHQUFBLENBQUMsQ0FBQztnQkFDekcsSUFBSSxDQUFDLHdCQUF3QixHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSx1QkFBdUIsQ0FBQyxDQUFDO2dCQUM5RSxJQUFJLENBQUMsd0JBQXdCLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLHVCQUF1QixDQUFDLENBQUM7Z0JBQzlFLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDO29CQUNFLENBQUMsRUFBRSxDQUFDO29CQUNKLENBQUMsRUFBRSxNQUFNLENBQUMsS0FBSztpQkFDaEIsQ0FBQyxDQUFDO2FBQzVCO1lBQ0QsSUFBSSxDQUFDLFdBQVcsR0FBRyxDQUFDLENBQUM7WUFDckIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDO1lBQzdCLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUM7WUFDN0MsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUM7WUFDcEQsSUFBSSxDQUFDLGFBQWEsQ0FBQyxTQUFTLEdBQUcsVUFBQyxLQUFhLEVBQUUsS0FBZ0I7Z0JBQzdELElBQUksS0FBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRTtvQkFDekMsT0FBTyxJQUFJLEdBQUcsS0FBSSxDQUFDLHdCQUF3QixDQUFDO2lCQUM3QztnQkFDRCxJQUFJLEtBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEtBQUssUUFBUSxFQUFFO29CQUN4QyxPQUFPLElBQUksR0FBRyxLQUFJLENBQUMsd0JBQXdCLENBQUM7aUJBQzdDO2dCQUNELE9BQU8sS0FBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFLENBQUM7YUFDckMsQ0FBQzs7WUFHRixJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO1NBQy9CO1FBQ0QsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUM1QyxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7UUFDbkMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO1FBQ3RDLElBQUksQ0FBQyxhQUFhLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUNwQyxJQUFJLENBQUMsYUFBYSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7Ozs7Ozs7O0lBS3BDLHFFQUFtQjs7Ozs7O2NBQUMsS0FBYTtRQUN2QyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFO1lBQzFDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQzlCOzs7Z0JBcklKLFNBQVMsU0FBQztvQkFDRSxRQUFRLEVBQUssbUNBQW1DO29CQUNoRCxRQUFRLEVBQUUsK1RBT3RCO29CQUNZLE1BQU0sRUFBRSxDQUFDLEVBQUUsQ0FBQztpQkFDYjs7Ozs7b0NBR1QsS0FBSzsyQkFDTCxLQUFLOzhCQWdCTCxNQUFNOztrREF0Q1Q7Ozs7Ozs7QUNBQTs7OztnQkFRQyxRQUFRLFNBQUM7b0JBQ0UsT0FBTyxFQUFPO3dCQUNaLFlBQVk7d0JBQ1osYUFBYTt3QkFDYixpQkFBaUI7d0JBQ2pCLG9CQUFvQjt3QkFDcEIsYUFBYTt3QkFDYixlQUFlO3dCQUNmLGNBQWM7d0JBQ2QsZUFBZTt3QkFDZixlQUFlO3FCQUNoQjtvQkFDRCxZQUFZLEVBQUU7d0JBQ1osaUNBQWlDO3dCQUNqQyx1Q0FBdUM7cUJBQ3hDO29CQUNELE9BQU8sRUFBTzt3QkFDWixpQ0FBaUM7d0JBQ2pDLHVDQUF1QztxQkFDeEM7aUJBQ0Y7O2dDQTVCWDs7Ozs7Ozs7Ozs7Ozs7OyJ9