/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var SliderRange = /** @class */ (function () {
    function SliderRange(min, max) {
        this.min = min;
        this.max = max;
    }
    return SliderRange;
}());
export { SliderRange };
if (false) {
    /** @type {?} */
    SliderRange.prototype.min;
    /** @type {?} */
    SliderRange.prototype.max;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2xpZGVyLXJhbmdlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vYm9vdHN0YXJ0LWZpbHRlci8iLCJzb3VyY2VzIjpbImxpYi9tb2RlbHMvdXRpbHMvc2xpZGVyLXJhbmdlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxJQUFBO0lBS0UscUJBQVksR0FBVyxFQUFFLEdBQVc7UUFDbEMsSUFBSSxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUM7UUFDZixJQUFJLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQztLQUNoQjtzQkFSSDtJQVNDLENBQUE7QUFURCx1QkFTQyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBjbGFzcyBTbGlkZXJSYW5nZSB7XG4gIG1pbjogbnVtYmVyO1xuICBtYXg6IG51bWJlcjtcblxuXG4gIGNvbnN0cnVjdG9yKG1pbjogbnVtYmVyLCBtYXg6IG51bWJlcikge1xuICAgIHRoaXMubWluID0gbWluO1xuICAgIHRoaXMubWF4ID0gbWF4O1xuICB9XG59XG4iXX0=