/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
export class SliderRange {
    /**
     * @param {?} min
     * @param {?} max
     */
    constructor(min, max) {
        this.min = min;
        this.max = max;
    }
}
if (false) {
    /** @type {?} */
    SliderRange.prototype.min;
    /** @type {?} */
    SliderRange.prototype.max;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2xpZGVyLXJhbmdlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vYm9vdHN0YXJ0LWZpbHRlci8iLCJzb3VyY2VzIjpbImxpYi9tb2RlbHMvdXRpbHMvc2xpZGVyLXJhbmdlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxNQUFNOzs7OztJQUtKLFlBQVksR0FBVyxFQUFFLEdBQVc7UUFDbEMsSUFBSSxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUM7UUFDZixJQUFJLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQztLQUNoQjtDQUNGIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIFNsaWRlclJhbmdlIHtcbiAgbWluOiBudW1iZXI7XG4gIG1heDogbnVtYmVyO1xuXG5cbiAgY29uc3RydWN0b3IobWluOiBudW1iZXIsIG1heDogbnVtYmVyKSB7XG4gICAgdGhpcy5taW4gPSBtaW47XG4gICAgdGhpcy5tYXggPSBtYXg7XG4gIH1cbn1cbiJdfQ==