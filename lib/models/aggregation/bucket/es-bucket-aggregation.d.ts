import { EsBucket } from './es-bucket';
import { EsAggregation } from '../es-aggregation';
export declare class EsBucketAggregation implements EsAggregation {
    /** List of buckets */
    buckets: EsBucket[];
    initAggregation(aggregation: EsBucketAggregation): void;
    updateAggregationCounts(aggregation: EsBucketAggregation): void;
}
