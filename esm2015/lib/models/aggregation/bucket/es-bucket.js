/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * Bucket response for a given Elasticsearch aggregation.
 */
export class EsBucket {
    constructor() {
        this.key = '';
        this.count = 0;
        this.proportion = 0;
        this.buckets = [];
    }
    /**
     * Initialization of the bucket from a backend response.
     * Used to keep the functions of this class.
     * @param {?} bucket Response from backend used for initialization.
     * @return {?}
     */
    initBucket(bucket) {
        this.key = bucket.key;
        this.count = bucket.count;
        this.proportion = bucket.proportion;
        this.buckets = bucket.buckets.map(item => {
            /** @type {?} */
            const result = new EsBucket();
            result.initBucket(item);
            return result;
        });
    }
    /**
     * Update counts for buckets and all sub-buckets given a bucket from backend response.
     * Used to keep the functions of this class.
     * @param {?} bucket Response from backend used for count update.
     * @param {?} maxDocCount Maximum Doc Count for the current bucket level, used to compute proportion
     * @return {?}
     */
    updateBucketCounts(bucket, maxDocCount) {
        if (bucket === undefined) {
            this.count = 0;
            this.proportion = 0;
            this.buckets.forEach(sub => sub.updateBucketCounts(undefined, 0));
        }
        else {
            this.count = bucket.count;
            this.proportion = bucket.count / maxDocCount;
            this.buckets.forEach(sub => {
                /** @type {?} */
                const subBucketsmaxDocCount = Math.max.apply(null, bucket.buckets.map(item => item.count));
                /** @type {?} */
                const matching = bucket.buckets.filter(item => item.key === sub.key);
                if (matching.length <= 1) {
                    sub.updateBucketCounts(matching[0], subBucketsmaxDocCount);
                }
                else {
                    console.error('Multiple buckets found for key ' + this.key);
                }
            });
        }
    }
}
if (false) {
    /**
     * Bucket key
     * @type {?}
     */
    EsBucket.prototype.key;
    /**
     * Doc count for the bucket
     * @type {?}
     */
    EsBucket.prototype.count;
    /**
     * Proportion used for progress bar
     * @type {?}
     */
    EsBucket.prototype.proportion;
    /**
     * List of sub buckets
     * @type {?}
     */
    EsBucket.prototype.buckets;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXMtYnVja2V0LmpzIiwic291cmNlUm9vdCI6Im5nOi8vYm9vdHN0YXJ0LWZpbHRlci8iLCJzb3VyY2VzIjpbImxpYi9tb2RlbHMvYWdncmVnYXRpb24vYnVja2V0L2VzLWJ1Y2tldC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBR0EsTUFBTTtJQVdKO1FBQ0UsSUFBSSxDQUFDLEdBQUcsR0FBRyxFQUFFLENBQUM7UUFDZCxJQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztRQUNmLElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDO1FBQ3BCLElBQUksQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDO0tBQ25COzs7Ozs7O0lBUUQsVUFBVSxDQUFDLE1BQWdCO1FBQ3pCLElBQUksQ0FBQyxHQUFHLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQztRQUN0QixJQUFJLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUM7UUFDMUIsSUFBSSxDQUFDLFVBQVUsR0FBRyxNQUFNLENBQUMsVUFBVSxDQUFDO1FBQ3BDLElBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEVBQUU7O1lBQ3ZDLE1BQU0sTUFBTSxHQUFHLElBQUksUUFBUSxFQUFFLENBQUM7WUFDOUIsTUFBTSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN4QixNQUFNLENBQUMsTUFBTSxDQUFDO1NBQ2YsQ0FBQyxDQUFDO0tBQ0o7Ozs7Ozs7O0lBUUQsa0JBQWtCLENBQUMsTUFBZ0IsRUFBRSxXQUFtQjtRQUN0RCxFQUFFLENBQUMsQ0FBQyxNQUFNLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQztZQUN6QixJQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztZQUNmLElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDO1lBQ3BCLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLGtCQUFrQixDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ25FO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixJQUFJLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUM7WUFDMUIsSUFBSSxDQUFDLFVBQVUsR0FBRyxNQUFNLENBQUMsS0FBSyxHQUFHLFdBQVcsQ0FBQztZQUM3QyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsRUFBRTs7Z0JBQ3pCLE1BQU0scUJBQXFCLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7O2dCQUMzRixNQUFNLFFBQVEsR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLEtBQUssR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUNyRSxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ3pCLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLEVBQUUscUJBQXFCLENBQUMsQ0FBQztpQkFDNUQ7Z0JBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ04sT0FBTyxDQUFDLEtBQUssQ0FBQyxpQ0FBaUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7aUJBQzdEO2FBQ0YsQ0FBQyxDQUFDO1NBQ0o7S0FDRjtDQUVGIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBCdWNrZXQgcmVzcG9uc2UgZm9yIGEgZ2l2ZW4gRWxhc3RpY3NlYXJjaCBhZ2dyZWdhdGlvbi5cbiAqL1xuZXhwb3J0IGNsYXNzIEVzQnVja2V0IHtcblxuICAvKiogQnVja2V0IGtleSAqL1xuICBrZXk6IHN0cmluZztcbiAgLyoqIERvYyBjb3VudCBmb3IgdGhlIGJ1Y2tldCAqL1xuICBjb3VudDogbnVtYmVyO1xuICAvKiogUHJvcG9ydGlvbiB1c2VkIGZvciBwcm9ncmVzcyBiYXIgKi9cbiAgcHJvcG9ydGlvbjogbnVtYmVyO1xuICAvKiogTGlzdCBvZiBzdWIgYnVja2V0cyAqL1xuICBidWNrZXRzOiBFc0J1Y2tldFtdO1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHRoaXMua2V5ID0gJyc7XG4gICAgdGhpcy5jb3VudCA9IDA7XG4gICAgdGhpcy5wcm9wb3J0aW9uID0gMDtcbiAgICB0aGlzLmJ1Y2tldHMgPSBbXTtcbiAgfVxuXG5cbiAgLyoqXG4gICAqIEluaXRpYWxpemF0aW9uIG9mIHRoZSBidWNrZXQgZnJvbSBhIGJhY2tlbmQgcmVzcG9uc2UuXG4gICAqIFVzZWQgdG8ga2VlcCB0aGUgZnVuY3Rpb25zIG9mIHRoaXMgY2xhc3MuXG4gICAqIEBwYXJhbSBidWNrZXQgUmVzcG9uc2UgZnJvbSBiYWNrZW5kIHVzZWQgZm9yIGluaXRpYWxpemF0aW9uLlxuICAgKi9cbiAgaW5pdEJ1Y2tldChidWNrZXQ6IEVzQnVja2V0KTogdm9pZCB7XG4gICAgdGhpcy5rZXkgPSBidWNrZXQua2V5O1xuICAgIHRoaXMuY291bnQgPSBidWNrZXQuY291bnQ7XG4gICAgdGhpcy5wcm9wb3J0aW9uID0gYnVja2V0LnByb3BvcnRpb247XG4gICAgdGhpcy5idWNrZXRzID0gYnVja2V0LmJ1Y2tldHMubWFwKGl0ZW0gPT4ge1xuICAgICAgY29uc3QgcmVzdWx0ID0gbmV3IEVzQnVja2V0KCk7XG4gICAgICByZXN1bHQuaW5pdEJ1Y2tldChpdGVtKTtcbiAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgfSk7XG4gIH1cblxuICAvKipcbiAgICogVXBkYXRlIGNvdW50cyBmb3IgYnVja2V0cyBhbmQgYWxsIHN1Yi1idWNrZXRzIGdpdmVuIGEgYnVja2V0IGZyb20gYmFja2VuZCByZXNwb25zZS5cbiAgICogVXNlZCB0byBrZWVwIHRoZSBmdW5jdGlvbnMgb2YgdGhpcyBjbGFzcy5cbiAgICogQHBhcmFtIGJ1Y2tldCBSZXNwb25zZSBmcm9tIGJhY2tlbmQgdXNlZCBmb3IgY291bnQgdXBkYXRlLlxuICAgKiBAcGFyYW0gbWF4RG9jQ291bnQgTWF4aW11bSBEb2MgQ291bnQgZm9yIHRoZSBjdXJyZW50IGJ1Y2tldCBsZXZlbCwgdXNlZCB0byBjb21wdXRlIHByb3BvcnRpb25cbiAgICovXG4gIHVwZGF0ZUJ1Y2tldENvdW50cyhidWNrZXQ6IEVzQnVja2V0LCBtYXhEb2NDb3VudDogbnVtYmVyKTogdm9pZCB7XG4gICAgaWYgKGJ1Y2tldCA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICB0aGlzLmNvdW50ID0gMDtcbiAgICAgIHRoaXMucHJvcG9ydGlvbiA9IDA7XG4gICAgICB0aGlzLmJ1Y2tldHMuZm9yRWFjaChzdWIgPT4gc3ViLnVwZGF0ZUJ1Y2tldENvdW50cyh1bmRlZmluZWQsIDApKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5jb3VudCA9IGJ1Y2tldC5jb3VudDtcbiAgICAgIHRoaXMucHJvcG9ydGlvbiA9IGJ1Y2tldC5jb3VudCAvIG1heERvY0NvdW50O1xuICAgICAgdGhpcy5idWNrZXRzLmZvckVhY2goc3ViID0+IHtcbiAgICAgICAgY29uc3Qgc3ViQnVja2V0c21heERvY0NvdW50ID0gTWF0aC5tYXguYXBwbHkobnVsbCwgYnVja2V0LmJ1Y2tldHMubWFwKGl0ZW0gPT4gaXRlbS5jb3VudCkpO1xuICAgICAgICBjb25zdCBtYXRjaGluZyA9IGJ1Y2tldC5idWNrZXRzLmZpbHRlcihpdGVtID0+IGl0ZW0ua2V5ID09PSBzdWIua2V5KTtcbiAgICAgICAgaWYgKG1hdGNoaW5nLmxlbmd0aCA8PSAxKSB7XG4gICAgICAgICAgc3ViLnVwZGF0ZUJ1Y2tldENvdW50cyhtYXRjaGluZ1swXSwgc3ViQnVja2V0c21heERvY0NvdW50KTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBjb25zb2xlLmVycm9yKCdNdWx0aXBsZSBidWNrZXRzIGZvdW5kIGZvciBrZXkgJyArIHRoaXMua2V5KTtcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgfVxuICB9XG5cbn1cbiJdfQ==