import { DoCheck, EventEmitter, OnInit } from '@angular/core';
import { EsBucketAggregation } from '../../models/aggregation/bucket/es-bucket-aggregation';
import { CategorizedFilter } from '../../models/filter/categorized-filter';
import { SelectionModel } from '@angular/cdk/collections';
import { BehaviorSubject } from 'rxjs';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material';
import { EsBucket } from '../../models/aggregation/bucket/es-bucket';
import { EsBucketFlatNode } from '../../models/aggregation/bucket/es-bucket-flat-node';
export declare class BootstartChecklistFilterComponent implements DoCheck, OnInit {
    /** Input bucket aggregation */
    aggregation: EsBucketAggregation;
    /** Translation prefix */
    translationPrefix: string;
    /** Categorized filter emitter */
    onChange: EventEmitter<CategorizedFilter[]>;
    /** Selection for checklist */
    checklistSelection: SelectionModel<EsBucket>;
    /** Selection for emitter */
    emitterSelection: SelectionModel<CategorizedFilter>;
    /** Elements discarded from selection (e.g. children of a node if all children are checked) */
    discardedEmitterSelection: SelectionModel<EsBucket>;
    /** Map from flat node to nested node. This helps us finding the nested node to be modified */
    flatNodeMap: Map<EsBucketFlatNode, EsBucket>;
    /** Map from nested node to flattened node. This helps us to keep the same object for selection */
    nestedNodeMap: Map<EsBucket, EsBucketFlatNode>;
    dataChange: BehaviorSubject<EsBucket[]>;
    treeControl: FlatTreeControl<EsBucketFlatNode>;
    treeFlattener: MatTreeFlattener<EsBucket, EsBucketFlatNode>;
    dataSource: MatTreeFlatDataSource<EsBucket, EsBucketFlatNode>;
    getLevel: (node: EsBucketFlatNode) => number;
    isExpandable: (node: EsBucketFlatNode) => boolean;
    getChildren: (node: EsBucket) => EsBucket[];
    hasChild: (_: number, _nodeData: EsBucketFlatNode) => boolean;
    constructor();
    ngOnInit(): void;
    ngDoCheck(): void;
    /** Whether all the descendants of the node are selected */
    descendantsAllSelected(node: EsBucketFlatNode): boolean;
    /** Whether part of descendants are selected */
    descendantsPartiallySelected(node: EsBucketFlatNode): boolean;
    /** Toggle the Elasticsearch bucket node item. Select/deselect all the descendants node */
    bucketSelectionToggle(node: EsBucketFlatNode): void;
    /** Toggle a leaf Elasticsearch bucket item selection. Check all the parents to see if they changed */
    bucketLeafSelectionToggle(node: EsBucketFlatNode): void;
    private _emitChange();
    private _addToEmitSelector(buckets);
    /** Transformer to convert nested node to flat node. Record the nodes in maps for later use. */
    private _transformer(node, level);
    /** Checks all the parents when a node is selected/unselected */
    private _checkAllParentsSelection(node);
    /** Check root node checked state and change it accordingly */
    private _checkRootNodeSelection(node);
    /** Get the parent node of a node */
    private _getParentNode(node);
}
