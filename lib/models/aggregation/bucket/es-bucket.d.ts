/**
 * Bucket response for a given Elasticsearch aggregation.
 */
export declare class EsBucket {
    /** Bucket key */
    key: string;
    /** Doc count for the bucket */
    count: number;
    /** Proportion used for progress bar */
    proportion: number;
    /** List of sub buckets */
    buckets: EsBucket[];
    constructor();
    /**
     * Initialization of the bucket from a backend response.
     * Used to keep the functions of this class.
     * @param bucket Response from backend used for initialization.
     */
    initBucket(bucket: EsBucket): void;
    /**
     * Update counts for buckets and all sub-buckets given a bucket from backend response.
     * Used to keep the functions of this class.
     * @param bucket Response from backend used for count update.
     * @param maxDocCount Maximum Doc Count for the current bucket level, used to compute proportion
     */
    updateBucketCounts(bucket: EsBucket, maxDocCount: number): void;
}
