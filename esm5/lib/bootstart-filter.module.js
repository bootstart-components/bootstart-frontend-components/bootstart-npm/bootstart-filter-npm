/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { BootstartChecklistFilterComponent } from './components/bootstart-checklist-filter/bootstart-checklist-filter.component';
import { BootstartHistogramSliderFilterComponent } from './components/bootstart-histogram-slider-filter/bootstart-histogram-slider-filter.component';
import { MatBadgeModule, MatButtonModule, MatCheckboxModule, MatIconModule, MatProgressBarModule, MatTreeModule } from '@angular/material';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { Ng5SliderModule } from 'ng5-slider';
var BootstartFilterModule = /** @class */ (function () {
    function BootstartFilterModule() {
    }
    BootstartFilterModule.decorators = [
        { type: NgModule, args: [{
                    imports: [
                        CommonModule,
                        MatTreeModule,
                        MatCheckboxModule,
                        MatProgressBarModule,
                        MatIconModule,
                        MatButtonModule,
                        MatBadgeModule,
                        TranslateModule,
                        Ng5SliderModule
                    ],
                    declarations: [
                        BootstartChecklistFilterComponent,
                        BootstartHistogramSliderFilterComponent
                    ],
                    exports: [
                        BootstartChecklistFilterComponent,
                        BootstartHistogramSliderFilterComponent
                    ]
                },] },
    ];
    return BootstartFilterModule;
}());
export { BootstartFilterModule };

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LWZpbHRlci5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9ib290c3RhcnQtZmlsdGVyLyIsInNvdXJjZXMiOlsibGliL2Jvb3RzdGFydC1maWx0ZXIubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsUUFBUSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3ZDLE9BQU8sRUFBQyxpQ0FBaUMsRUFBQyxNQUFNLDhFQUE4RSxDQUFDO0FBQy9ILE9BQU8sRUFBQyx1Q0FBdUMsRUFBQyxNQUFNLDRGQUE0RixDQUFDO0FBQ25KLE9BQU8sRUFBQyxjQUFjLEVBQUUsZUFBZSxFQUFFLGlCQUFpQixFQUFFLGFBQWEsRUFBRSxvQkFBb0IsRUFBRSxhQUFhLEVBQUMsTUFBTSxtQkFBbUIsQ0FBQztBQUN6SSxPQUFPLEVBQUMsWUFBWSxFQUFDLE1BQU0saUJBQWlCLENBQUM7QUFDN0MsT0FBTyxFQUFDLGVBQWUsRUFBQyxNQUFNLHFCQUFxQixDQUFDO0FBQ3BELE9BQU8sRUFBQyxlQUFlLEVBQUMsTUFBTSxZQUFZLENBQUM7Ozs7O2dCQUUxQyxRQUFRLFNBQUM7b0JBQ0UsT0FBTyxFQUFPO3dCQUNaLFlBQVk7d0JBQ1osYUFBYTt3QkFDYixpQkFBaUI7d0JBQ2pCLG9CQUFvQjt3QkFDcEIsYUFBYTt3QkFDYixlQUFlO3dCQUNmLGNBQWM7d0JBQ2QsZUFBZTt3QkFDZixlQUFlO3FCQUNoQjtvQkFDRCxZQUFZLEVBQUU7d0JBQ1osaUNBQWlDO3dCQUNqQyx1Q0FBdUM7cUJBQ3hDO29CQUNELE9BQU8sRUFBTzt3QkFDWixpQ0FBaUM7d0JBQ2pDLHVDQUF1QztxQkFDeEM7aUJBQ0Y7O2dDQTVCWDs7U0E2QmEscUJBQXFCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtOZ01vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0Jvb3RzdGFydENoZWNrbGlzdEZpbHRlckNvbXBvbmVudH0gZnJvbSAnLi9jb21wb25lbnRzL2Jvb3RzdGFydC1jaGVja2xpc3QtZmlsdGVyL2Jvb3RzdGFydC1jaGVja2xpc3QtZmlsdGVyLmNvbXBvbmVudCc7XG5pbXBvcnQge0Jvb3RzdGFydEhpc3RvZ3JhbVNsaWRlckZpbHRlckNvbXBvbmVudH0gZnJvbSAnLi9jb21wb25lbnRzL2Jvb3RzdGFydC1oaXN0b2dyYW0tc2xpZGVyLWZpbHRlci9ib290c3RhcnQtaGlzdG9ncmFtLXNsaWRlci1maWx0ZXIuY29tcG9uZW50JztcbmltcG9ydCB7TWF0QmFkZ2VNb2R1bGUsIE1hdEJ1dHRvbk1vZHVsZSwgTWF0Q2hlY2tib3hNb2R1bGUsIE1hdEljb25Nb2R1bGUsIE1hdFByb2dyZXNzQmFyTW9kdWxlLCBNYXRUcmVlTW9kdWxlfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XG5pbXBvcnQge0NvbW1vbk1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7VHJhbnNsYXRlTW9kdWxlfSBmcm9tICdAbmd4LXRyYW5zbGF0ZS9jb3JlJztcbmltcG9ydCB7Tmc1U2xpZGVyTW9kdWxlfSBmcm9tICduZzUtc2xpZGVyJztcblxuQE5nTW9kdWxlKHtcbiAgICAgICAgICAgIGltcG9ydHMgICAgIDogW1xuICAgICAgICAgICAgICBDb21tb25Nb2R1bGUsXG4gICAgICAgICAgICAgIE1hdFRyZWVNb2R1bGUsXG4gICAgICAgICAgICAgIE1hdENoZWNrYm94TW9kdWxlLFxuICAgICAgICAgICAgICBNYXRQcm9ncmVzc0Jhck1vZHVsZSxcbiAgICAgICAgICAgICAgTWF0SWNvbk1vZHVsZSxcbiAgICAgICAgICAgICAgTWF0QnV0dG9uTW9kdWxlLFxuICAgICAgICAgICAgICBNYXRCYWRnZU1vZHVsZSxcbiAgICAgICAgICAgICAgVHJhbnNsYXRlTW9kdWxlLFxuICAgICAgICAgICAgICBOZzVTbGlkZXJNb2R1bGVcbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBkZWNsYXJhdGlvbnM6IFtcbiAgICAgICAgICAgICAgQm9vdHN0YXJ0Q2hlY2tsaXN0RmlsdGVyQ29tcG9uZW50LFxuICAgICAgICAgICAgICBCb290c3RhcnRIaXN0b2dyYW1TbGlkZXJGaWx0ZXJDb21wb25lbnRcbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBleHBvcnRzICAgICA6IFtcbiAgICAgICAgICAgICAgQm9vdHN0YXJ0Q2hlY2tsaXN0RmlsdGVyQ29tcG9uZW50LFxuICAgICAgICAgICAgICBCb290c3RhcnRIaXN0b2dyYW1TbGlkZXJGaWx0ZXJDb21wb25lbnRcbiAgICAgICAgICAgIF1cbiAgICAgICAgICB9KVxuZXhwb3J0IGNsYXNzIEJvb3RzdGFydEZpbHRlck1vZHVsZSB7XG59XG4iXX0=