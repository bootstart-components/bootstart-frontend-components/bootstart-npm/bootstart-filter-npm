/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { EsBucketAggregation } from '../../models';
import { Options } from 'ng5-slider';
import { Interval } from '../../models/utils/interval';
import { BehaviorSubject } from 'rxjs';
var BootstartHistogramSliderFilterComponent = /** @class */ (function () {
    function BootstartHistogramSliderFilterComponent() {
        var _this = this;
        this.minRange = 1;
        this.dataChange = new BehaviorSubject([]);
        this.breakpoints = [];
        this.lowerBounded = true;
        this.upperBounded = true;
        this.histogramData = [];
        this.sliderOptions = new Options();
        this.sliderInitialized = false;
        this.rangeChange = new EventEmitter();
        this.dataSource = [];
        this.dataChange.subscribe(function (data) {
            if (!_this.sliderInitialized) {
                _this.dataSource = data;
                _this._init();
            }
        });
    }
    /**
     * @return {?}
     */
    BootstartHistogramSliderFilterComponent.prototype.ngDoCheck = /**
     * @return {?}
     */
    function () {
        this.dataChange.next(this.bucketAggregation.buckets);
    };
    /**
     * Emits range event when value changes.
     * If min or max is infinite, returns null.
     */
    /**
     * Emits range event when value changes.
     * If min or max is infinite, returns null.
     * @return {?}
     */
    BootstartHistogramSliderFilterComponent.prototype.valueChange = /**
     * Emits range event when value changes.
     * If min or max is infinite, returns null.
     * @return {?}
     */
    function () {
        /** @type {?} */
        var interval = new Interval();
        interval.min = (!this.lowerBounded && this.minSelected === this.sliderOptions.floor) ? null : this.breakpoints[this.minSelected];
        interval.max = (!this.upperBounded && this.maxSelected === this.sliderOptions.ceil) ? null : this.breakpoints[this.maxSelected];
        this.rangeChange.emit(interval);
    };
    /**
     * Initialization from bucket aggregation
     * @return {?}
     */
    BootstartHistogramSliderFilterComponent.prototype._init = /**
     * Initialization from bucket aggregation
     * @return {?}
     */
    function () {
        var _this = this;
        if (this.dataSource.length > 0) {
            for (var i = 0; i < this.dataSource.length; i++) {
                /** @type {?} */
                var bucket = this.dataSource[i];
                /** @type {?} */
                var bucketRangeValues = bucket.key.split('-');
                if (bucketRangeValues.length === 1) {
                    // Aggregation is an histogram
                    this._addBreakpointValue(-Infinity);
                    this._addBreakpointValue(+bucketRangeValues[0]);
                    this.lowerBounded = false;
                }
                else {
                    // Aggregation is a list of ranges
                    if (bucketRangeValues[0] === '*') {
                        this._addBreakpointValue(-Infinity);
                        this._addBreakpointValue(+bucketRangeValues[1]);
                        this.lowerBounded = false;
                    }
                    else if (bucketRangeValues[1] === '*') {
                        this._addBreakpointValue(+bucketRangeValues[0]);
                        this._addBreakpointValue(Infinity);
                        this.upperBounded = false;
                    }
                    else {
                        this._addBreakpointValue(+bucketRangeValues[0]);
                        this._addBreakpointValue(+bucketRangeValues[1]);
                    }
                }
                /** @type {?} */
                var breakpointsFiniteValues = this.breakpoints.filter(function (item) { return item !== -Infinity && item !== Infinity; });
                this.breakpointFiniteMinValue = Math.min.apply(null, breakpointsFiniteValues);
                this.breakpointFiniteMaxValue = Math.max.apply(null, breakpointsFiniteValues);
                this.histogramData.push({
                    x: i,
                    y: bucket.count
                });
            }
            this.minSelected = 0;
            this.sliderOptions.floor = 0;
            this.maxSelected = this.histogramData.length;
            this.sliderOptions.ceil = this.histogramData.length;
            this.sliderOptions.translate = function (value, label) {
                if (_this.breakpoints[value] === -Infinity) {
                    return '< ' + _this.breakpointFiniteMinValue;
                }
                if (_this.breakpoints[value] === Infinity) {
                    return '> ' + _this.breakpointFiniteMaxValue;
                }
                return _this.breakpoints[value] + '';
            };
            // Slider can only be initialized once.
            this.sliderInitialized = true;
        }
        this.sliderOptions.minRange = this.minRange;
        this.sliderOptions.animate = false;
        this.sliderOptions.noSwitching = true;
        this.sliderOptions.pushRange = true;
        this.sliderOptions.hideLimitLabels = true;
    };
    /**
     * Adds value to breakpoints array.
     *  Checks first if value is already registered.
     * @param {?} value
     * @return {?}
     */
    BootstartHistogramSliderFilterComponent.prototype._addBreakpointValue = /**
     * Adds value to breakpoints array.
     *  Checks first if value is already registered.
     * @param {?} value
     * @return {?}
     */
    function (value) {
        if (this.breakpoints.indexOf(value) === -1) {
            this.breakpoints.push(value);
        }
    };
    BootstartHistogramSliderFilterComponent.decorators = [
        { type: Component, args: [{
                    selector: 'bootstart-histogram-slider-filter',
                    template: "<div class=\"bootstart-range-slider\" *ngIf=\"sliderInitialized\">\n  <ng5-slider [(value)]=\"minSelected\"\n              [(highValue)]=\"maxSelected\"\n              [options]=\"sliderOptions\"\n              (valueChange)=\"valueChange()\"\n              (highValueChange)=\"valueChange()\"></ng5-slider>\n</div>\n",
                    styles: [""]
                },] },
    ];
    /** @nocollapse */
    BootstartHistogramSliderFilterComponent.ctorParameters = function () { return []; };
    BootstartHistogramSliderFilterComponent.propDecorators = {
        bucketAggregation: [{ type: Input }],
        minRange: [{ type: Input }],
        rangeChange: [{ type: Output }]
    };
    return BootstartHistogramSliderFilterComponent;
}());
export { BootstartHistogramSliderFilterComponent };
if (false) {
    /** @type {?} */
    BootstartHistogramSliderFilterComponent.prototype.bucketAggregation;
    /** @type {?} */
    BootstartHistogramSliderFilterComponent.prototype.minRange;
    /** @type {?} */
    BootstartHistogramSliderFilterComponent.prototype.breakpoints;
    /** @type {?} */
    BootstartHistogramSliderFilterComponent.prototype.breakpointFiniteMinValue;
    /** @type {?} */
    BootstartHistogramSliderFilterComponent.prototype.breakpointFiniteMaxValue;
    /** @type {?} */
    BootstartHistogramSliderFilterComponent.prototype.lowerBounded;
    /** @type {?} */
    BootstartHistogramSliderFilterComponent.prototype.upperBounded;
    /** @type {?} */
    BootstartHistogramSliderFilterComponent.prototype.histogramData;
    /** @type {?} */
    BootstartHistogramSliderFilterComponent.prototype.minSelected;
    /** @type {?} */
    BootstartHistogramSliderFilterComponent.prototype.maxSelected;
    /** @type {?} */
    BootstartHistogramSliderFilterComponent.prototype.sliderOptions;
    /** @type {?} */
    BootstartHistogramSliderFilterComponent.prototype.sliderInitialized;
    /** @type {?} */
    BootstartHistogramSliderFilterComponent.prototype.dataSource;
    /** @type {?} */
    BootstartHistogramSliderFilterComponent.prototype.dataChange;
    /** @type {?} */
    BootstartHistogramSliderFilterComponent.prototype.rangeChange;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LWhpc3RvZ3JhbS1zbGlkZXItZmlsdGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Jvb3RzdGFydC1maWx0ZXIvIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9ib290c3RhcnQtaGlzdG9ncmFtLXNsaWRlci1maWx0ZXIvYm9vdHN0YXJ0LWhpc3RvZ3JhbS1zbGlkZXItZmlsdGVyLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLFNBQVMsRUFBVyxZQUFZLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUM5RSxPQUFPLEVBQVcsbUJBQW1CLEVBQUMsTUFBTSxjQUFjLENBQUM7QUFDM0QsT0FBTyxFQUFZLE9BQU8sRUFBQyxNQUFNLFlBQVksQ0FBQztBQUU5QyxPQUFPLEVBQUMsUUFBUSxFQUFDLE1BQU0sNkJBQTZCLENBQUM7QUFDckQsT0FBTyxFQUFDLGVBQWUsRUFBQyxNQUFNLE1BQU0sQ0FBQzs7SUFvQ25DO1FBQUEsaUJBZ0JDO3dCQW5DMkIsQ0FBQzswQkFjYSxJQUFJLGVBQWUsQ0FBYSxFQUFFLENBQUM7UUFNM0UsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7UUFDdEIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7UUFDekIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7UUFDekIsSUFBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7UUFDeEIsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO1FBQ25DLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUM7UUFDL0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBRXRDLElBQUksQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLFVBQUEsSUFBSTtZQUM1QixFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUM7Z0JBQzVCLEtBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO2dCQUN2QixLQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7YUFDZDtTQUNGLENBQUMsQ0FBQztLQUNKOzs7O0lBRUQsMkRBQVM7OztJQUFUO1FBQ0UsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxDQUFDO0tBQ3REO0lBR0Q7OztPQUdHOzs7Ozs7SUFDSCw2REFBVzs7Ozs7SUFBWDs7UUFDRSxJQUFNLFFBQVEsR0FBRyxJQUFJLFFBQVEsRUFBRSxDQUFDO1FBQ2hDLFFBQVEsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLElBQUksSUFBSSxDQUFDLFdBQVcsS0FBSyxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ2pJLFFBQVEsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLElBQUksSUFBSSxDQUFDLFdBQVcsS0FBSyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ2hJLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0tBQ2pDOzs7OztJQUdPLHVEQUFLOzs7Ozs7UUFDWCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQy9CLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQzs7Z0JBQ2hELElBQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7O2dCQUNsQyxJQUFNLGlCQUFpQixHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUVoRCxFQUFFLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQzs7b0JBRW5DLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUNwQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNoRCxJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztpQkFDM0I7Z0JBQUMsSUFBSSxDQUFDLENBQUM7O29CQUVOLEVBQUUsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUM7d0JBQ2pDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDO3dCQUNwQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUNoRCxJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztxQkFDM0I7b0JBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUM7d0JBQ3hDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ2hELElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsQ0FBQzt3QkFDbkMsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7cUJBQzNCO29CQUFDLElBQUksQ0FBQyxDQUFDO3dCQUNOLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ2hELElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7cUJBQ2pEO2lCQUNGOztnQkFFRCxJQUFNLHVCQUF1QixHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxLQUFLLENBQUMsUUFBUSxJQUFJLElBQUksS0FBSyxRQUFRLEVBQXZDLENBQXVDLENBQUMsQ0FBQztnQkFDekcsSUFBSSxDQUFDLHdCQUF3QixHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSx1QkFBdUIsQ0FBQyxDQUFDO2dCQUM5RSxJQUFJLENBQUMsd0JBQXdCLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLHVCQUF1QixDQUFDLENBQUM7Z0JBQzlFLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDO29CQUNFLENBQUMsRUFBRSxDQUFDO29CQUNKLENBQUMsRUFBRSxNQUFNLENBQUMsS0FBSztpQkFDaEIsQ0FBQyxDQUFDO2FBQzVCO1lBQ0QsSUFBSSxDQUFDLFdBQVcsR0FBRyxDQUFDLENBQUM7WUFDckIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDO1lBQzdCLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUM7WUFDN0MsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUM7WUFDcEQsSUFBSSxDQUFDLGFBQWEsQ0FBQyxTQUFTLEdBQUcsVUFBQyxLQUFhLEVBQUUsS0FBZ0I7Z0JBQzdELEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO29CQUMxQyxNQUFNLENBQUMsSUFBSSxHQUFHLEtBQUksQ0FBQyx3QkFBd0IsQ0FBQztpQkFDN0M7Z0JBQ0QsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDO29CQUN6QyxNQUFNLENBQUMsSUFBSSxHQUFHLEtBQUksQ0FBQyx3QkFBd0IsQ0FBQztpQkFDN0M7Z0JBQ0QsTUFBTSxDQUFDLEtBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxDQUFDO2FBQ3JDLENBQUM7O1lBR0YsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQztTQUMvQjtRQUNELElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7UUFDNUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1FBQ25DLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztRQUN0QyxJQUFJLENBQUMsYUFBYSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7UUFDcEMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDOzs7Ozs7OztJQUtwQyxxRUFBbUI7Ozs7OztjQUFDLEtBQWE7UUFDdkMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzNDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQzlCOzs7Z0JBcklKLFNBQVMsU0FBQztvQkFDRSxRQUFRLEVBQUssbUNBQW1DO29CQUNoRCxRQUFRLEVBQUUsK1RBT3RCO29CQUNZLE1BQU0sRUFBRSxDQUFDLEVBQUUsQ0FBQztpQkFDYjs7Ozs7b0NBR1QsS0FBSzsyQkFDTCxLQUFLOzhCQWdCTCxNQUFNOztrREF0Q1Q7O1NBbUJhLHVDQUF1QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBEb0NoZWNrLCBFdmVudEVtaXR0ZXIsIElucHV0LCBPdXRwdXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtFc0J1Y2tldCwgRXNCdWNrZXRBZ2dyZWdhdGlvbn0gZnJvbSAnLi4vLi4vbW9kZWxzJztcbmltcG9ydCB7TGFiZWxUeXBlLCBPcHRpb25zfSBmcm9tICduZzUtc2xpZGVyJztcbmltcG9ydCB7SGlzdG9ncmFtSW50ZXJ2YWxEYXRhfSBmcm9tICcuLi8uLi9tb2RlbHMvdXRpbHMvaGlzdG9ncmFtLWludGVydmFsLWRhdGEnO1xuaW1wb3J0IHtJbnRlcnZhbH0gZnJvbSAnLi4vLi4vbW9kZWxzL3V0aWxzL2ludGVydmFsJztcbmltcG9ydCB7QmVoYXZpb3JTdWJqZWN0fSBmcm9tICdyeGpzJztcblxuQENvbXBvbmVudCh7XG4gICAgICAgICAgICAgc2VsZWN0b3IgICA6ICdib290c3RhcnQtaGlzdG9ncmFtLXNsaWRlci1maWx0ZXInLFxuICAgICAgICAgICAgIHRlbXBsYXRlOiBgPGRpdiBjbGFzcz1cImJvb3RzdGFydC1yYW5nZS1zbGlkZXJcIiAqbmdJZj1cInNsaWRlckluaXRpYWxpemVkXCI+XG4gIDxuZzUtc2xpZGVyIFsodmFsdWUpXT1cIm1pblNlbGVjdGVkXCJcbiAgICAgICAgICAgICAgWyhoaWdoVmFsdWUpXT1cIm1heFNlbGVjdGVkXCJcbiAgICAgICAgICAgICAgW29wdGlvbnNdPVwic2xpZGVyT3B0aW9uc1wiXG4gICAgICAgICAgICAgICh2YWx1ZUNoYW5nZSk9XCJ2YWx1ZUNoYW5nZSgpXCJcbiAgICAgICAgICAgICAgKGhpZ2hWYWx1ZUNoYW5nZSk9XCJ2YWx1ZUNoYW5nZSgpXCI+PC9uZzUtc2xpZGVyPlxuPC9kaXY+XG5gLFxuICAgICAgICAgICAgIHN0eWxlczogW2BgXVxuICAgICAgICAgICB9KVxuZXhwb3J0IGNsYXNzIEJvb3RzdGFydEhpc3RvZ3JhbVNsaWRlckZpbHRlckNvbXBvbmVudCBpbXBsZW1lbnRzIERvQ2hlY2sge1xuXG4gIEBJbnB1dCgpIGJ1Y2tldEFnZ3JlZ2F0aW9uOiBFc0J1Y2tldEFnZ3JlZ2F0aW9uO1xuICBASW5wdXQoKSBtaW5SYW5nZTogbnVtYmVyID0gMTtcblxuICBicmVha3BvaW50czogbnVtYmVyW107XG4gIGJyZWFrcG9pbnRGaW5pdGVNaW5WYWx1ZTogbnVtYmVyO1xuICBicmVha3BvaW50RmluaXRlTWF4VmFsdWU6IG51bWJlcjtcbiAgbG93ZXJCb3VuZGVkOiBib29sZWFuO1xuICB1cHBlckJvdW5kZWQ6IGJvb2xlYW47XG4gIGhpc3RvZ3JhbURhdGE6IEhpc3RvZ3JhbUludGVydmFsRGF0YVtdO1xuICBtaW5TZWxlY3RlZDogbnVtYmVyO1xuICBtYXhTZWxlY3RlZDogbnVtYmVyO1xuICBzbGlkZXJPcHRpb25zOiBPcHRpb25zO1xuICBzbGlkZXJJbml0aWFsaXplZDogYm9vbGVhbjtcblxuICBkYXRhU291cmNlOiBFc0J1Y2tldFtdO1xuICBkYXRhQ2hhbmdlOiBCZWhhdmlvclN1YmplY3Q8RXNCdWNrZXRbXT4gPSBuZXcgQmVoYXZpb3JTdWJqZWN0PEVzQnVja2V0W10+KFtdKTtcblxuICBAT3V0cHV0KCkgcmFuZ2VDaGFuZ2U6IEV2ZW50RW1pdHRlcjxJbnRlcnZhbD47XG5cblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgICB0aGlzLmJyZWFrcG9pbnRzID0gW107XG4gICAgdGhpcy5sb3dlckJvdW5kZWQgPSB0cnVlO1xuICAgIHRoaXMudXBwZXJCb3VuZGVkID0gdHJ1ZTtcbiAgICB0aGlzLmhpc3RvZ3JhbURhdGEgPSBbXTtcbiAgICB0aGlzLnNsaWRlck9wdGlvbnMgPSBuZXcgT3B0aW9ucygpO1xuICAgIHRoaXMuc2xpZGVySW5pdGlhbGl6ZWQgPSBmYWxzZTtcbiAgICB0aGlzLnJhbmdlQ2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXG4gICAgdGhpcy5kYXRhU291cmNlID0gW107XG4gICAgdGhpcy5kYXRhQ2hhbmdlLnN1YnNjcmliZShkYXRhID0+IHtcbiAgICAgIGlmICghdGhpcy5zbGlkZXJJbml0aWFsaXplZCkge1xuICAgICAgICB0aGlzLmRhdGFTb3VyY2UgPSBkYXRhO1xuICAgICAgICB0aGlzLl9pbml0KCk7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cblxuICBuZ0RvQ2hlY2soKTogdm9pZCB7XG4gICAgdGhpcy5kYXRhQ2hhbmdlLm5leHQodGhpcy5idWNrZXRBZ2dyZWdhdGlvbi5idWNrZXRzKTtcbiAgfVxuXG5cbiAgLyoqXG4gICAqIEVtaXRzIHJhbmdlIGV2ZW50IHdoZW4gdmFsdWUgY2hhbmdlcy5cbiAgICogSWYgbWluIG9yIG1heCBpcyBpbmZpbml0ZSwgcmV0dXJucyBudWxsLlxuICAgKi9cbiAgdmFsdWVDaGFuZ2UoKTogdm9pZCB7XG4gICAgY29uc3QgaW50ZXJ2YWwgPSBuZXcgSW50ZXJ2YWwoKTtcbiAgICBpbnRlcnZhbC5taW4gPSAoIXRoaXMubG93ZXJCb3VuZGVkICYmIHRoaXMubWluU2VsZWN0ZWQgPT09IHRoaXMuc2xpZGVyT3B0aW9ucy5mbG9vcikgPyBudWxsIDogdGhpcy5icmVha3BvaW50c1t0aGlzLm1pblNlbGVjdGVkXTtcbiAgICBpbnRlcnZhbC5tYXggPSAoIXRoaXMudXBwZXJCb3VuZGVkICYmIHRoaXMubWF4U2VsZWN0ZWQgPT09IHRoaXMuc2xpZGVyT3B0aW9ucy5jZWlsKSA/IG51bGwgOiB0aGlzLmJyZWFrcG9pbnRzW3RoaXMubWF4U2VsZWN0ZWRdO1xuICAgIHRoaXMucmFuZ2VDaGFuZ2UuZW1pdChpbnRlcnZhbCk7XG4gIH1cblxuICAvKiogSW5pdGlhbGl6YXRpb24gZnJvbSBidWNrZXQgYWdncmVnYXRpb24gKi9cbiAgcHJpdmF0ZSBfaW5pdCgpOiB2b2lkIHtcbiAgICBpZiAodGhpcy5kYXRhU291cmNlLmxlbmd0aCA+IDApIHtcbiAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5kYXRhU291cmNlLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIGNvbnN0IGJ1Y2tldCA9IHRoaXMuZGF0YVNvdXJjZVtpXTtcbiAgICAgICAgY29uc3QgYnVja2V0UmFuZ2VWYWx1ZXMgPSBidWNrZXQua2V5LnNwbGl0KCctJyk7XG5cbiAgICAgICAgaWYgKGJ1Y2tldFJhbmdlVmFsdWVzLmxlbmd0aCA9PT0gMSkge1xuICAgICAgICAgIC8vIEFnZ3JlZ2F0aW9uIGlzIGFuIGhpc3RvZ3JhbVxuICAgICAgICAgIHRoaXMuX2FkZEJyZWFrcG9pbnRWYWx1ZSgtSW5maW5pdHkpO1xuICAgICAgICAgIHRoaXMuX2FkZEJyZWFrcG9pbnRWYWx1ZSgrYnVja2V0UmFuZ2VWYWx1ZXNbMF0pO1xuICAgICAgICAgIHRoaXMubG93ZXJCb3VuZGVkID0gZmFsc2U7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgLy8gQWdncmVnYXRpb24gaXMgYSBsaXN0IG9mIHJhbmdlc1xuICAgICAgICAgIGlmIChidWNrZXRSYW5nZVZhbHVlc1swXSA9PT0gJyonKSB7XG4gICAgICAgICAgICB0aGlzLl9hZGRCcmVha3BvaW50VmFsdWUoLUluZmluaXR5KTtcbiAgICAgICAgICAgIHRoaXMuX2FkZEJyZWFrcG9pbnRWYWx1ZSgrYnVja2V0UmFuZ2VWYWx1ZXNbMV0pO1xuICAgICAgICAgICAgdGhpcy5sb3dlckJvdW5kZWQgPSBmYWxzZTtcbiAgICAgICAgICB9IGVsc2UgaWYgKGJ1Y2tldFJhbmdlVmFsdWVzWzFdID09PSAnKicpIHtcbiAgICAgICAgICAgIHRoaXMuX2FkZEJyZWFrcG9pbnRWYWx1ZSgrYnVja2V0UmFuZ2VWYWx1ZXNbMF0pO1xuICAgICAgICAgICAgdGhpcy5fYWRkQnJlYWtwb2ludFZhbHVlKEluZmluaXR5KTtcbiAgICAgICAgICAgIHRoaXMudXBwZXJCb3VuZGVkID0gZmFsc2U7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuX2FkZEJyZWFrcG9pbnRWYWx1ZSgrYnVja2V0UmFuZ2VWYWx1ZXNbMF0pO1xuICAgICAgICAgICAgdGhpcy5fYWRkQnJlYWtwb2ludFZhbHVlKCtidWNrZXRSYW5nZVZhbHVlc1sxXSk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgY29uc3QgYnJlYWtwb2ludHNGaW5pdGVWYWx1ZXMgPSB0aGlzLmJyZWFrcG9pbnRzLmZpbHRlcihpdGVtID0+IGl0ZW0gIT09IC1JbmZpbml0eSAmJiBpdGVtICE9PSBJbmZpbml0eSk7XG4gICAgICAgIHRoaXMuYnJlYWtwb2ludEZpbml0ZU1pblZhbHVlID0gTWF0aC5taW4uYXBwbHkobnVsbCwgYnJlYWtwb2ludHNGaW5pdGVWYWx1ZXMpO1xuICAgICAgICB0aGlzLmJyZWFrcG9pbnRGaW5pdGVNYXhWYWx1ZSA9IE1hdGgubWF4LmFwcGx5KG51bGwsIGJyZWFrcG9pbnRzRmluaXRlVmFsdWVzKTtcbiAgICAgICAgdGhpcy5oaXN0b2dyYW1EYXRhLnB1c2goe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHg6IGksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeTogYnVja2V0LmNvdW50XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgfVxuICAgICAgdGhpcy5taW5TZWxlY3RlZCA9IDA7XG4gICAgICB0aGlzLnNsaWRlck9wdGlvbnMuZmxvb3IgPSAwO1xuICAgICAgdGhpcy5tYXhTZWxlY3RlZCA9IHRoaXMuaGlzdG9ncmFtRGF0YS5sZW5ndGg7XG4gICAgICB0aGlzLnNsaWRlck9wdGlvbnMuY2VpbCA9IHRoaXMuaGlzdG9ncmFtRGF0YS5sZW5ndGg7XG4gICAgICB0aGlzLnNsaWRlck9wdGlvbnMudHJhbnNsYXRlID0gKHZhbHVlOiBudW1iZXIsIGxhYmVsOiBMYWJlbFR5cGUpID0+IHtcbiAgICAgICAgaWYgKHRoaXMuYnJlYWtwb2ludHNbdmFsdWVdID09PSAtSW5maW5pdHkpIHtcbiAgICAgICAgICByZXR1cm4gJzwgJyArIHRoaXMuYnJlYWtwb2ludEZpbml0ZU1pblZhbHVlO1xuICAgICAgICB9XG4gICAgICAgIGlmICh0aGlzLmJyZWFrcG9pbnRzW3ZhbHVlXSA9PT0gSW5maW5pdHkpIHtcbiAgICAgICAgICByZXR1cm4gJz4gJyArIHRoaXMuYnJlYWtwb2ludEZpbml0ZU1heFZhbHVlO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0aGlzLmJyZWFrcG9pbnRzW3ZhbHVlXSArICcnO1xuICAgICAgfTtcblxuICAgICAgLy8gU2xpZGVyIGNhbiBvbmx5IGJlIGluaXRpYWxpemVkIG9uY2UuXG4gICAgICB0aGlzLnNsaWRlckluaXRpYWxpemVkID0gdHJ1ZTtcbiAgICB9XG4gICAgdGhpcy5zbGlkZXJPcHRpb25zLm1pblJhbmdlID0gdGhpcy5taW5SYW5nZTtcbiAgICB0aGlzLnNsaWRlck9wdGlvbnMuYW5pbWF0ZSA9IGZhbHNlO1xuICAgIHRoaXMuc2xpZGVyT3B0aW9ucy5ub1N3aXRjaGluZyA9IHRydWU7XG4gICAgdGhpcy5zbGlkZXJPcHRpb25zLnB1c2hSYW5nZSA9IHRydWU7XG4gICAgdGhpcy5zbGlkZXJPcHRpb25zLmhpZGVMaW1pdExhYmVscyA9IHRydWU7XG4gIH1cblxuICAvKiogQWRkcyB2YWx1ZSB0byBicmVha3BvaW50cyBhcnJheS5cbiAgICogIENoZWNrcyBmaXJzdCBpZiB2YWx1ZSBpcyBhbHJlYWR5IHJlZ2lzdGVyZWQuKi9cbiAgcHJpdmF0ZSBfYWRkQnJlYWtwb2ludFZhbHVlKHZhbHVlOiBudW1iZXIpOiB2b2lkIHtcbiAgICBpZiAodGhpcy5icmVha3BvaW50cy5pbmRleE9mKHZhbHVlKSA9PT0gLTEpIHtcbiAgICAgIHRoaXMuYnJlYWtwb2ludHMucHVzaCh2YWx1ZSk7XG4gICAgfVxuICB9XG5cblxufVxuXG5cbiJdfQ==