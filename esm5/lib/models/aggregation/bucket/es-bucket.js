/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * Bucket response for a given Elasticsearch aggregation.
 */
var /**
 * Bucket response for a given Elasticsearch aggregation.
 */
EsBucket = /** @class */ (function () {
    function EsBucket() {
        this.key = '';
        this.count = 0;
        this.proportion = 0;
        this.buckets = [];
    }
    /**
     * Initialization of the bucket from a backend response.
     * Used to keep the functions of this class.
     * @param bucket Response from backend used for initialization.
     */
    /**
     * Initialization of the bucket from a backend response.
     * Used to keep the functions of this class.
     * @param {?} bucket Response from backend used for initialization.
     * @return {?}
     */
    EsBucket.prototype.initBucket = /**
     * Initialization of the bucket from a backend response.
     * Used to keep the functions of this class.
     * @param {?} bucket Response from backend used for initialization.
     * @return {?}
     */
    function (bucket) {
        this.key = bucket.key;
        this.count = bucket.count;
        this.proportion = bucket.proportion;
        this.buckets = bucket.buckets.map(function (item) {
            /** @type {?} */
            var result = new EsBucket();
            result.initBucket(item);
            return result;
        });
    };
    /**
     * Update counts for buckets and all sub-buckets given a bucket from backend response.
     * Used to keep the functions of this class.
     * @param bucket Response from backend used for count update.
     * @param maxDocCount Maximum Doc Count for the current bucket level, used to compute proportion
     */
    /**
     * Update counts for buckets and all sub-buckets given a bucket from backend response.
     * Used to keep the functions of this class.
     * @param {?} bucket Response from backend used for count update.
     * @param {?} maxDocCount Maximum Doc Count for the current bucket level, used to compute proportion
     * @return {?}
     */
    EsBucket.prototype.updateBucketCounts = /**
     * Update counts for buckets and all sub-buckets given a bucket from backend response.
     * Used to keep the functions of this class.
     * @param {?} bucket Response from backend used for count update.
     * @param {?} maxDocCount Maximum Doc Count for the current bucket level, used to compute proportion
     * @return {?}
     */
    function (bucket, maxDocCount) {
        var _this = this;
        if (bucket === undefined) {
            this.count = 0;
            this.proportion = 0;
            this.buckets.forEach(function (sub) { return sub.updateBucketCounts(undefined, 0); });
        }
        else {
            this.count = bucket.count;
            this.proportion = bucket.count / maxDocCount;
            this.buckets.forEach(function (sub) {
                /** @type {?} */
                var subBucketsmaxDocCount = Math.max.apply(null, bucket.buckets.map(function (item) { return item.count; }));
                /** @type {?} */
                var matching = bucket.buckets.filter(function (item) { return item.key === sub.key; });
                if (matching.length <= 1) {
                    sub.updateBucketCounts(matching[0], subBucketsmaxDocCount);
                }
                else {
                    console.error('Multiple buckets found for key ' + _this.key);
                }
            });
        }
    };
    return EsBucket;
}());
/**
 * Bucket response for a given Elasticsearch aggregation.
 */
export { EsBucket };
if (false) {
    /**
     * Bucket key
     * @type {?}
     */
    EsBucket.prototype.key;
    /**
     * Doc count for the bucket
     * @type {?}
     */
    EsBucket.prototype.count;
    /**
     * Proportion used for progress bar
     * @type {?}
     */
    EsBucket.prototype.proportion;
    /**
     * List of sub buckets
     * @type {?}
     */
    EsBucket.prototype.buckets;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXMtYnVja2V0LmpzIiwic291cmNlUm9vdCI6Im5nOi8vYm9vdHN0YXJ0LWZpbHRlci8iLCJzb3VyY2VzIjpbImxpYi9tb2RlbHMvYWdncmVnYXRpb24vYnVja2V0L2VzLWJ1Y2tldC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBR0E7OztBQUFBO0lBV0U7UUFDRSxJQUFJLENBQUMsR0FBRyxHQUFHLEVBQUUsQ0FBQztRQUNkLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDO1FBQ2YsSUFBSSxDQUFDLFVBQVUsR0FBRyxDQUFDLENBQUM7UUFDcEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUM7S0FDbkI7SUFHRDs7OztPQUlHOzs7Ozs7O0lBQ0gsNkJBQVU7Ozs7OztJQUFWLFVBQVcsTUFBZ0I7UUFDekIsSUFBSSxDQUFDLEdBQUcsR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQztRQUMxQixJQUFJLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQyxVQUFVLENBQUM7UUFDcEMsSUFBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFBLElBQUk7O1lBQ3BDLElBQU0sTUFBTSxHQUFHLElBQUksUUFBUSxFQUFFLENBQUM7WUFDOUIsTUFBTSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN4QixNQUFNLENBQUMsTUFBTSxDQUFDO1NBQ2YsQ0FBQyxDQUFDO0tBQ0o7SUFFRDs7Ozs7T0FLRzs7Ozs7Ozs7SUFDSCxxQ0FBa0I7Ozs7Ozs7SUFBbEIsVUFBbUIsTUFBZ0IsRUFBRSxXQUFtQjtRQUF4RCxpQkFrQkM7UUFqQkMsRUFBRSxDQUFDLENBQUMsTUFBTSxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDekIsSUFBSSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUM7WUFDZixJQUFJLENBQUMsVUFBVSxHQUFHLENBQUMsQ0FBQztZQUNwQixJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDLEVBQXBDLENBQW9DLENBQUMsQ0FBQztTQUNuRTtRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sSUFBSSxDQUFDLEtBQUssR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDO1lBQzFCLElBQUksQ0FBQyxVQUFVLEdBQUcsTUFBTSxDQUFDLEtBQUssR0FBRyxXQUFXLENBQUM7WUFDN0MsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsVUFBQSxHQUFHOztnQkFDdEIsSUFBTSxxQkFBcUIsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxJQUFJLENBQUMsS0FBSyxFQUFWLENBQVUsQ0FBQyxDQUFDLENBQUM7O2dCQUMzRixJQUFNLFFBQVEsR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLElBQUksQ0FBQyxHQUFHLEtBQUssR0FBRyxDQUFDLEdBQUcsRUFBcEIsQ0FBb0IsQ0FBQyxDQUFDO2dCQUNyRSxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ3pCLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLEVBQUUscUJBQXFCLENBQUMsQ0FBQztpQkFDNUQ7Z0JBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ04sT0FBTyxDQUFDLEtBQUssQ0FBQyxpQ0FBaUMsR0FBRyxLQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7aUJBQzdEO2FBQ0YsQ0FBQyxDQUFDO1NBQ0o7S0FDRjttQkE5REg7SUFnRUMsQ0FBQTs7OztBQTdERCxvQkE2REMiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIEJ1Y2tldCByZXNwb25zZSBmb3IgYSBnaXZlbiBFbGFzdGljc2VhcmNoIGFnZ3JlZ2F0aW9uLlxuICovXG5leHBvcnQgY2xhc3MgRXNCdWNrZXQge1xuXG4gIC8qKiBCdWNrZXQga2V5ICovXG4gIGtleTogc3RyaW5nO1xuICAvKiogRG9jIGNvdW50IGZvciB0aGUgYnVja2V0ICovXG4gIGNvdW50OiBudW1iZXI7XG4gIC8qKiBQcm9wb3J0aW9uIHVzZWQgZm9yIHByb2dyZXNzIGJhciAqL1xuICBwcm9wb3J0aW9uOiBudW1iZXI7XG4gIC8qKiBMaXN0IG9mIHN1YiBidWNrZXRzICovXG4gIGJ1Y2tldHM6IEVzQnVja2V0W107XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gICAgdGhpcy5rZXkgPSAnJztcbiAgICB0aGlzLmNvdW50ID0gMDtcbiAgICB0aGlzLnByb3BvcnRpb24gPSAwO1xuICAgIHRoaXMuYnVja2V0cyA9IFtdO1xuICB9XG5cblxuICAvKipcbiAgICogSW5pdGlhbGl6YXRpb24gb2YgdGhlIGJ1Y2tldCBmcm9tIGEgYmFja2VuZCByZXNwb25zZS5cbiAgICogVXNlZCB0byBrZWVwIHRoZSBmdW5jdGlvbnMgb2YgdGhpcyBjbGFzcy5cbiAgICogQHBhcmFtIGJ1Y2tldCBSZXNwb25zZSBmcm9tIGJhY2tlbmQgdXNlZCBmb3IgaW5pdGlhbGl6YXRpb24uXG4gICAqL1xuICBpbml0QnVja2V0KGJ1Y2tldDogRXNCdWNrZXQpOiB2b2lkIHtcbiAgICB0aGlzLmtleSA9IGJ1Y2tldC5rZXk7XG4gICAgdGhpcy5jb3VudCA9IGJ1Y2tldC5jb3VudDtcbiAgICB0aGlzLnByb3BvcnRpb24gPSBidWNrZXQucHJvcG9ydGlvbjtcbiAgICB0aGlzLmJ1Y2tldHMgPSBidWNrZXQuYnVja2V0cy5tYXAoaXRlbSA9PiB7XG4gICAgICBjb25zdCByZXN1bHQgPSBuZXcgRXNCdWNrZXQoKTtcbiAgICAgIHJlc3VsdC5pbml0QnVja2V0KGl0ZW0pO1xuICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICB9KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBVcGRhdGUgY291bnRzIGZvciBidWNrZXRzIGFuZCBhbGwgc3ViLWJ1Y2tldHMgZ2l2ZW4gYSBidWNrZXQgZnJvbSBiYWNrZW5kIHJlc3BvbnNlLlxuICAgKiBVc2VkIHRvIGtlZXAgdGhlIGZ1bmN0aW9ucyBvZiB0aGlzIGNsYXNzLlxuICAgKiBAcGFyYW0gYnVja2V0IFJlc3BvbnNlIGZyb20gYmFja2VuZCB1c2VkIGZvciBjb3VudCB1cGRhdGUuXG4gICAqIEBwYXJhbSBtYXhEb2NDb3VudCBNYXhpbXVtIERvYyBDb3VudCBmb3IgdGhlIGN1cnJlbnQgYnVja2V0IGxldmVsLCB1c2VkIHRvIGNvbXB1dGUgcHJvcG9ydGlvblxuICAgKi9cbiAgdXBkYXRlQnVja2V0Q291bnRzKGJ1Y2tldDogRXNCdWNrZXQsIG1heERvY0NvdW50OiBudW1iZXIpOiB2b2lkIHtcbiAgICBpZiAoYnVja2V0ID09PSB1bmRlZmluZWQpIHtcbiAgICAgIHRoaXMuY291bnQgPSAwO1xuICAgICAgdGhpcy5wcm9wb3J0aW9uID0gMDtcbiAgICAgIHRoaXMuYnVja2V0cy5mb3JFYWNoKHN1YiA9PiBzdWIudXBkYXRlQnVja2V0Q291bnRzKHVuZGVmaW5lZCwgMCkpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmNvdW50ID0gYnVja2V0LmNvdW50O1xuICAgICAgdGhpcy5wcm9wb3J0aW9uID0gYnVja2V0LmNvdW50IC8gbWF4RG9jQ291bnQ7XG4gICAgICB0aGlzLmJ1Y2tldHMuZm9yRWFjaChzdWIgPT4ge1xuICAgICAgICBjb25zdCBzdWJCdWNrZXRzbWF4RG9jQ291bnQgPSBNYXRoLm1heC5hcHBseShudWxsLCBidWNrZXQuYnVja2V0cy5tYXAoaXRlbSA9PiBpdGVtLmNvdW50KSk7XG4gICAgICAgIGNvbnN0IG1hdGNoaW5nID0gYnVja2V0LmJ1Y2tldHMuZmlsdGVyKGl0ZW0gPT4gaXRlbS5rZXkgPT09IHN1Yi5rZXkpO1xuICAgICAgICBpZiAobWF0Y2hpbmcubGVuZ3RoIDw9IDEpIHtcbiAgICAgICAgICBzdWIudXBkYXRlQnVja2V0Q291bnRzKG1hdGNoaW5nWzBdLCBzdWJCdWNrZXRzbWF4RG9jQ291bnQpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGNvbnNvbGUuZXJyb3IoJ011bHRpcGxlIGJ1Y2tldHMgZm91bmQgZm9yIGtleSAnICsgdGhpcy5rZXkpO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9XG4gIH1cblxufVxuIl19