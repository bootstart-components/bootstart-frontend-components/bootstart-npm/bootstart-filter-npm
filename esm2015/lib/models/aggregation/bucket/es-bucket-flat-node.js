/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { EsBucket } from './es-bucket';
/**
 * Extension of ES bucket to flat node.
 */
export class EsBucketFlatNode extends EsBucket {
}
if (false) {
    /**
     * The node level
     * @type {?}
     */
    EsBucketFlatNode.prototype.level;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXMtYnVja2V0LWZsYXQtbm9kZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Jvb3RzdGFydC1maWx0ZXIvIiwic291cmNlcyI6WyJsaWIvbW9kZWxzL2FnZ3JlZ2F0aW9uL2J1Y2tldC9lcy1idWNrZXQtZmxhdC1ub2RlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsUUFBUSxFQUFDLE1BQU0sYUFBYSxDQUFDOzs7O0FBS3JDLE1BQU0sdUJBQXdCLFNBQVEsUUFBUTtDQUc3QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7RXNCdWNrZXR9IGZyb20gJy4vZXMtYnVja2V0JztcblxuLyoqXG4gKiBFeHRlbnNpb24gb2YgRVMgYnVja2V0IHRvIGZsYXQgbm9kZS5cbiAqL1xuZXhwb3J0IGNsYXNzIEVzQnVja2V0RmxhdE5vZGUgZXh0ZW5kcyBFc0J1Y2tldCB7XG4gIC8qKiBUaGUgbm9kZSBsZXZlbCAqL1xuICBsZXZlbDogbnVtYmVyO1xufVxuIl19