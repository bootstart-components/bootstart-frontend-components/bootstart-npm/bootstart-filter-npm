export declare class SliderRange {
    min: number;
    max: number;
    constructor(min: number, max: number);
}
