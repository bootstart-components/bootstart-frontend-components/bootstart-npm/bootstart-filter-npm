import { EsBucket } from './es-bucket';
/**
 * Extension of ES bucket to flat node.
 */
export declare class EsBucketFlatNode extends EsBucket {
    /** The node level */
    level: number;
}
