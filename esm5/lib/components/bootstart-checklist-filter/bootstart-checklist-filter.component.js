/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { EsBucketAggregation } from '../../models/aggregation/bucket/es-bucket-aggregation';
import { CategorizedFilter } from '../../models/filter/categorized-filter';
import { SelectionModel } from '@angular/cdk/collections';
import { BehaviorSubject } from 'rxjs';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material';
import { EsBucketFlatNode } from '../../models/aggregation/bucket/es-bucket-flat-node';
var BootstartChecklistFilterComponent = /** @class */ (function () {
    function BootstartChecklistFilterComponent() {
        var _this = this;
        /**
         * Categorized filter emitter
         */
        this.onChange = new EventEmitter();
        /**
         * Selection for checklist
         */
        this.checklistSelection = new SelectionModel(true);
        /**
         * Selection for emitter
         */
        this.emitterSelection = new SelectionModel(true);
        /**
         * Elements discarded from selection (e.g. children of a node if all children are checked)
         */
        this.discardedEmitterSelection = new SelectionModel(true);
        /**
         * Map from flat node to nested node. This helps us finding the nested node to be modified
         */
        this.flatNodeMap = new Map();
        /**
         * Map from nested node to flattened node. This helps us to keep the same object for selection
         */
        this.nestedNodeMap = new Map();
        this.dataChange = new BehaviorSubject([]);
        this.getLevel = function (node) { return node.level; };
        this.isExpandable = function (node) { return node.buckets.length > 0; };
        this.getChildren = function (node) { return node.buckets; };
        this.hasChild = function (_, _nodeData) { return _nodeData.buckets.length > 0; };
        this.treeFlattener = new MatTreeFlattener(this._transformer, this.getLevel, this.isExpandable, this.getChildren);
        this.treeControl = new FlatTreeControl(this.getLevel, this.isExpandable);
        this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
        this.dataChange.subscribe(function (data) { return _this.dataSource.data = data; });
    }
    /**
     * @return {?}
     */
    BootstartChecklistFilterComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        if (this.translationPrefix === undefined) {
            this.translationPrefix = '';
        }
    };
    /**
     * @return {?}
     */
    BootstartChecklistFilterComponent.prototype.ngDoCheck = /**
     * @return {?}
     */
    function () {
        this.dataChange.next(this.aggregation.buckets);
    };
    /** Whether all the descendants of the node are selected */
    /**
     * Whether all the descendants of the node are selected
     * @param {?} node
     * @return {?}
     */
    BootstartChecklistFilterComponent.prototype.descendantsAllSelected = /**
     * Whether all the descendants of the node are selected
     * @param {?} node
     * @return {?}
     */
    function (node) {
        var _this = this;
        /** @type {?} */
        var descendants = this.treeControl.getDescendants(node);
        if (descendants.length === 0) {
            return false;
        }
        return descendants.every(function (child) { return _this.checklistSelection.isSelected(child); });
    };
    /** Whether part of descendants are selected */
    /**
     * Whether part of descendants are selected
     * @param {?} node
     * @return {?}
     */
    BootstartChecklistFilterComponent.prototype.descendantsPartiallySelected = /**
     * Whether part of descendants are selected
     * @param {?} node
     * @return {?}
     */
    function (node) {
        var _this = this;
        /** @type {?} */
        var descendants = this.treeControl.getDescendants(node);
        /** @type {?} */
        var result = descendants.some(function (child) { return _this.checklistSelection.isSelected(child); });
        return result && !this.descendantsAllSelected(node);
    };
    /** Toggle the Elasticsearch bucket node item. Select/deselect all the descendants node */
    /**
     * Toggle the Elasticsearch bucket node item. Select/deselect all the descendants node
     * @param {?} node
     * @return {?}
     */
    BootstartChecklistFilterComponent.prototype.bucketSelectionToggle = /**
     * Toggle the Elasticsearch bucket node item. Select/deselect all the descendants node
     * @param {?} node
     * @return {?}
     */
    function (node) {
        this.checklistSelection.toggle(node);
        /** @type {?} */
        var descendants = this.treeControl.getDescendants(node);
        this.checklistSelection.isSelected(node)
            ? (_a = this.checklistSelection).select.apply(_a, tslib_1.__spread(descendants)) : (_b = this.checklistSelection).deselect.apply(_b, tslib_1.__spread(descendants));
        this._checkAllParentsSelection(node);
        this._emitChange();
        var _a, _b;
    };
    /** Toggle a leaf Elasticsearch bucket item selection. Check all the parents to see if they changed */
    /**
     * Toggle a leaf Elasticsearch bucket item selection. Check all the parents to see if they changed
     * @param {?} node
     * @return {?}
     */
    BootstartChecklistFilterComponent.prototype.bucketLeafSelectionToggle = /**
     * Toggle a leaf Elasticsearch bucket item selection. Check all the parents to see if they changed
     * @param {?} node
     * @return {?}
     */
    function (node) {
        this.checklistSelection.toggle(node);
        this._checkAllParentsSelection(node);
        this._emitChange();
    };
    /**
     * @return {?}
     */
    BootstartChecklistFilterComponent.prototype._emitChange = /**
     * @return {?}
     */
    function () {
        this.emitterSelection.clear();
        this.discardedEmitterSelection.clear();
        this._addToEmitSelector(this.checklistSelection.selected);
        this.onChange.emit(this.emitterSelection.selected);
    };
    /**
     * @param {?} buckets
     * @return {?}
     */
    BootstartChecklistFilterComponent.prototype._addToEmitSelector = /**
     * @param {?} buckets
     * @return {?}
     */
    function (buckets) {
        var _this = this;
        buckets.forEach(function (element) {
            /** @type {?} */
            var flatElement = /** @type {?} */ (element);
            if (_this.checklistSelection.isSelected(element)) {
                if (_this.descendantsAllSelected(flatElement)) {
                    /** @type {?} */
                    var descendants = _this.treeControl.getDescendants(flatElement);
                    (_a = _this.discardedEmitterSelection).select.apply(_a, tslib_1.__spread(descendants));
                }
                /** @type {?} */
                var parent_1 = _this._getParentNode(flatElement);
                while (parent_1) {
                    _this._addToEmitSelector([parent_1]);
                    parent_1 = _this._getParentNode(parent_1);
                }
                if (!_this.discardedEmitterSelection.isSelected(element)) {
                    /** @type {?} */
                    var filter = new CategorizedFilter(flatElement);
                    /** @type {?} */
                    var eltParent = _this._getParentNode(flatElement);
                    while (eltParent) {
                        filter.parents.push(new CategorizedFilter(eltParent));
                        eltParent = _this._getParentNode(eltParent);
                    }
                    _this.emitterSelection.select(filter);
                    _this.discardedEmitterSelection.select(element);
                }
            }
            var _a;
        });
    };
    /**
     * Transformer to convert nested node to flat node. Record the nodes in maps for later use.
     * @param {?} node
     * @param {?} level
     * @return {?}
     */
    BootstartChecklistFilterComponent.prototype._transformer = /**
     * Transformer to convert nested node to flat node. Record the nodes in maps for later use.
     * @param {?} node
     * @param {?} level
     * @return {?}
     */
    function (node, level) {
        if (!this.nestedNodeMap) {
            this.nestedNodeMap = new Map();
        }
        if (!this.flatNodeMap) {
            this.flatNodeMap = new Map();
        }
        /** @type {?} */
        var existingNode = this.nestedNodeMap.get(node);
        /** @type {?} */
        var flatNode = existingNode && existingNode.key === node.key
            ? existingNode
            : new EsBucketFlatNode();
        flatNode.key = node.key;
        flatNode.count = node.count;
        flatNode.buckets = node.buckets;
        flatNode.proportion = node.proportion;
        flatNode.level = level;
        this.flatNodeMap.set(flatNode, node);
        this.nestedNodeMap.set(node, flatNode);
        return flatNode;
    };
    /**
     * Checks all the parents when a node is selected/unselected
     * @param {?} node
     * @return {?}
     */
    BootstartChecklistFilterComponent.prototype._checkAllParentsSelection = /**
     * Checks all the parents when a node is selected/unselected
     * @param {?} node
     * @return {?}
     */
    function (node) {
        /** @type {?} */
        var parent = this._getParentNode(node);
        while (parent) {
            this._checkRootNodeSelection(parent);
            parent = this._getParentNode(parent);
        }
    };
    /**
     * Check root node checked state and change it accordingly
     * @param {?} node
     * @return {?}
     */
    BootstartChecklistFilterComponent.prototype._checkRootNodeSelection = /**
     * Check root node checked state and change it accordingly
     * @param {?} node
     * @return {?}
     */
    function (node) {
        /** @type {?} */
        var nodeSelected = this.checklistSelection.isSelected(node);
        if (nodeSelected && !this.descendantsAllSelected(node)) {
            this.checklistSelection.deselect(node);
        }
        else if (!nodeSelected && this.descendantsAllSelected(node)) {
            this.checklistSelection.select(node);
        }
    };
    /**
     * Get the parent node of a node
     * @param {?} node
     * @return {?}
     */
    BootstartChecklistFilterComponent.prototype._getParentNode = /**
     * Get the parent node of a node
     * @param {?} node
     * @return {?}
     */
    function (node) {
        /** @type {?} */
        var currentLevel = this.getLevel(node);
        if (currentLevel < 1) {
            return null;
        }
        /** @type {?} */
        var startIndex = this.treeControl.dataNodes.indexOf(node) - 1;
        for (var i = startIndex; i >= 0; i--) {
            /** @type {?} */
            var currentNode = this.treeControl.dataNodes[i];
            if (this.getLevel(currentNode) < currentLevel) {
                return currentNode;
            }
        }
        return null;
    };
    BootstartChecklistFilterComponent.decorators = [
        { type: Component, args: [{
                    selector: 'bootstart-checklist-filter',
                    template: "<mat-tree [dataSource]=\"dataSource\" [treeControl]=\"treeControl\" class=\"bootstart-checklist-tree\">\n\n  <!--Nested tree node-->\n  <mat-tree-node *matTreeNodeDef=\"let node; when: hasChild\"\n                 matTreeNodePadding\n                 class=\"row\"\n                 title=\"{{translationPrefix+node.key|translate}} ({{node.count}})\">\n\n    <div class=\"col-md-10 row\">\n\n      <mat-checkbox color=\"primary\"\n                    class=\"col-md-12\"\n                    [checked]=\"descendantsAllSelected(node)\"\n                    [indeterminate]=\"descendantsPartiallySelected(node)\"\n                    (change)=\"bucketSelectionToggle(node)\">\n\n        <div class=\"row\">\n          <div class=\"col-md-12 checkbox-label\"\n               [class.checkbox-label-zero-count]=\"node.count === 0\">\n            <span class=\"bucket-key\">{{translationPrefix+node.key|translate}}</span>\n            <span class=\"bucket-count\">&nbsp;&bull;&nbsp;{{node.count}}</span>\n          </div>\n        </div>\n\n      </mat-checkbox>\n\n      <mat-progress-bar class=\"col-md-12\"\n                        mode=\"determinate\"\n                        value=\"{{node.proportion*100 || 0}}\"></mat-progress-bar>\n\n    </div>\n\n    <button mat-icon-button matTreeNodeToggle class=\"col-md-2\">\n      <mat-icon class=\"mat-icon-rtl-mirror\">\n        {{treeControl.isExpanded(node) ? 'expand_more' : 'chevron_right'}}\n      </mat-icon>\n    </button>\n\n    <ul [class.checklist-tree-invisible]=\"!treeControl.isExpanded(node)\"\n        class=\"col-md-12 bootstart-tree-child-node\">\n      <ng-container matTreeNodeOutlet></ng-container>\n    </ul>\n\n  </mat-tree-node>\n\n\n  <!--Leaf node-->\n  <mat-tree-node *matTreeNodeDef=\"let node\"\n                 class=\"row\"\n                 matTreeNodeToggle matTreeNodePadding\n                 title=\"{{translationPrefix+node.key|translate}} ({{node.count}})\">\n\n    <div class=\"row col-md-12\">\n\n      <mat-checkbox color=\"primary\"\n                    class=\"col-md-12\"\n                    [checked]=\"checklistSelection.isSelected(node)\"\n                    (change)=\"bucketLeafSelectionToggle(node)\">\n\n        <div class=\"row\">\n          <div class=\"col-md-12 checkbox-label\"\n               [class.checkbox-label-zero-count]=\"node.count === 0\">\n            <span class=\"bucket-key\">{{translationPrefix+node.key|translate}}</span>\n            <span class=\"bucket-count\">&nbsp;&bull;&nbsp;{{node.count}}</span>\n          </div>\n        </div>\n\n      </mat-checkbox>\n\n      <mat-progress-bar class=\"col-md-12\"\n                        mode=\"determinate\"\n                        value=\"{{node.proportion*100 || 0}}\"></mat-progress-bar>\n\n    </div>\n\n  </mat-tree-node>\n\n</mat-tree>\n",
                    styles: [".bootstart-checklist-tree-invisible{display:none}.bootstart-checklist-tree{margin-top:15px}.bootstart-checklist-tree mat-tree-node{margin-top:-5px}.bootstart-checklist-tree mat-checkbox{cursor:pointer}.bootstart-checklist-tree button:focus{outline:0}.bootstart-checklist-tree mat-progress-bar{margin-top:-7px}.bootstart-tree-child-node{margin-bottom:-15px}.checkbox-label{white-space:initial!important}.bucket-key{font-size:small}.bucket-count{font-size:small;font-weight:700}.checkbox-label-zero-count span{color:gray!important}.checkbox-label-zero-count .bucket-count{font-size:small!important;font-weight:400!important}"]
                },] },
    ];
    /** @nocollapse */
    BootstartChecklistFilterComponent.ctorParameters = function () { return []; };
    BootstartChecklistFilterComponent.propDecorators = {
        aggregation: [{ type: Input }],
        translationPrefix: [{ type: Input }],
        onChange: [{ type: Output }]
    };
    return BootstartChecklistFilterComponent;
}());
export { BootstartChecklistFilterComponent };
if (false) {
    /**
     * Input bucket aggregation
     * @type {?}
     */
    BootstartChecklistFilterComponent.prototype.aggregation;
    /**
     * Translation prefix
     * @type {?}
     */
    BootstartChecklistFilterComponent.prototype.translationPrefix;
    /**
     * Categorized filter emitter
     * @type {?}
     */
    BootstartChecklistFilterComponent.prototype.onChange;
    /**
     * Selection for checklist
     * @type {?}
     */
    BootstartChecklistFilterComponent.prototype.checklistSelection;
    /**
     * Selection for emitter
     * @type {?}
     */
    BootstartChecklistFilterComponent.prototype.emitterSelection;
    /**
     * Elements discarded from selection (e.g. children of a node if all children are checked)
     * @type {?}
     */
    BootstartChecklistFilterComponent.prototype.discardedEmitterSelection;
    /**
     * Map from flat node to nested node. This helps us finding the nested node to be modified
     * @type {?}
     */
    BootstartChecklistFilterComponent.prototype.flatNodeMap;
    /**
     * Map from nested node to flattened node. This helps us to keep the same object for selection
     * @type {?}
     */
    BootstartChecklistFilterComponent.prototype.nestedNodeMap;
    /** @type {?} */
    BootstartChecklistFilterComponent.prototype.dataChange;
    /** @type {?} */
    BootstartChecklistFilterComponent.prototype.treeControl;
    /** @type {?} */
    BootstartChecklistFilterComponent.prototype.treeFlattener;
    /** @type {?} */
    BootstartChecklistFilterComponent.prototype.dataSource;
    /** @type {?} */
    BootstartChecklistFilterComponent.prototype.getLevel;
    /** @type {?} */
    BootstartChecklistFilterComponent.prototype.isExpandable;
    /** @type {?} */
    BootstartChecklistFilterComponent.prototype.getChildren;
    /** @type {?} */
    BootstartChecklistFilterComponent.prototype.hasChild;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LWNoZWNrbGlzdC1maWx0ZXIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vYm9vdHN0YXJ0LWZpbHRlci8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2Jvb3RzdGFydC1jaGVja2xpc3QtZmlsdGVyL2Jvb3RzdGFydC1jaGVja2xpc3QtZmlsdGVyLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQVcsWUFBWSxFQUFFLEtBQUssRUFBVSxNQUFNLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDdEYsT0FBTyxFQUFDLG1CQUFtQixFQUFDLE1BQU0sdURBQXVELENBQUM7QUFDMUYsT0FBTyxFQUFDLGlCQUFpQixFQUFDLE1BQU0sd0NBQXdDLENBQUM7QUFDekUsT0FBTyxFQUFDLGNBQWMsRUFBQyxNQUFNLDBCQUEwQixDQUFDO0FBQ3hELE9BQU8sRUFBQyxlQUFlLEVBQUMsTUFBTSxNQUFNLENBQUM7QUFDckMsT0FBTyxFQUFDLGVBQWUsRUFBQyxNQUFNLG1CQUFtQixDQUFDO0FBQ2xELE9BQU8sRUFBQyxxQkFBcUIsRUFBRSxnQkFBZ0IsRUFBQyxNQUFNLG1CQUFtQixDQUFDO0FBRTFFLE9BQU8sRUFBQyxnQkFBZ0IsRUFBQyxNQUFNLHFEQUFxRCxDQUFDOztJQTRIbkY7UUFBQSxpQkFLQzs7Ozt3QkFuQ29CLElBQUksWUFBWSxFQUF1Qjs7OztrQ0FHdkMsSUFBSSxjQUFjLENBQVcsSUFBSSxDQUFDOzs7O2dDQUdwQyxJQUFJLGNBQWMsQ0FBb0IsSUFBSSxDQUFDOzs7O3lDQUdsQyxJQUFJLGNBQWMsQ0FBVyxJQUFJLENBQUM7Ozs7MkJBR2hELElBQUksR0FBRyxFQUE4Qjs7Ozs2QkFHbkMsSUFBSSxHQUFHLEVBQThCOzBCQUVYLElBQUksZUFBZSxDQUFhLEVBQUUsQ0FBQzt3QkFRbEUsVUFBQyxJQUFzQixJQUFLLE9BQUEsSUFBSSxDQUFDLEtBQUssRUFBVixDQUFVOzRCQUNsQyxVQUFDLElBQXNCLElBQUssT0FBQSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQXZCLENBQXVCOzJCQUNwRCxVQUFDLElBQWMsSUFBaUIsT0FBQSxJQUFJLENBQUMsT0FBTyxFQUFaLENBQVk7d0JBQy9DLFVBQUMsQ0FBUyxFQUFFLFNBQTJCLElBQUssT0FBQSxTQUFTLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQTVCLENBQTRCO1FBR2pGLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDakgsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLGVBQWUsQ0FBbUIsSUFBSSxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDM0YsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLHFCQUFxQixDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ2xGLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsS0FBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLEdBQUcsSUFBSSxFQUEzQixDQUEyQixDQUFDLENBQUM7S0FDaEU7Ozs7SUFFRCxvREFBUTs7O0lBQVI7UUFDRSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQztZQUN6QyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsRUFBRSxDQUFDO1NBQzdCO0tBQ0Y7Ozs7SUFFRCxxREFBUzs7O0lBQVQ7UUFDRSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0tBQ2hEO0lBRUQsMkRBQTJEOzs7Ozs7SUFDM0Qsa0VBQXNCOzs7OztJQUF0QixVQUF1QixJQUFzQjtRQUE3QyxpQkFNQzs7UUFMQyxJQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMxRCxFQUFFLENBQUMsQ0FBQyxXQUFXLENBQUMsTUFBTSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDN0IsTUFBTSxDQUFDLEtBQUssQ0FBQztTQUNkO1FBQ0QsTUFBTSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsVUFBQSxLQUFLLElBQUksT0FBQSxLQUFJLENBQUMsa0JBQWtCLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxFQUF6QyxDQUF5QyxDQUFDLENBQUM7S0FDOUU7SUFFRCwrQ0FBK0M7Ozs7OztJQUMvQyx3RUFBNEI7Ozs7O0lBQTVCLFVBQTZCLElBQXNCO1FBQW5ELGlCQUlDOztRQUhDLElBQU0sV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDOztRQUMxRCxJQUFNLE1BQU0sR0FBRyxXQUFXLENBQUMsSUFBSSxDQUFDLFVBQUEsS0FBSyxJQUFJLE9BQUEsS0FBSSxDQUFDLGtCQUFrQixDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsRUFBekMsQ0FBeUMsQ0FBQyxDQUFDO1FBQ3BGLE1BQU0sQ0FBQyxNQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLENBQUM7S0FDckQ7SUFFRCwwRkFBMEY7Ozs7OztJQUMxRixpRUFBcUI7Ozs7O0lBQXJCLFVBQXNCLElBQXNCO1FBQzFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7O1FBQ3JDLElBQU0sV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzFELElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDO1lBQ3hDLENBQUMsQ0FBQyxDQUFBLEtBQUEsSUFBSSxDQUFDLGtCQUFrQixDQUFBLENBQUMsTUFBTSw0QkFBSSxXQUFXLEdBQy9DLENBQUMsQ0FBQyxDQUFBLEtBQUEsSUFBSSxDQUFDLGtCQUFrQixDQUFBLENBQUMsUUFBUSw0QkFBSSxXQUFXLEVBQUMsQ0FBQztRQUNuRCxJQUFJLENBQUMseUJBQXlCLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDckMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDOztLQUNwQjtJQUVELHNHQUFzRzs7Ozs7O0lBQ3RHLHFFQUF5Qjs7Ozs7SUFBekIsVUFBMEIsSUFBc0I7UUFDOUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNyQyxJQUFJLENBQUMseUJBQXlCLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDckMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO0tBQ3BCOzs7O0lBRU8sdURBQVc7Ozs7UUFDakIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUssRUFBRSxDQUFDO1FBQzlCLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUN2QyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzFELElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQzs7Ozs7O0lBRzdDLDhEQUFrQjs7OztjQUFDLE9BQW1COztRQUM1QyxPQUFPLENBQUMsT0FBTyxDQUFDLFVBQUEsT0FBTzs7WUFDckIsSUFBTSxXQUFXLHFCQUFxQixPQUFPLEVBQUM7WUFDOUMsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLGtCQUFrQixDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2hELEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7O29CQUM3QyxJQUFNLFdBQVcsR0FBRyxLQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQztvQkFDakUsQ0FBQSxLQUFBLEtBQUksQ0FBQyx5QkFBeUIsQ0FBQSxDQUFDLE1BQU0sNEJBQUksV0FBVyxHQUFFO2lCQUN2RDs7Z0JBRUQsSUFBSSxRQUFNLEdBQTRCLEtBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQ3ZFLE9BQU8sUUFBTSxFQUFFLENBQUM7b0JBQ2QsS0FBSSxDQUFDLGtCQUFrQixDQUFDLENBQUMsUUFBTSxDQUFDLENBQUMsQ0FBQztvQkFDbEMsUUFBTSxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsUUFBTSxDQUFDLENBQUM7aUJBQ3RDO2dCQUVELEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLHlCQUF5QixDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7O29CQUN4RCxJQUFNLE1BQU0sR0FBRyxJQUFJLGlCQUFpQixDQUFDLFdBQVcsQ0FBQyxDQUFDOztvQkFDbEQsSUFBSSxTQUFTLEdBQTRCLEtBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLENBQUM7b0JBQzFFLE9BQU8sU0FBUyxFQUFFLENBQUM7d0JBQ2pCLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksaUJBQWlCLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQzt3QkFDdEQsU0FBUyxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLENBQUM7cUJBQzVDO29CQUNELEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUM7b0JBQ3JDLEtBQUksQ0FBQyx5QkFBeUIsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7aUJBQ2hEO2FBQ0Y7O1NBQ0YsQ0FBQyxDQUFDOzs7Ozs7OztJQUlHLHdEQUFZOzs7Ozs7Y0FBQyxJQUFjLEVBQUUsS0FBYTtRQUNoRCxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDO1lBQ3hCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxHQUFHLEVBQThCLENBQUM7U0FDNUQ7UUFDRCxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1lBQ3RCLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxHQUFHLEVBQThCLENBQUM7U0FDMUQ7O1FBQ0QsSUFBTSxZQUFZLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7O1FBQ2xELElBQU0sUUFBUSxHQUFHLFlBQVksSUFBSSxZQUFZLENBQUMsR0FBRyxLQUFLLElBQUksQ0FBQyxHQUFHO1lBQzdDLENBQUMsQ0FBQyxZQUFZO1lBQ2QsQ0FBQyxDQUFDLElBQUksZ0JBQWdCLEVBQUUsQ0FBQztRQUMxQyxRQUFRLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUM7UUFDeEIsUUFBUSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBQzVCLFFBQVEsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQztRQUNoQyxRQUFRLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7UUFDdEMsUUFBUSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7UUFDdkIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3JDLElBQUksQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxRQUFRLENBQUMsQ0FBQztRQUN2QyxNQUFNLENBQUMsUUFBUSxDQUFDOzs7Ozs7O0lBSVYscUVBQXlCOzs7OztjQUFDLElBQXNCOztRQUN0RCxJQUFJLE1BQU0sR0FBNEIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNoRSxPQUFPLE1BQU0sRUFBRSxDQUFDO1lBQ2QsSUFBSSxDQUFDLHVCQUF1QixDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ3JDLE1BQU0sR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1NBQ3RDOzs7Ozs7O0lBSUssbUVBQXVCOzs7OztjQUFDLElBQXNCOztRQUNwRCxJQUFNLFlBQVksR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzlELEVBQUUsQ0FBQyxDQUFDLFlBQVksSUFBSSxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDdkQsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUN4QztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzlELElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDdEM7Ozs7Ozs7SUFJSywwREFBYzs7Ozs7Y0FBQyxJQUFzQjs7UUFDM0MsSUFBTSxZQUFZLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUV6QyxFQUFFLENBQUMsQ0FBQyxZQUFZLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNyQixNQUFNLENBQUMsSUFBSSxDQUFDO1NBQ2I7O1FBRUQsSUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUVoRSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxVQUFVLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDOztZQUNyQyxJQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUVsRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxHQUFHLFlBQVksQ0FBQyxDQUFDLENBQUM7Z0JBQzlDLE1BQU0sQ0FBQyxXQUFXLENBQUM7YUFDcEI7U0FDRjtRQUNELE1BQU0sQ0FBQyxJQUFJLENBQUM7OztnQkEzUWYsU0FBUyxTQUFDO29CQUNFLFFBQVEsRUFBSyw0QkFBNEI7b0JBQ3pDLFFBQVEsRUFBRSw2dkZBOEV0QjtvQkFDWSxNQUFNLEVBQUUsQ0FBQyxnbkJBQWduQixDQUFDO2lCQUMzbkI7Ozs7OzhCQUlULEtBQUs7b0NBR0wsS0FBSzsyQkFHTCxNQUFNOzs0Q0F0R1Q7O1NBNkZhLGlDQUFpQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBEb0NoZWNrLCBFdmVudEVtaXR0ZXIsIElucHV0LCBPbkluaXQsIE91dHB1dH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0VzQnVja2V0QWdncmVnYXRpb259IGZyb20gJy4uLy4uL21vZGVscy9hZ2dyZWdhdGlvbi9idWNrZXQvZXMtYnVja2V0LWFnZ3JlZ2F0aW9uJztcbmltcG9ydCB7Q2F0ZWdvcml6ZWRGaWx0ZXJ9IGZyb20gJy4uLy4uL21vZGVscy9maWx0ZXIvY2F0ZWdvcml6ZWQtZmlsdGVyJztcbmltcG9ydCB7U2VsZWN0aW9uTW9kZWx9IGZyb20gJ0Bhbmd1bGFyL2Nkay9jb2xsZWN0aW9ucyc7XG5pbXBvcnQge0JlaGF2aW9yU3ViamVjdH0gZnJvbSAncnhqcyc7XG5pbXBvcnQge0ZsYXRUcmVlQ29udHJvbH0gZnJvbSAnQGFuZ3VsYXIvY2RrL3RyZWUnO1xuaW1wb3J0IHtNYXRUcmVlRmxhdERhdGFTb3VyY2UsIE1hdFRyZWVGbGF0dGVuZXJ9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcbmltcG9ydCB7RXNCdWNrZXR9IGZyb20gJy4uLy4uL21vZGVscy9hZ2dyZWdhdGlvbi9idWNrZXQvZXMtYnVja2V0JztcbmltcG9ydCB7RXNCdWNrZXRGbGF0Tm9kZX0gZnJvbSAnLi4vLi4vbW9kZWxzL2FnZ3JlZ2F0aW9uL2J1Y2tldC9lcy1idWNrZXQtZmxhdC1ub2RlJztcblxuQENvbXBvbmVudCh7XG4gICAgICAgICAgICAgc2VsZWN0b3IgICA6ICdib290c3RhcnQtY2hlY2tsaXN0LWZpbHRlcicsXG4gICAgICAgICAgICAgdGVtcGxhdGU6IGA8bWF0LXRyZWUgW2RhdGFTb3VyY2VdPVwiZGF0YVNvdXJjZVwiIFt0cmVlQ29udHJvbF09XCJ0cmVlQ29udHJvbFwiIGNsYXNzPVwiYm9vdHN0YXJ0LWNoZWNrbGlzdC10cmVlXCI+XG5cbiAgPCEtLU5lc3RlZCB0cmVlIG5vZGUtLT5cbiAgPG1hdC10cmVlLW5vZGUgKm1hdFRyZWVOb2RlRGVmPVwibGV0IG5vZGU7IHdoZW46IGhhc0NoaWxkXCJcbiAgICAgICAgICAgICAgICAgbWF0VHJlZU5vZGVQYWRkaW5nXG4gICAgICAgICAgICAgICAgIGNsYXNzPVwicm93XCJcbiAgICAgICAgICAgICAgICAgdGl0bGU9XCJ7e3RyYW5zbGF0aW9uUHJlZml4K25vZGUua2V5fHRyYW5zbGF0ZX19ICh7e25vZGUuY291bnR9fSlcIj5cblxuICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtMTAgcm93XCI+XG5cbiAgICAgIDxtYXQtY2hlY2tib3ggY29sb3I9XCJwcmltYXJ5XCJcbiAgICAgICAgICAgICAgICAgICAgY2xhc3M9XCJjb2wtbWQtMTJcIlxuICAgICAgICAgICAgICAgICAgICBbY2hlY2tlZF09XCJkZXNjZW5kYW50c0FsbFNlbGVjdGVkKG5vZGUpXCJcbiAgICAgICAgICAgICAgICAgICAgW2luZGV0ZXJtaW5hdGVdPVwiZGVzY2VuZGFudHNQYXJ0aWFsbHlTZWxlY3RlZChub2RlKVwiXG4gICAgICAgICAgICAgICAgICAgIChjaGFuZ2UpPVwiYnVja2V0U2VsZWN0aW9uVG9nZ2xlKG5vZGUpXCI+XG5cbiAgICAgICAgPGRpdiBjbGFzcz1cInJvd1wiPlxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtMTIgY2hlY2tib3gtbGFiZWxcIlxuICAgICAgICAgICAgICAgW2NsYXNzLmNoZWNrYm94LWxhYmVsLXplcm8tY291bnRdPVwibm9kZS5jb3VudCA9PT0gMFwiPlxuICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJidWNrZXQta2V5XCI+e3t0cmFuc2xhdGlvblByZWZpeCtub2RlLmtleXx0cmFuc2xhdGV9fTwvc3Bhbj5cbiAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiYnVja2V0LWNvdW50XCI+Jm5ic3A7JmJ1bGw7Jm5ic3A7e3tub2RlLmNvdW50fX08L3NwYW4+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuXG4gICAgICA8L21hdC1jaGVja2JveD5cblxuICAgICAgPG1hdC1wcm9ncmVzcy1iYXIgY2xhc3M9XCJjb2wtbWQtMTJcIlxuICAgICAgICAgICAgICAgICAgICAgICAgbW9kZT1cImRldGVybWluYXRlXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPVwie3tub2RlLnByb3BvcnRpb24qMTAwIHx8IDB9fVwiPjwvbWF0LXByb2dyZXNzLWJhcj5cblxuICAgIDwvZGl2PlxuXG4gICAgPGJ1dHRvbiBtYXQtaWNvbi1idXR0b24gbWF0VHJlZU5vZGVUb2dnbGUgY2xhc3M9XCJjb2wtbWQtMlwiPlxuICAgICAgPG1hdC1pY29uIGNsYXNzPVwibWF0LWljb24tcnRsLW1pcnJvclwiPlxuICAgICAgICB7e3RyZWVDb250cm9sLmlzRXhwYW5kZWQobm9kZSkgPyAnZXhwYW5kX21vcmUnIDogJ2NoZXZyb25fcmlnaHQnfX1cbiAgICAgIDwvbWF0LWljb24+XG4gICAgPC9idXR0b24+XG5cbiAgICA8dWwgW2NsYXNzLmNoZWNrbGlzdC10cmVlLWludmlzaWJsZV09XCIhdHJlZUNvbnRyb2wuaXNFeHBhbmRlZChub2RlKVwiXG4gICAgICAgIGNsYXNzPVwiY29sLW1kLTEyIGJvb3RzdGFydC10cmVlLWNoaWxkLW5vZGVcIj5cbiAgICAgIDxuZy1jb250YWluZXIgbWF0VHJlZU5vZGVPdXRsZXQ+PC9uZy1jb250YWluZXI+XG4gICAgPC91bD5cblxuICA8L21hdC10cmVlLW5vZGU+XG5cblxuICA8IS0tTGVhZiBub2RlLS0+XG4gIDxtYXQtdHJlZS1ub2RlICptYXRUcmVlTm9kZURlZj1cImxldCBub2RlXCJcbiAgICAgICAgICAgICAgICAgY2xhc3M9XCJyb3dcIlxuICAgICAgICAgICAgICAgICBtYXRUcmVlTm9kZVRvZ2dsZSBtYXRUcmVlTm9kZVBhZGRpbmdcbiAgICAgICAgICAgICAgICAgdGl0bGU9XCJ7e3RyYW5zbGF0aW9uUHJlZml4K25vZGUua2V5fHRyYW5zbGF0ZX19ICh7e25vZGUuY291bnR9fSlcIj5cblxuICAgIDxkaXYgY2xhc3M9XCJyb3cgY29sLW1kLTEyXCI+XG5cbiAgICAgIDxtYXQtY2hlY2tib3ggY29sb3I9XCJwcmltYXJ5XCJcbiAgICAgICAgICAgICAgICAgICAgY2xhc3M9XCJjb2wtbWQtMTJcIlxuICAgICAgICAgICAgICAgICAgICBbY2hlY2tlZF09XCJjaGVja2xpc3RTZWxlY3Rpb24uaXNTZWxlY3RlZChub2RlKVwiXG4gICAgICAgICAgICAgICAgICAgIChjaGFuZ2UpPVwiYnVja2V0TGVhZlNlbGVjdGlvblRvZ2dsZShub2RlKVwiPlxuXG4gICAgICAgIDxkaXYgY2xhc3M9XCJyb3dcIj5cbiAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEyIGNoZWNrYm94LWxhYmVsXCJcbiAgICAgICAgICAgICAgIFtjbGFzcy5jaGVja2JveC1sYWJlbC16ZXJvLWNvdW50XT1cIm5vZGUuY291bnQgPT09IDBcIj5cbiAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiYnVja2V0LWtleVwiPnt7dHJhbnNsYXRpb25QcmVmaXgrbm9kZS5rZXl8dHJhbnNsYXRlfX08L3NwYW4+XG4gICAgICAgICAgICA8c3BhbiBjbGFzcz1cImJ1Y2tldC1jb3VudFwiPiZuYnNwOyZidWxsOyZuYnNwO3t7bm9kZS5jb3VudH19PC9zcGFuPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cblxuICAgICAgPC9tYXQtY2hlY2tib3g+XG5cbiAgICAgIDxtYXQtcHJvZ3Jlc3MtYmFyIGNsYXNzPVwiY29sLW1kLTEyXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1vZGU9XCJkZXRlcm1pbmF0ZVwiXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT1cInt7bm9kZS5wcm9wb3J0aW9uKjEwMCB8fCAwfX1cIj48L21hdC1wcm9ncmVzcy1iYXI+XG5cbiAgICA8L2Rpdj5cblxuICA8L21hdC10cmVlLW5vZGU+XG5cbjwvbWF0LXRyZWU+XG5gLFxuICAgICAgICAgICAgIHN0eWxlczogW2AuYm9vdHN0YXJ0LWNoZWNrbGlzdC10cmVlLWludmlzaWJsZXtkaXNwbGF5Om5vbmV9LmJvb3RzdGFydC1jaGVja2xpc3QtdHJlZXttYXJnaW4tdG9wOjE1cHh9LmJvb3RzdGFydC1jaGVja2xpc3QtdHJlZSBtYXQtdHJlZS1ub2Rle21hcmdpbi10b3A6LTVweH0uYm9vdHN0YXJ0LWNoZWNrbGlzdC10cmVlIG1hdC1jaGVja2JveHtjdXJzb3I6cG9pbnRlcn0uYm9vdHN0YXJ0LWNoZWNrbGlzdC10cmVlIGJ1dHRvbjpmb2N1c3tvdXRsaW5lOjB9LmJvb3RzdGFydC1jaGVja2xpc3QtdHJlZSBtYXQtcHJvZ3Jlc3MtYmFye21hcmdpbi10b3A6LTdweH0uYm9vdHN0YXJ0LXRyZWUtY2hpbGQtbm9kZXttYXJnaW4tYm90dG9tOi0xNXB4fS5jaGVja2JveC1sYWJlbHt3aGl0ZS1zcGFjZTppbml0aWFsIWltcG9ydGFudH0uYnVja2V0LWtleXtmb250LXNpemU6c21hbGx9LmJ1Y2tldC1jb3VudHtmb250LXNpemU6c21hbGw7Zm9udC13ZWlnaHQ6NzAwfS5jaGVja2JveC1sYWJlbC16ZXJvLWNvdW50IHNwYW57Y29sb3I6Z3JheSFpbXBvcnRhbnR9LmNoZWNrYm94LWxhYmVsLXplcm8tY291bnQgLmJ1Y2tldC1jb3VudHtmb250LXNpemU6c21hbGwhaW1wb3J0YW50O2ZvbnQtd2VpZ2h0OjQwMCFpbXBvcnRhbnR9YF1cbiAgICAgICAgICAgfSlcbmV4cG9ydCBjbGFzcyBCb290c3RhcnRDaGVja2xpc3RGaWx0ZXJDb21wb25lbnQgaW1wbGVtZW50cyBEb0NoZWNrLCBPbkluaXQge1xuXG4gIC8qKiBJbnB1dCBidWNrZXQgYWdncmVnYXRpb24gKi9cbiAgQElucHV0KCkgYWdncmVnYXRpb246IEVzQnVja2V0QWdncmVnYXRpb247XG5cbiAgLyoqIFRyYW5zbGF0aW9uIHByZWZpeCAqL1xuICBASW5wdXQoKSB0cmFuc2xhdGlvblByZWZpeDogc3RyaW5nO1xuXG4gIC8qKiBDYXRlZ29yaXplZCBmaWx0ZXIgZW1pdHRlciAqL1xuICBAT3V0cHV0KCkgb25DaGFuZ2UgPSBuZXcgRXZlbnRFbWl0dGVyPENhdGVnb3JpemVkRmlsdGVyW10+KCk7XG5cbiAgLyoqIFNlbGVjdGlvbiBmb3IgY2hlY2tsaXN0ICovXG4gIGNoZWNrbGlzdFNlbGVjdGlvbiA9IG5ldyBTZWxlY3Rpb25Nb2RlbDxFc0J1Y2tldD4odHJ1ZSk7XG5cbiAgLyoqIFNlbGVjdGlvbiBmb3IgZW1pdHRlciAqL1xuICBlbWl0dGVyU2VsZWN0aW9uID0gbmV3IFNlbGVjdGlvbk1vZGVsPENhdGVnb3JpemVkRmlsdGVyPih0cnVlKTtcblxuICAvKiogRWxlbWVudHMgZGlzY2FyZGVkIGZyb20gc2VsZWN0aW9uIChlLmcuIGNoaWxkcmVuIG9mIGEgbm9kZSBpZiBhbGwgY2hpbGRyZW4gYXJlIGNoZWNrZWQpICovXG4gIGRpc2NhcmRlZEVtaXR0ZXJTZWxlY3Rpb24gPSBuZXcgU2VsZWN0aW9uTW9kZWw8RXNCdWNrZXQ+KHRydWUpO1xuXG4gIC8qKiBNYXAgZnJvbSBmbGF0IG5vZGUgdG8gbmVzdGVkIG5vZGUuIFRoaXMgaGVscHMgdXMgZmluZGluZyB0aGUgbmVzdGVkIG5vZGUgdG8gYmUgbW9kaWZpZWQgKi9cbiAgZmxhdE5vZGVNYXAgPSBuZXcgTWFwPEVzQnVja2V0RmxhdE5vZGUsIEVzQnVja2V0PigpO1xuXG4gIC8qKiBNYXAgZnJvbSBuZXN0ZWQgbm9kZSB0byBmbGF0dGVuZWQgbm9kZS4gVGhpcyBoZWxwcyB1cyB0byBrZWVwIHRoZSBzYW1lIG9iamVjdCBmb3Igc2VsZWN0aW9uICovXG4gIG5lc3RlZE5vZGVNYXAgPSBuZXcgTWFwPEVzQnVja2V0LCBFc0J1Y2tldEZsYXROb2RlPigpO1xuXG4gIGRhdGFDaGFuZ2U6IEJlaGF2aW9yU3ViamVjdDxFc0J1Y2tldFtdPiA9IG5ldyBCZWhhdmlvclN1YmplY3Q8RXNCdWNrZXRbXT4oW10pO1xuXG4gIHRyZWVDb250cm9sOiBGbGF0VHJlZUNvbnRyb2w8RXNCdWNrZXRGbGF0Tm9kZT47XG5cbiAgdHJlZUZsYXR0ZW5lcjogTWF0VHJlZUZsYXR0ZW5lcjxFc0J1Y2tldCwgRXNCdWNrZXRGbGF0Tm9kZT47XG5cbiAgZGF0YVNvdXJjZTogTWF0VHJlZUZsYXREYXRhU291cmNlPEVzQnVja2V0LCBFc0J1Y2tldEZsYXROb2RlPjtcblxuICBnZXRMZXZlbCA9IChub2RlOiBFc0J1Y2tldEZsYXROb2RlKSA9PiBub2RlLmxldmVsO1xuICBpc0V4cGFuZGFibGUgPSAobm9kZTogRXNCdWNrZXRGbGF0Tm9kZSkgPT4gbm9kZS5idWNrZXRzLmxlbmd0aCA+IDA7XG4gIGdldENoaWxkcmVuID0gKG5vZGU6IEVzQnVja2V0KTogRXNCdWNrZXRbXSA9PiBub2RlLmJ1Y2tldHM7XG4gIGhhc0NoaWxkID0gKF86IG51bWJlciwgX25vZGVEYXRhOiBFc0J1Y2tldEZsYXROb2RlKSA9PiBfbm9kZURhdGEuYnVja2V0cy5sZW5ndGggPiAwO1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHRoaXMudHJlZUZsYXR0ZW5lciA9IG5ldyBNYXRUcmVlRmxhdHRlbmVyKHRoaXMuX3RyYW5zZm9ybWVyLCB0aGlzLmdldExldmVsLCB0aGlzLmlzRXhwYW5kYWJsZSwgdGhpcy5nZXRDaGlsZHJlbik7XG4gICAgdGhpcy50cmVlQ29udHJvbCA9IG5ldyBGbGF0VHJlZUNvbnRyb2w8RXNCdWNrZXRGbGF0Tm9kZT4odGhpcy5nZXRMZXZlbCwgdGhpcy5pc0V4cGFuZGFibGUpO1xuICAgIHRoaXMuZGF0YVNvdXJjZSA9IG5ldyBNYXRUcmVlRmxhdERhdGFTb3VyY2UodGhpcy50cmVlQ29udHJvbCwgdGhpcy50cmVlRmxhdHRlbmVyKTtcbiAgICB0aGlzLmRhdGFDaGFuZ2Uuc3Vic2NyaWJlKGRhdGEgPT4gdGhpcy5kYXRhU291cmNlLmRhdGEgPSBkYXRhKTtcbiAgfVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIGlmICh0aGlzLnRyYW5zbGF0aW9uUHJlZml4ID09PSB1bmRlZmluZWQpIHtcbiAgICAgIHRoaXMudHJhbnNsYXRpb25QcmVmaXggPSAnJztcbiAgICB9XG4gIH1cblxuICBuZ0RvQ2hlY2soKTogdm9pZCB7XG4gICAgdGhpcy5kYXRhQ2hhbmdlLm5leHQodGhpcy5hZ2dyZWdhdGlvbi5idWNrZXRzKTtcbiAgfVxuXG4gIC8qKiBXaGV0aGVyIGFsbCB0aGUgZGVzY2VuZGFudHMgb2YgdGhlIG5vZGUgYXJlIHNlbGVjdGVkICovXG4gIGRlc2NlbmRhbnRzQWxsU2VsZWN0ZWQobm9kZTogRXNCdWNrZXRGbGF0Tm9kZSk6IGJvb2xlYW4ge1xuICAgIGNvbnN0IGRlc2NlbmRhbnRzID0gdGhpcy50cmVlQ29udHJvbC5nZXREZXNjZW5kYW50cyhub2RlKTtcbiAgICBpZiAoZGVzY2VuZGFudHMubGVuZ3RoID09PSAwKSB7XG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuICAgIHJldHVybiBkZXNjZW5kYW50cy5ldmVyeShjaGlsZCA9PiB0aGlzLmNoZWNrbGlzdFNlbGVjdGlvbi5pc1NlbGVjdGVkKGNoaWxkKSk7XG4gIH1cblxuICAvKiogV2hldGhlciBwYXJ0IG9mIGRlc2NlbmRhbnRzIGFyZSBzZWxlY3RlZCAqL1xuICBkZXNjZW5kYW50c1BhcnRpYWxseVNlbGVjdGVkKG5vZGU6IEVzQnVja2V0RmxhdE5vZGUpOiBib29sZWFuIHtcbiAgICBjb25zdCBkZXNjZW5kYW50cyA9IHRoaXMudHJlZUNvbnRyb2wuZ2V0RGVzY2VuZGFudHMobm9kZSk7XG4gICAgY29uc3QgcmVzdWx0ID0gZGVzY2VuZGFudHMuc29tZShjaGlsZCA9PiB0aGlzLmNoZWNrbGlzdFNlbGVjdGlvbi5pc1NlbGVjdGVkKGNoaWxkKSk7XG4gICAgcmV0dXJuIHJlc3VsdCAmJiAhdGhpcy5kZXNjZW5kYW50c0FsbFNlbGVjdGVkKG5vZGUpO1xuICB9XG5cbiAgLyoqIFRvZ2dsZSB0aGUgRWxhc3RpY3NlYXJjaCBidWNrZXQgbm9kZSBpdGVtLiBTZWxlY3QvZGVzZWxlY3QgYWxsIHRoZSBkZXNjZW5kYW50cyBub2RlICovXG4gIGJ1Y2tldFNlbGVjdGlvblRvZ2dsZShub2RlOiBFc0J1Y2tldEZsYXROb2RlKTogdm9pZCB7XG4gICAgdGhpcy5jaGVja2xpc3RTZWxlY3Rpb24udG9nZ2xlKG5vZGUpO1xuICAgIGNvbnN0IGRlc2NlbmRhbnRzID0gdGhpcy50cmVlQ29udHJvbC5nZXREZXNjZW5kYW50cyhub2RlKTtcbiAgICB0aGlzLmNoZWNrbGlzdFNlbGVjdGlvbi5pc1NlbGVjdGVkKG5vZGUpXG4gICAgPyB0aGlzLmNoZWNrbGlzdFNlbGVjdGlvbi5zZWxlY3QoLi4uZGVzY2VuZGFudHMpXG4gICAgOiB0aGlzLmNoZWNrbGlzdFNlbGVjdGlvbi5kZXNlbGVjdCguLi5kZXNjZW5kYW50cyk7XG4gICAgdGhpcy5fY2hlY2tBbGxQYXJlbnRzU2VsZWN0aW9uKG5vZGUpO1xuICAgIHRoaXMuX2VtaXRDaGFuZ2UoKTtcbiAgfVxuXG4gIC8qKiBUb2dnbGUgYSBsZWFmIEVsYXN0aWNzZWFyY2ggYnVja2V0IGl0ZW0gc2VsZWN0aW9uLiBDaGVjayBhbGwgdGhlIHBhcmVudHMgdG8gc2VlIGlmIHRoZXkgY2hhbmdlZCAqL1xuICBidWNrZXRMZWFmU2VsZWN0aW9uVG9nZ2xlKG5vZGU6IEVzQnVja2V0RmxhdE5vZGUpOiB2b2lkIHtcbiAgICB0aGlzLmNoZWNrbGlzdFNlbGVjdGlvbi50b2dnbGUobm9kZSk7XG4gICAgdGhpcy5fY2hlY2tBbGxQYXJlbnRzU2VsZWN0aW9uKG5vZGUpO1xuICAgIHRoaXMuX2VtaXRDaGFuZ2UoKTtcbiAgfVxuXG4gIHByaXZhdGUgX2VtaXRDaGFuZ2UoKTogdm9pZCB7XG4gICAgdGhpcy5lbWl0dGVyU2VsZWN0aW9uLmNsZWFyKCk7XG4gICAgdGhpcy5kaXNjYXJkZWRFbWl0dGVyU2VsZWN0aW9uLmNsZWFyKCk7XG4gICAgdGhpcy5fYWRkVG9FbWl0U2VsZWN0b3IodGhpcy5jaGVja2xpc3RTZWxlY3Rpb24uc2VsZWN0ZWQpO1xuICAgIHRoaXMub25DaGFuZ2UuZW1pdCh0aGlzLmVtaXR0ZXJTZWxlY3Rpb24uc2VsZWN0ZWQpO1xuICB9XG5cbiAgcHJpdmF0ZSBfYWRkVG9FbWl0U2VsZWN0b3IoYnVja2V0czogRXNCdWNrZXRbXSk6IHZvaWQge1xuICAgIGJ1Y2tldHMuZm9yRWFjaChlbGVtZW50ID0+IHtcbiAgICAgIGNvbnN0IGZsYXRFbGVtZW50ID0gPEVzQnVja2V0RmxhdE5vZGU+ZWxlbWVudDtcbiAgICAgIGlmICh0aGlzLmNoZWNrbGlzdFNlbGVjdGlvbi5pc1NlbGVjdGVkKGVsZW1lbnQpKSB7XG4gICAgICAgIGlmICh0aGlzLmRlc2NlbmRhbnRzQWxsU2VsZWN0ZWQoZmxhdEVsZW1lbnQpKSB7XG4gICAgICAgICAgY29uc3QgZGVzY2VuZGFudHMgPSB0aGlzLnRyZWVDb250cm9sLmdldERlc2NlbmRhbnRzKGZsYXRFbGVtZW50KTtcbiAgICAgICAgICB0aGlzLmRpc2NhcmRlZEVtaXR0ZXJTZWxlY3Rpb24uc2VsZWN0KC4uLmRlc2NlbmRhbnRzKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGxldCBwYXJlbnQ6IEVzQnVja2V0RmxhdE5vZGUgfCBudWxsID0gdGhpcy5fZ2V0UGFyZW50Tm9kZShmbGF0RWxlbWVudCk7XG4gICAgICAgIHdoaWxlIChwYXJlbnQpIHtcbiAgICAgICAgICB0aGlzLl9hZGRUb0VtaXRTZWxlY3RvcihbcGFyZW50XSk7XG4gICAgICAgICAgcGFyZW50ID0gdGhpcy5fZ2V0UGFyZW50Tm9kZShwYXJlbnQpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKCF0aGlzLmRpc2NhcmRlZEVtaXR0ZXJTZWxlY3Rpb24uaXNTZWxlY3RlZChlbGVtZW50KSkge1xuICAgICAgICAgIGNvbnN0IGZpbHRlciA9IG5ldyBDYXRlZ29yaXplZEZpbHRlcihmbGF0RWxlbWVudCk7XG4gICAgICAgICAgbGV0IGVsdFBhcmVudDogRXNCdWNrZXRGbGF0Tm9kZSB8IG51bGwgPSB0aGlzLl9nZXRQYXJlbnROb2RlKGZsYXRFbGVtZW50KTtcbiAgICAgICAgICB3aGlsZSAoZWx0UGFyZW50KSB7XG4gICAgICAgICAgICBmaWx0ZXIucGFyZW50cy5wdXNoKG5ldyBDYXRlZ29yaXplZEZpbHRlcihlbHRQYXJlbnQpKTtcbiAgICAgICAgICAgIGVsdFBhcmVudCA9IHRoaXMuX2dldFBhcmVudE5vZGUoZWx0UGFyZW50KTtcbiAgICAgICAgICB9XG4gICAgICAgICAgdGhpcy5lbWl0dGVyU2VsZWN0aW9uLnNlbGVjdChmaWx0ZXIpO1xuICAgICAgICAgIHRoaXMuZGlzY2FyZGVkRW1pdHRlclNlbGVjdGlvbi5zZWxlY3QoZWxlbWVudCk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIC8qKiBUcmFuc2Zvcm1lciB0byBjb252ZXJ0IG5lc3RlZCBub2RlIHRvIGZsYXQgbm9kZS4gUmVjb3JkIHRoZSBub2RlcyBpbiBtYXBzIGZvciBsYXRlciB1c2UuICovXG4gIHByaXZhdGUgX3RyYW5zZm9ybWVyKG5vZGU6IEVzQnVja2V0LCBsZXZlbDogbnVtYmVyKTogRXNCdWNrZXRGbGF0Tm9kZSB7XG4gICAgaWYgKCF0aGlzLm5lc3RlZE5vZGVNYXApIHtcbiAgICAgIHRoaXMubmVzdGVkTm9kZU1hcCA9IG5ldyBNYXA8RXNCdWNrZXQsIEVzQnVja2V0RmxhdE5vZGU+KCk7XG4gICAgfVxuICAgIGlmICghdGhpcy5mbGF0Tm9kZU1hcCkge1xuICAgICAgdGhpcy5mbGF0Tm9kZU1hcCA9IG5ldyBNYXA8RXNCdWNrZXRGbGF0Tm9kZSwgRXNCdWNrZXQ+KCk7XG4gICAgfVxuICAgIGNvbnN0IGV4aXN0aW5nTm9kZSA9IHRoaXMubmVzdGVkTm9kZU1hcC5nZXQobm9kZSk7XG4gICAgY29uc3QgZmxhdE5vZGUgPSBleGlzdGluZ05vZGUgJiYgZXhpc3RpbmdOb2RlLmtleSA9PT0gbm9kZS5rZXlcbiAgICAgICAgICAgICAgICAgICAgID8gZXhpc3RpbmdOb2RlXG4gICAgICAgICAgICAgICAgICAgICA6IG5ldyBFc0J1Y2tldEZsYXROb2RlKCk7XG4gICAgZmxhdE5vZGUua2V5ID0gbm9kZS5rZXk7XG4gICAgZmxhdE5vZGUuY291bnQgPSBub2RlLmNvdW50O1xuICAgIGZsYXROb2RlLmJ1Y2tldHMgPSBub2RlLmJ1Y2tldHM7XG4gICAgZmxhdE5vZGUucHJvcG9ydGlvbiA9IG5vZGUucHJvcG9ydGlvbjtcbiAgICBmbGF0Tm9kZS5sZXZlbCA9IGxldmVsO1xuICAgIHRoaXMuZmxhdE5vZGVNYXAuc2V0KGZsYXROb2RlLCBub2RlKTtcbiAgICB0aGlzLm5lc3RlZE5vZGVNYXAuc2V0KG5vZGUsIGZsYXROb2RlKTtcbiAgICByZXR1cm4gZmxhdE5vZGU7XG4gIH1cblxuICAvKiogQ2hlY2tzIGFsbCB0aGUgcGFyZW50cyB3aGVuIGEgbm9kZSBpcyBzZWxlY3RlZC91bnNlbGVjdGVkICovXG4gIHByaXZhdGUgX2NoZWNrQWxsUGFyZW50c1NlbGVjdGlvbihub2RlOiBFc0J1Y2tldEZsYXROb2RlKTogdm9pZCB7XG4gICAgbGV0IHBhcmVudDogRXNCdWNrZXRGbGF0Tm9kZSB8IG51bGwgPSB0aGlzLl9nZXRQYXJlbnROb2RlKG5vZGUpO1xuICAgIHdoaWxlIChwYXJlbnQpIHtcbiAgICAgIHRoaXMuX2NoZWNrUm9vdE5vZGVTZWxlY3Rpb24ocGFyZW50KTtcbiAgICAgIHBhcmVudCA9IHRoaXMuX2dldFBhcmVudE5vZGUocGFyZW50KTtcbiAgICB9XG4gIH1cblxuICAvKiogQ2hlY2sgcm9vdCBub2RlIGNoZWNrZWQgc3RhdGUgYW5kIGNoYW5nZSBpdCBhY2NvcmRpbmdseSAqL1xuICBwcml2YXRlIF9jaGVja1Jvb3ROb2RlU2VsZWN0aW9uKG5vZGU6IEVzQnVja2V0RmxhdE5vZGUpOiB2b2lkIHtcbiAgICBjb25zdCBub2RlU2VsZWN0ZWQgPSB0aGlzLmNoZWNrbGlzdFNlbGVjdGlvbi5pc1NlbGVjdGVkKG5vZGUpO1xuICAgIGlmIChub2RlU2VsZWN0ZWQgJiYgIXRoaXMuZGVzY2VuZGFudHNBbGxTZWxlY3RlZChub2RlKSkge1xuICAgICAgdGhpcy5jaGVja2xpc3RTZWxlY3Rpb24uZGVzZWxlY3Qobm9kZSk7XG4gICAgfSBlbHNlIGlmICghbm9kZVNlbGVjdGVkICYmIHRoaXMuZGVzY2VuZGFudHNBbGxTZWxlY3RlZChub2RlKSkge1xuICAgICAgdGhpcy5jaGVja2xpc3RTZWxlY3Rpb24uc2VsZWN0KG5vZGUpO1xuICAgIH1cbiAgfVxuXG4gIC8qKiBHZXQgdGhlIHBhcmVudCBub2RlIG9mIGEgbm9kZSAqL1xuICBwcml2YXRlIF9nZXRQYXJlbnROb2RlKG5vZGU6IEVzQnVja2V0RmxhdE5vZGUpOiBFc0J1Y2tldEZsYXROb2RlIHwgbnVsbCB7XG4gICAgY29uc3QgY3VycmVudExldmVsID0gdGhpcy5nZXRMZXZlbChub2RlKTtcblxuICAgIGlmIChjdXJyZW50TGV2ZWwgPCAxKSB7XG4gICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG5cbiAgICBjb25zdCBzdGFydEluZGV4ID0gdGhpcy50cmVlQ29udHJvbC5kYXRhTm9kZXMuaW5kZXhPZihub2RlKSAtIDE7XG5cbiAgICBmb3IgKGxldCBpID0gc3RhcnRJbmRleDsgaSA+PSAwOyBpLS0pIHtcbiAgICAgIGNvbnN0IGN1cnJlbnROb2RlID0gdGhpcy50cmVlQ29udHJvbC5kYXRhTm9kZXNbaV07XG5cbiAgICAgIGlmICh0aGlzLmdldExldmVsKGN1cnJlbnROb2RlKSA8IGN1cnJlbnRMZXZlbCkge1xuICAgICAgICByZXR1cm4gY3VycmVudE5vZGU7XG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiBudWxsO1xuICB9XG59XG4iXX0=