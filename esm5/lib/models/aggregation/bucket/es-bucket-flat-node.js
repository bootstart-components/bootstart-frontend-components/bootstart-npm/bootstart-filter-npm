/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { EsBucket } from './es-bucket';
/**
 * Extension of ES bucket to flat node.
 */
var /**
 * Extension of ES bucket to flat node.
 */
EsBucketFlatNode = /** @class */ (function (_super) {
    tslib_1.__extends(EsBucketFlatNode, _super);
    function EsBucketFlatNode() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return EsBucketFlatNode;
}(EsBucket));
/**
 * Extension of ES bucket to flat node.
 */
export { EsBucketFlatNode };
if (false) {
    /**
     * The node level
     * @type {?}
     */
    EsBucketFlatNode.prototype.level;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXMtYnVja2V0LWZsYXQtbm9kZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Jvb3RzdGFydC1maWx0ZXIvIiwic291cmNlcyI6WyJsaWIvbW9kZWxzL2FnZ3JlZ2F0aW9uL2J1Y2tldC9lcy1idWNrZXQtZmxhdC1ub2RlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFDLFFBQVEsRUFBQyxNQUFNLGFBQWEsQ0FBQzs7OztBQUtyQzs7O0FBQUE7SUFBc0MsNENBQVE7Ozs7MkJBTDlDO0VBS3NDLFFBQVEsRUFHN0MsQ0FBQTs7OztBQUhELDRCQUdDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtFc0J1Y2tldH0gZnJvbSAnLi9lcy1idWNrZXQnO1xuXG4vKipcbiAqIEV4dGVuc2lvbiBvZiBFUyBidWNrZXQgdG8gZmxhdCBub2RlLlxuICovXG5leHBvcnQgY2xhc3MgRXNCdWNrZXRGbGF0Tm9kZSBleHRlbmRzIEVzQnVja2V0IHtcbiAgLyoqIFRoZSBub2RlIGxldmVsICovXG4gIGxldmVsOiBudW1iZXI7XG59XG4iXX0=