/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { BootstartChecklistFilterComponent } from './components/bootstart-checklist-filter/bootstart-checklist-filter.component';
import { BootstartHistogramSliderFilterComponent } from './components/bootstart-histogram-slider-filter/bootstart-histogram-slider-filter.component';
import { MatBadgeModule, MatButtonModule, MatCheckboxModule, MatIconModule, MatProgressBarModule, MatTreeModule } from '@angular/material';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { Ng5SliderModule } from 'ng5-slider';
export class BootstartFilterModule {
}
BootstartFilterModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule,
                    MatTreeModule,
                    MatCheckboxModule,
                    MatProgressBarModule,
                    MatIconModule,
                    MatButtonModule,
                    MatBadgeModule,
                    TranslateModule,
                    Ng5SliderModule
                ],
                declarations: [
                    BootstartChecklistFilterComponent,
                    BootstartHistogramSliderFilterComponent
                ],
                exports: [
                    BootstartChecklistFilterComponent,
                    BootstartHistogramSliderFilterComponent
                ]
            },] },
];

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LWZpbHRlci5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9ib290c3RhcnQtZmlsdGVyLyIsInNvdXJjZXMiOlsibGliL2Jvb3RzdGFydC1maWx0ZXIubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsUUFBUSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3ZDLE9BQU8sRUFBQyxpQ0FBaUMsRUFBQyxNQUFNLDhFQUE4RSxDQUFDO0FBQy9ILE9BQU8sRUFBQyx1Q0FBdUMsRUFBQyxNQUFNLDRGQUE0RixDQUFDO0FBQ25KLE9BQU8sRUFBQyxjQUFjLEVBQUUsZUFBZSxFQUFFLGlCQUFpQixFQUFFLGFBQWEsRUFBRSxvQkFBb0IsRUFBRSxhQUFhLEVBQUMsTUFBTSxtQkFBbUIsQ0FBQztBQUN6SSxPQUFPLEVBQUMsWUFBWSxFQUFDLE1BQU0saUJBQWlCLENBQUM7QUFDN0MsT0FBTyxFQUFDLGVBQWUsRUFBQyxNQUFNLHFCQUFxQixDQUFDO0FBQ3BELE9BQU8sRUFBQyxlQUFlLEVBQUMsTUFBTSxZQUFZLENBQUM7QUF1QjNDLE1BQU07OztZQXJCTCxRQUFRLFNBQUM7Z0JBQ0UsT0FBTyxFQUFPO29CQUNaLFlBQVk7b0JBQ1osYUFBYTtvQkFDYixpQkFBaUI7b0JBQ2pCLG9CQUFvQjtvQkFDcEIsYUFBYTtvQkFDYixlQUFlO29CQUNmLGNBQWM7b0JBQ2QsZUFBZTtvQkFDZixlQUFlO2lCQUNoQjtnQkFDRCxZQUFZLEVBQUU7b0JBQ1osaUNBQWlDO29CQUNqQyx1Q0FBdUM7aUJBQ3hDO2dCQUNELE9BQU8sRUFBTztvQkFDWixpQ0FBaUM7b0JBQ2pDLHVDQUF1QztpQkFDeEM7YUFDRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7TmdNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtCb290c3RhcnRDaGVja2xpc3RGaWx0ZXJDb21wb25lbnR9IGZyb20gJy4vY29tcG9uZW50cy9ib290c3RhcnQtY2hlY2tsaXN0LWZpbHRlci9ib290c3RhcnQtY2hlY2tsaXN0LWZpbHRlci5jb21wb25lbnQnO1xuaW1wb3J0IHtCb290c3RhcnRIaXN0b2dyYW1TbGlkZXJGaWx0ZXJDb21wb25lbnR9IGZyb20gJy4vY29tcG9uZW50cy9ib290c3RhcnQtaGlzdG9ncmFtLXNsaWRlci1maWx0ZXIvYm9vdHN0YXJ0LWhpc3RvZ3JhbS1zbGlkZXItZmlsdGVyLmNvbXBvbmVudCc7XG5pbXBvcnQge01hdEJhZGdlTW9kdWxlLCBNYXRCdXR0b25Nb2R1bGUsIE1hdENoZWNrYm94TW9kdWxlLCBNYXRJY29uTW9kdWxlLCBNYXRQcm9ncmVzc0Jhck1vZHVsZSwgTWF0VHJlZU1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xuaW1wb3J0IHtDb21tb25Nb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQge1RyYW5zbGF0ZU1vZHVsZX0gZnJvbSAnQG5neC10cmFuc2xhdGUvY29yZSc7XG5pbXBvcnQge05nNVNsaWRlck1vZHVsZX0gZnJvbSAnbmc1LXNsaWRlcic7XG5cbkBOZ01vZHVsZSh7XG4gICAgICAgICAgICBpbXBvcnRzICAgICA6IFtcbiAgICAgICAgICAgICAgQ29tbW9uTW9kdWxlLFxuICAgICAgICAgICAgICBNYXRUcmVlTW9kdWxlLFxuICAgICAgICAgICAgICBNYXRDaGVja2JveE1vZHVsZSxcbiAgICAgICAgICAgICAgTWF0UHJvZ3Jlc3NCYXJNb2R1bGUsXG4gICAgICAgICAgICAgIE1hdEljb25Nb2R1bGUsXG4gICAgICAgICAgICAgIE1hdEJ1dHRvbk1vZHVsZSxcbiAgICAgICAgICAgICAgTWF0QmFkZ2VNb2R1bGUsXG4gICAgICAgICAgICAgIFRyYW5zbGF0ZU1vZHVsZSxcbiAgICAgICAgICAgICAgTmc1U2xpZGVyTW9kdWxlXG4gICAgICAgICAgICBdLFxuICAgICAgICAgICAgZGVjbGFyYXRpb25zOiBbXG4gICAgICAgICAgICAgIEJvb3RzdGFydENoZWNrbGlzdEZpbHRlckNvbXBvbmVudCxcbiAgICAgICAgICAgICAgQm9vdHN0YXJ0SGlzdG9ncmFtU2xpZGVyRmlsdGVyQ29tcG9uZW50XG4gICAgICAgICAgICBdLFxuICAgICAgICAgICAgZXhwb3J0cyAgICAgOiBbXG4gICAgICAgICAgICAgIEJvb3RzdGFydENoZWNrbGlzdEZpbHRlckNvbXBvbmVudCxcbiAgICAgICAgICAgICAgQm9vdHN0YXJ0SGlzdG9ncmFtU2xpZGVyRmlsdGVyQ29tcG9uZW50XG4gICAgICAgICAgICBdXG4gICAgICAgICAgfSlcbmV4cG9ydCBjbGFzcyBCb290c3RhcnRGaWx0ZXJNb2R1bGUge1xufVxuIl19