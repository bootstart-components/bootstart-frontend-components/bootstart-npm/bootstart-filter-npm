/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { EsBucket } from './es-bucket';
export class EsBucketAggregation {
    constructor() {
        /**
         * List of buckets
         */
        this.buckets = [];
    }
    /**
     * @param {?} aggregation
     * @return {?}
     */
    initAggregation(aggregation) {
        this.buckets = aggregation.buckets.map(item => {
            /** @type {?} */
            const result = new EsBucket();
            result.initBucket(item);
            return result;
        });
    }
    /**
     * @param {?} aggregation
     * @return {?}
     */
    updateAggregationCounts(aggregation) {
        this.buckets.forEach(bucket => {
            /** @type {?} */
            const maxDocCount = Math.max.apply(null, aggregation.buckets.map(item => item.count));
            /** @type {?} */
            const matching = aggregation.buckets.filter(item => bucket.key === item.key);
            if (matching.length <= 1) {
                bucket.updateBucketCounts(matching[0], maxDocCount);
            }
            else {
                console.error('Multiple Elasticsearch buckets found!');
            }
        });
    }
}
if (false) {
    /**
     * List of buckets
     * @type {?}
     */
    EsBucketAggregation.prototype.buckets;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXMtYnVja2V0LWFnZ3JlZ2F0aW9uLmpzIiwic291cmNlUm9vdCI6Im5nOi8vYm9vdHN0YXJ0LWZpbHRlci8iLCJzb3VyY2VzIjpbImxpYi9tb2RlbHMvYWdncmVnYXRpb24vYnVja2V0L2VzLWJ1Y2tldC1hZ2dyZWdhdGlvbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLFFBQVEsRUFBQyxNQUFNLGFBQWEsQ0FBQztBQUlyQyxNQUFNOzs7Ozt1QkFHa0IsRUFBRTs7Ozs7O0lBR3hCLGVBQWUsQ0FBQyxXQUFnQztRQUM5QyxJQUFJLENBQUMsT0FBTyxHQUFHLFdBQVcsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFOztZQUM1QyxNQUFNLE1BQU0sR0FBRyxJQUFJLFFBQVEsRUFBRSxDQUFDO1lBQzlCLE1BQU0sQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDeEIsTUFBTSxDQUFDLE1BQU0sQ0FBQztTQUNmLENBQUMsQ0FBQztLQUNKOzs7OztJQUVELHVCQUF1QixDQUFDLFdBQWdDO1FBQ3RELElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFOztZQUM1QixNQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsV0FBVyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQzs7WUFDdEYsTUFBTSxRQUFRLEdBQUcsV0FBVyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsR0FBRyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUM3RSxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3pCLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLEVBQUUsV0FBVyxDQUFDLENBQUM7YUFDckQ7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDTixPQUFPLENBQUMsS0FBSyxDQUFDLHVDQUF1QyxDQUFDLENBQUM7YUFDeEQ7U0FDRixDQUFDLENBQUM7S0FDSjtDQUVGIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtFc0J1Y2tldH0gZnJvbSAnLi9lcy1idWNrZXQnO1xuaW1wb3J0IHtFc0FnZ3JlZ2F0aW9ufSBmcm9tICcuLi9lcy1hZ2dyZWdhdGlvbic7XG5pbXBvcnQge2F9IGZyb20gJ0Bhbmd1bGFyL2NvcmUvc3JjL3JlbmRlcjMnO1xuXG5leHBvcnQgY2xhc3MgRXNCdWNrZXRBZ2dyZWdhdGlvbiBpbXBsZW1lbnRzIEVzQWdncmVnYXRpb24ge1xuXG4gIC8qKiBMaXN0IG9mIGJ1Y2tldHMgKi9cbiAgYnVja2V0czogRXNCdWNrZXRbXSA9IFtdO1xuXG5cbiAgaW5pdEFnZ3JlZ2F0aW9uKGFnZ3JlZ2F0aW9uOiBFc0J1Y2tldEFnZ3JlZ2F0aW9uKTogdm9pZCB7XG4gICAgdGhpcy5idWNrZXRzID0gYWdncmVnYXRpb24uYnVja2V0cy5tYXAoaXRlbSA9PiB7XG4gICAgICBjb25zdCByZXN1bHQgPSBuZXcgRXNCdWNrZXQoKTtcbiAgICAgIHJlc3VsdC5pbml0QnVja2V0KGl0ZW0pO1xuICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICB9KTtcbiAgfVxuXG4gIHVwZGF0ZUFnZ3JlZ2F0aW9uQ291bnRzKGFnZ3JlZ2F0aW9uOiBFc0J1Y2tldEFnZ3JlZ2F0aW9uKTogdm9pZCB7XG4gICAgdGhpcy5idWNrZXRzLmZvckVhY2goYnVja2V0ID0+IHtcbiAgICAgIGNvbnN0IG1heERvY0NvdW50ID0gTWF0aC5tYXguYXBwbHkobnVsbCwgYWdncmVnYXRpb24uYnVja2V0cy5tYXAoaXRlbSA9PiBpdGVtLmNvdW50KSk7XG4gICAgICBjb25zdCBtYXRjaGluZyA9IGFnZ3JlZ2F0aW9uLmJ1Y2tldHMuZmlsdGVyKGl0ZW0gPT4gYnVja2V0LmtleSA9PT0gaXRlbS5rZXkpO1xuICAgICAgaWYgKG1hdGNoaW5nLmxlbmd0aCA8PSAxKSB7XG4gICAgICAgIGJ1Y2tldC51cGRhdGVCdWNrZXRDb3VudHMobWF0Y2hpbmdbMF0sIG1heERvY0NvdW50KTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGNvbnNvbGUuZXJyb3IoJ011bHRpcGxlIEVsYXN0aWNzZWFyY2ggYnVja2V0cyBmb3VuZCEnKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG59XG4iXX0=