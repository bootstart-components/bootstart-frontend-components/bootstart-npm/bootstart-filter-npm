import { DoCheck, EventEmitter } from '@angular/core';
import { EsBucket, EsBucketAggregation } from '../../models';
import { Options } from 'ng5-slider';
import { HistogramIntervalData } from '../../models/utils/histogram-interval-data';
import { Interval } from '../../models/utils/interval';
import { BehaviorSubject } from 'rxjs';
export declare class BootstartHistogramSliderFilterComponent implements DoCheck {
    bucketAggregation: EsBucketAggregation;
    minRange: number;
    breakpoints: number[];
    breakpointFiniteMinValue: number;
    breakpointFiniteMaxValue: number;
    lowerBounded: boolean;
    upperBounded: boolean;
    histogramData: HistogramIntervalData[];
    minSelected: number;
    maxSelected: number;
    sliderOptions: Options;
    sliderInitialized: boolean;
    dataSource: EsBucket[];
    dataChange: BehaviorSubject<EsBucket[]>;
    rangeChange: EventEmitter<Interval>;
    constructor();
    ngDoCheck(): void;
    /**
     * Emits range event when value changes.
     * If min or max is infinite, returns null.
     */
    valueChange(): void;
    /** Initialization from bucket aggregation */
    private _init();
    /** Adds value to breakpoints array.
     *  Checks first if value is already registered.*/
    private _addBreakpointValue(value);
}
