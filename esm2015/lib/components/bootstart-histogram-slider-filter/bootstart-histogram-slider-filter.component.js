/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { EsBucketAggregation } from '../../models';
import { Options } from 'ng5-slider';
import { Interval } from '../../models/utils/interval';
import { BehaviorSubject } from 'rxjs';
export class BootstartHistogramSliderFilterComponent {
    constructor() {
        this.minRange = 1;
        this.dataChange = new BehaviorSubject([]);
        this.breakpoints = [];
        this.lowerBounded = true;
        this.upperBounded = true;
        this.histogramData = [];
        this.sliderOptions = new Options();
        this.sliderInitialized = false;
        this.rangeChange = new EventEmitter();
        this.dataSource = [];
        this.dataChange.subscribe(data => {
            if (!this.sliderInitialized) {
                this.dataSource = data;
                this._init();
            }
        });
    }
    /**
     * @return {?}
     */
    ngDoCheck() {
        this.dataChange.next(this.bucketAggregation.buckets);
    }
    /**
     * Emits range event when value changes.
     * If min or max is infinite, returns null.
     * @return {?}
     */
    valueChange() {
        /** @type {?} */
        const interval = new Interval();
        interval.min = (!this.lowerBounded && this.minSelected === this.sliderOptions.floor) ? null : this.breakpoints[this.minSelected];
        interval.max = (!this.upperBounded && this.maxSelected === this.sliderOptions.ceil) ? null : this.breakpoints[this.maxSelected];
        this.rangeChange.emit(interval);
    }
    /**
     * Initialization from bucket aggregation
     * @return {?}
     */
    _init() {
        if (this.dataSource.length > 0) {
            for (let i = 0; i < this.dataSource.length; i++) {
                /** @type {?} */
                const bucket = this.dataSource[i];
                /** @type {?} */
                const bucketRangeValues = bucket.key.split('-');
                if (bucketRangeValues.length === 1) {
                    // Aggregation is an histogram
                    this._addBreakpointValue(-Infinity);
                    this._addBreakpointValue(+bucketRangeValues[0]);
                    this.lowerBounded = false;
                }
                else {
                    // Aggregation is a list of ranges
                    if (bucketRangeValues[0] === '*') {
                        this._addBreakpointValue(-Infinity);
                        this._addBreakpointValue(+bucketRangeValues[1]);
                        this.lowerBounded = false;
                    }
                    else if (bucketRangeValues[1] === '*') {
                        this._addBreakpointValue(+bucketRangeValues[0]);
                        this._addBreakpointValue(Infinity);
                        this.upperBounded = false;
                    }
                    else {
                        this._addBreakpointValue(+bucketRangeValues[0]);
                        this._addBreakpointValue(+bucketRangeValues[1]);
                    }
                }
                /** @type {?} */
                const breakpointsFiniteValues = this.breakpoints.filter(item => item !== -Infinity && item !== Infinity);
                this.breakpointFiniteMinValue = Math.min.apply(null, breakpointsFiniteValues);
                this.breakpointFiniteMaxValue = Math.max.apply(null, breakpointsFiniteValues);
                this.histogramData.push({
                    x: i,
                    y: bucket.count
                });
            }
            this.minSelected = 0;
            this.sliderOptions.floor = 0;
            this.maxSelected = this.histogramData.length;
            this.sliderOptions.ceil = this.histogramData.length;
            this.sliderOptions.translate = (value, label) => {
                if (this.breakpoints[value] === -Infinity) {
                    return '< ' + this.breakpointFiniteMinValue;
                }
                if (this.breakpoints[value] === Infinity) {
                    return '> ' + this.breakpointFiniteMaxValue;
                }
                return this.breakpoints[value] + '';
            };
            // Slider can only be initialized once.
            this.sliderInitialized = true;
        }
        this.sliderOptions.minRange = this.minRange;
        this.sliderOptions.animate = false;
        this.sliderOptions.noSwitching = true;
        this.sliderOptions.pushRange = true;
        this.sliderOptions.hideLimitLabels = true;
    }
    /**
     * Adds value to breakpoints array.
     *  Checks first if value is already registered.
     * @param {?} value
     * @return {?}
     */
    _addBreakpointValue(value) {
        if (this.breakpoints.indexOf(value) === -1) {
            this.breakpoints.push(value);
        }
    }
}
BootstartHistogramSliderFilterComponent.decorators = [
    { type: Component, args: [{
                selector: 'bootstart-histogram-slider-filter',
                template: `<div class="bootstart-range-slider" *ngIf="sliderInitialized">
  <ng5-slider [(value)]="minSelected"
              [(highValue)]="maxSelected"
              [options]="sliderOptions"
              (valueChange)="valueChange()"
              (highValueChange)="valueChange()"></ng5-slider>
</div>
`,
                styles: [``]
            },] },
];
/** @nocollapse */
BootstartHistogramSliderFilterComponent.ctorParameters = () => [];
BootstartHistogramSliderFilterComponent.propDecorators = {
    bucketAggregation: [{ type: Input }],
    minRange: [{ type: Input }],
    rangeChange: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    BootstartHistogramSliderFilterComponent.prototype.bucketAggregation;
    /** @type {?} */
    BootstartHistogramSliderFilterComponent.prototype.minRange;
    /** @type {?} */
    BootstartHistogramSliderFilterComponent.prototype.breakpoints;
    /** @type {?} */
    BootstartHistogramSliderFilterComponent.prototype.breakpointFiniteMinValue;
    /** @type {?} */
    BootstartHistogramSliderFilterComponent.prototype.breakpointFiniteMaxValue;
    /** @type {?} */
    BootstartHistogramSliderFilterComponent.prototype.lowerBounded;
    /** @type {?} */
    BootstartHistogramSliderFilterComponent.prototype.upperBounded;
    /** @type {?} */
    BootstartHistogramSliderFilterComponent.prototype.histogramData;
    /** @type {?} */
    BootstartHistogramSliderFilterComponent.prototype.minSelected;
    /** @type {?} */
    BootstartHistogramSliderFilterComponent.prototype.maxSelected;
    /** @type {?} */
    BootstartHistogramSliderFilterComponent.prototype.sliderOptions;
    /** @type {?} */
    BootstartHistogramSliderFilterComponent.prototype.sliderInitialized;
    /** @type {?} */
    BootstartHistogramSliderFilterComponent.prototype.dataSource;
    /** @type {?} */
    BootstartHistogramSliderFilterComponent.prototype.dataChange;
    /** @type {?} */
    BootstartHistogramSliderFilterComponent.prototype.rangeChange;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LWhpc3RvZ3JhbS1zbGlkZXItZmlsdGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Jvb3RzdGFydC1maWx0ZXIvIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9ib290c3RhcnQtaGlzdG9ncmFtLXNsaWRlci1maWx0ZXIvYm9vdHN0YXJ0LWhpc3RvZ3JhbS1zbGlkZXItZmlsdGVyLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLFNBQVMsRUFBVyxZQUFZLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUM5RSxPQUFPLEVBQVcsbUJBQW1CLEVBQUMsTUFBTSxjQUFjLENBQUM7QUFDM0QsT0FBTyxFQUFZLE9BQU8sRUFBQyxNQUFNLFlBQVksQ0FBQztBQUU5QyxPQUFPLEVBQUMsUUFBUSxFQUFDLE1BQU0sNkJBQTZCLENBQUM7QUFDckQsT0FBTyxFQUFDLGVBQWUsRUFBQyxNQUFNLE1BQU0sQ0FBQztBQWNyQyxNQUFNO0lBc0JKO3dCQW5CNEIsQ0FBQzswQkFjYSxJQUFJLGVBQWUsQ0FBYSxFQUFFLENBQUM7UUFNM0UsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7UUFDdEIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7UUFDekIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7UUFDekIsSUFBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7UUFDeEIsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO1FBQ25DLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUM7UUFDL0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBRXRDLElBQUksQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQy9CLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQztnQkFDNUIsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7Z0JBQ3ZCLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQzthQUNkO1NBQ0YsQ0FBQyxDQUFDO0tBQ0o7Ozs7SUFFRCxTQUFTO1FBQ1AsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxDQUFDO0tBQ3REOzs7Ozs7SUFPRCxXQUFXOztRQUNULE1BQU0sUUFBUSxHQUFHLElBQUksUUFBUSxFQUFFLENBQUM7UUFDaEMsUUFBUSxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUMsV0FBVyxLQUFLLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDakksUUFBUSxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUMsV0FBVyxLQUFLLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDaEksSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7S0FDakM7Ozs7O0lBR08sS0FBSztRQUNYLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDL0IsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDOztnQkFDaEQsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQzs7Z0JBQ2xDLE1BQU0saUJBQWlCLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBRWhELEVBQUUsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDOztvQkFFbkMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ3BDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ2hELElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO2lCQUMzQjtnQkFBQyxJQUFJLENBQUMsQ0FBQzs7b0JBRU4sRUFBRSxDQUFDLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQzt3QkFDakMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUMsUUFBUSxDQUFDLENBQUM7d0JBQ3BDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ2hELElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO3FCQUMzQjtvQkFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQzt3QkFDeEMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDaEQsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxDQUFDO3dCQUNuQyxJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztxQkFDM0I7b0JBQUMsSUFBSSxDQUFDLENBQUM7d0JBQ04sSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDaEQsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztxQkFDakQ7aUJBQ0Y7O2dCQUVELE1BQU0sdUJBQXVCLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLEtBQUssQ0FBQyxRQUFRLElBQUksSUFBSSxLQUFLLFFBQVEsQ0FBQyxDQUFDO2dCQUN6RyxJQUFJLENBQUMsd0JBQXdCLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLHVCQUF1QixDQUFDLENBQUM7Z0JBQzlFLElBQUksQ0FBQyx3QkFBd0IsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsdUJBQXVCLENBQUMsQ0FBQztnQkFDOUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUM7b0JBQ0UsQ0FBQyxFQUFFLENBQUM7b0JBQ0osQ0FBQyxFQUFFLE1BQU0sQ0FBQyxLQUFLO2lCQUNoQixDQUFDLENBQUM7YUFDNUI7WUFDRCxJQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQztZQUNyQixJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUM7WUFDN0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQztZQUM3QyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQztZQUNwRCxJQUFJLENBQUMsYUFBYSxDQUFDLFNBQVMsR0FBRyxDQUFDLEtBQWEsRUFBRSxLQUFnQixFQUFFLEVBQUU7Z0JBQ2pFLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO29CQUMxQyxNQUFNLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyx3QkFBd0IsQ0FBQztpQkFDN0M7Z0JBQ0QsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDO29CQUN6QyxNQUFNLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyx3QkFBd0IsQ0FBQztpQkFDN0M7Z0JBQ0QsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxDQUFDO2FBQ3JDLENBQUM7O1lBR0YsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQztTQUMvQjtRQUNELElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7UUFDNUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1FBQ25DLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztRQUN0QyxJQUFJLENBQUMsYUFBYSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7UUFDcEMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDOzs7Ozs7OztJQUtwQyxtQkFBbUIsQ0FBQyxLQUFhO1FBQ3ZDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMzQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUM5Qjs7OztZQXJJSixTQUFTLFNBQUM7Z0JBQ0UsUUFBUSxFQUFLLG1DQUFtQztnQkFDaEQsUUFBUSxFQUFFOzs7Ozs7O0NBT3RCO2dCQUNZLE1BQU0sRUFBRSxDQUFDLEVBQUUsQ0FBQzthQUNiOzs7OztnQ0FHVCxLQUFLO3VCQUNMLEtBQUs7MEJBZ0JMLE1BQU0iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgRG9DaGVjaywgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT3V0cHV0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7RXNCdWNrZXQsIEVzQnVja2V0QWdncmVnYXRpb259IGZyb20gJy4uLy4uL21vZGVscyc7XG5pbXBvcnQge0xhYmVsVHlwZSwgT3B0aW9uc30gZnJvbSAnbmc1LXNsaWRlcic7XG5pbXBvcnQge0hpc3RvZ3JhbUludGVydmFsRGF0YX0gZnJvbSAnLi4vLi4vbW9kZWxzL3V0aWxzL2hpc3RvZ3JhbS1pbnRlcnZhbC1kYXRhJztcbmltcG9ydCB7SW50ZXJ2YWx9IGZyb20gJy4uLy4uL21vZGVscy91dGlscy9pbnRlcnZhbCc7XG5pbXBvcnQge0JlaGF2aW9yU3ViamVjdH0gZnJvbSAncnhqcyc7XG5cbkBDb21wb25lbnQoe1xuICAgICAgICAgICAgIHNlbGVjdG9yICAgOiAnYm9vdHN0YXJ0LWhpc3RvZ3JhbS1zbGlkZXItZmlsdGVyJyxcbiAgICAgICAgICAgICB0ZW1wbGF0ZTogYDxkaXYgY2xhc3M9XCJib290c3RhcnQtcmFuZ2Utc2xpZGVyXCIgKm5nSWY9XCJzbGlkZXJJbml0aWFsaXplZFwiPlxuICA8bmc1LXNsaWRlciBbKHZhbHVlKV09XCJtaW5TZWxlY3RlZFwiXG4gICAgICAgICAgICAgIFsoaGlnaFZhbHVlKV09XCJtYXhTZWxlY3RlZFwiXG4gICAgICAgICAgICAgIFtvcHRpb25zXT1cInNsaWRlck9wdGlvbnNcIlxuICAgICAgICAgICAgICAodmFsdWVDaGFuZ2UpPVwidmFsdWVDaGFuZ2UoKVwiXG4gICAgICAgICAgICAgIChoaWdoVmFsdWVDaGFuZ2UpPVwidmFsdWVDaGFuZ2UoKVwiPjwvbmc1LXNsaWRlcj5cbjwvZGl2PlxuYCxcbiAgICAgICAgICAgICBzdHlsZXM6IFtgYF1cbiAgICAgICAgICAgfSlcbmV4cG9ydCBjbGFzcyBCb290c3RhcnRIaXN0b2dyYW1TbGlkZXJGaWx0ZXJDb21wb25lbnQgaW1wbGVtZW50cyBEb0NoZWNrIHtcblxuICBASW5wdXQoKSBidWNrZXRBZ2dyZWdhdGlvbjogRXNCdWNrZXRBZ2dyZWdhdGlvbjtcbiAgQElucHV0KCkgbWluUmFuZ2U6IG51bWJlciA9IDE7XG5cbiAgYnJlYWtwb2ludHM6IG51bWJlcltdO1xuICBicmVha3BvaW50RmluaXRlTWluVmFsdWU6IG51bWJlcjtcbiAgYnJlYWtwb2ludEZpbml0ZU1heFZhbHVlOiBudW1iZXI7XG4gIGxvd2VyQm91bmRlZDogYm9vbGVhbjtcbiAgdXBwZXJCb3VuZGVkOiBib29sZWFuO1xuICBoaXN0b2dyYW1EYXRhOiBIaXN0b2dyYW1JbnRlcnZhbERhdGFbXTtcbiAgbWluU2VsZWN0ZWQ6IG51bWJlcjtcbiAgbWF4U2VsZWN0ZWQ6IG51bWJlcjtcbiAgc2xpZGVyT3B0aW9uczogT3B0aW9ucztcbiAgc2xpZGVySW5pdGlhbGl6ZWQ6IGJvb2xlYW47XG5cbiAgZGF0YVNvdXJjZTogRXNCdWNrZXRbXTtcbiAgZGF0YUNoYW5nZTogQmVoYXZpb3JTdWJqZWN0PEVzQnVja2V0W10+ID0gbmV3IEJlaGF2aW9yU3ViamVjdDxFc0J1Y2tldFtdPihbXSk7XG5cbiAgQE91dHB1dCgpIHJhbmdlQ2hhbmdlOiBFdmVudEVtaXR0ZXI8SW50ZXJ2YWw+O1xuXG5cbiAgY29uc3RydWN0b3IoKSB7XG4gICAgdGhpcy5icmVha3BvaW50cyA9IFtdO1xuICAgIHRoaXMubG93ZXJCb3VuZGVkID0gdHJ1ZTtcbiAgICB0aGlzLnVwcGVyQm91bmRlZCA9IHRydWU7XG4gICAgdGhpcy5oaXN0b2dyYW1EYXRhID0gW107XG4gICAgdGhpcy5zbGlkZXJPcHRpb25zID0gbmV3IE9wdGlvbnMoKTtcbiAgICB0aGlzLnNsaWRlckluaXRpYWxpemVkID0gZmFsc2U7XG4gICAgdGhpcy5yYW5nZUNoYW5nZSA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcblxuICAgIHRoaXMuZGF0YVNvdXJjZSA9IFtdO1xuICAgIHRoaXMuZGF0YUNoYW5nZS5zdWJzY3JpYmUoZGF0YSA9PiB7XG4gICAgICBpZiAoIXRoaXMuc2xpZGVySW5pdGlhbGl6ZWQpIHtcbiAgICAgICAgdGhpcy5kYXRhU291cmNlID0gZGF0YTtcbiAgICAgICAgdGhpcy5faW5pdCgpO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgbmdEb0NoZWNrKCk6IHZvaWQge1xuICAgIHRoaXMuZGF0YUNoYW5nZS5uZXh0KHRoaXMuYnVja2V0QWdncmVnYXRpb24uYnVja2V0cyk7XG4gIH1cblxuXG4gIC8qKlxuICAgKiBFbWl0cyByYW5nZSBldmVudCB3aGVuIHZhbHVlIGNoYW5nZXMuXG4gICAqIElmIG1pbiBvciBtYXggaXMgaW5maW5pdGUsIHJldHVybnMgbnVsbC5cbiAgICovXG4gIHZhbHVlQ2hhbmdlKCk6IHZvaWQge1xuICAgIGNvbnN0IGludGVydmFsID0gbmV3IEludGVydmFsKCk7XG4gICAgaW50ZXJ2YWwubWluID0gKCF0aGlzLmxvd2VyQm91bmRlZCAmJiB0aGlzLm1pblNlbGVjdGVkID09PSB0aGlzLnNsaWRlck9wdGlvbnMuZmxvb3IpID8gbnVsbCA6IHRoaXMuYnJlYWtwb2ludHNbdGhpcy5taW5TZWxlY3RlZF07XG4gICAgaW50ZXJ2YWwubWF4ID0gKCF0aGlzLnVwcGVyQm91bmRlZCAmJiB0aGlzLm1heFNlbGVjdGVkID09PSB0aGlzLnNsaWRlck9wdGlvbnMuY2VpbCkgPyBudWxsIDogdGhpcy5icmVha3BvaW50c1t0aGlzLm1heFNlbGVjdGVkXTtcbiAgICB0aGlzLnJhbmdlQ2hhbmdlLmVtaXQoaW50ZXJ2YWwpO1xuICB9XG5cbiAgLyoqIEluaXRpYWxpemF0aW9uIGZyb20gYnVja2V0IGFnZ3JlZ2F0aW9uICovXG4gIHByaXZhdGUgX2luaXQoKTogdm9pZCB7XG4gICAgaWYgKHRoaXMuZGF0YVNvdXJjZS5sZW5ndGggPiAwKSB7XG4gICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMuZGF0YVNvdXJjZS5sZW5ndGg7IGkrKykge1xuICAgICAgICBjb25zdCBidWNrZXQgPSB0aGlzLmRhdGFTb3VyY2VbaV07XG4gICAgICAgIGNvbnN0IGJ1Y2tldFJhbmdlVmFsdWVzID0gYnVja2V0LmtleS5zcGxpdCgnLScpO1xuXG4gICAgICAgIGlmIChidWNrZXRSYW5nZVZhbHVlcy5sZW5ndGggPT09IDEpIHtcbiAgICAgICAgICAvLyBBZ2dyZWdhdGlvbiBpcyBhbiBoaXN0b2dyYW1cbiAgICAgICAgICB0aGlzLl9hZGRCcmVha3BvaW50VmFsdWUoLUluZmluaXR5KTtcbiAgICAgICAgICB0aGlzLl9hZGRCcmVha3BvaW50VmFsdWUoK2J1Y2tldFJhbmdlVmFsdWVzWzBdKTtcbiAgICAgICAgICB0aGlzLmxvd2VyQm91bmRlZCA9IGZhbHNlO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIC8vIEFnZ3JlZ2F0aW9uIGlzIGEgbGlzdCBvZiByYW5nZXNcbiAgICAgICAgICBpZiAoYnVja2V0UmFuZ2VWYWx1ZXNbMF0gPT09ICcqJykge1xuICAgICAgICAgICAgdGhpcy5fYWRkQnJlYWtwb2ludFZhbHVlKC1JbmZpbml0eSk7XG4gICAgICAgICAgICB0aGlzLl9hZGRCcmVha3BvaW50VmFsdWUoK2J1Y2tldFJhbmdlVmFsdWVzWzFdKTtcbiAgICAgICAgICAgIHRoaXMubG93ZXJCb3VuZGVkID0gZmFsc2U7XG4gICAgICAgICAgfSBlbHNlIGlmIChidWNrZXRSYW5nZVZhbHVlc1sxXSA9PT0gJyonKSB7XG4gICAgICAgICAgICB0aGlzLl9hZGRCcmVha3BvaW50VmFsdWUoK2J1Y2tldFJhbmdlVmFsdWVzWzBdKTtcbiAgICAgICAgICAgIHRoaXMuX2FkZEJyZWFrcG9pbnRWYWx1ZShJbmZpbml0eSk7XG4gICAgICAgICAgICB0aGlzLnVwcGVyQm91bmRlZCA9IGZhbHNlO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aGlzLl9hZGRCcmVha3BvaW50VmFsdWUoK2J1Y2tldFJhbmdlVmFsdWVzWzBdKTtcbiAgICAgICAgICAgIHRoaXMuX2FkZEJyZWFrcG9pbnRWYWx1ZSgrYnVja2V0UmFuZ2VWYWx1ZXNbMV0pO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIGNvbnN0IGJyZWFrcG9pbnRzRmluaXRlVmFsdWVzID0gdGhpcy5icmVha3BvaW50cy5maWx0ZXIoaXRlbSA9PiBpdGVtICE9PSAtSW5maW5pdHkgJiYgaXRlbSAhPT0gSW5maW5pdHkpO1xuICAgICAgICB0aGlzLmJyZWFrcG9pbnRGaW5pdGVNaW5WYWx1ZSA9IE1hdGgubWluLmFwcGx5KG51bGwsIGJyZWFrcG9pbnRzRmluaXRlVmFsdWVzKTtcbiAgICAgICAgdGhpcy5icmVha3BvaW50RmluaXRlTWF4VmFsdWUgPSBNYXRoLm1heC5hcHBseShudWxsLCBicmVha3BvaW50c0Zpbml0ZVZhbHVlcyk7XG4gICAgICAgIHRoaXMuaGlzdG9ncmFtRGF0YS5wdXNoKHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB4OiBpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHk6IGJ1Y2tldC5jb3VudFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgIH1cbiAgICAgIHRoaXMubWluU2VsZWN0ZWQgPSAwO1xuICAgICAgdGhpcy5zbGlkZXJPcHRpb25zLmZsb29yID0gMDtcbiAgICAgIHRoaXMubWF4U2VsZWN0ZWQgPSB0aGlzLmhpc3RvZ3JhbURhdGEubGVuZ3RoO1xuICAgICAgdGhpcy5zbGlkZXJPcHRpb25zLmNlaWwgPSB0aGlzLmhpc3RvZ3JhbURhdGEubGVuZ3RoO1xuICAgICAgdGhpcy5zbGlkZXJPcHRpb25zLnRyYW5zbGF0ZSA9ICh2YWx1ZTogbnVtYmVyLCBsYWJlbDogTGFiZWxUeXBlKSA9PiB7XG4gICAgICAgIGlmICh0aGlzLmJyZWFrcG9pbnRzW3ZhbHVlXSA9PT0gLUluZmluaXR5KSB7XG4gICAgICAgICAgcmV0dXJuICc8ICcgKyB0aGlzLmJyZWFrcG9pbnRGaW5pdGVNaW5WYWx1ZTtcbiAgICAgICAgfVxuICAgICAgICBpZiAodGhpcy5icmVha3BvaW50c1t2YWx1ZV0gPT09IEluZmluaXR5KSB7XG4gICAgICAgICAgcmV0dXJuICc+ICcgKyB0aGlzLmJyZWFrcG9pbnRGaW5pdGVNYXhWYWx1ZTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpcy5icmVha3BvaW50c1t2YWx1ZV0gKyAnJztcbiAgICAgIH07XG5cbiAgICAgIC8vIFNsaWRlciBjYW4gb25seSBiZSBpbml0aWFsaXplZCBvbmNlLlxuICAgICAgdGhpcy5zbGlkZXJJbml0aWFsaXplZCA9IHRydWU7XG4gICAgfVxuICAgIHRoaXMuc2xpZGVyT3B0aW9ucy5taW5SYW5nZSA9IHRoaXMubWluUmFuZ2U7XG4gICAgdGhpcy5zbGlkZXJPcHRpb25zLmFuaW1hdGUgPSBmYWxzZTtcbiAgICB0aGlzLnNsaWRlck9wdGlvbnMubm9Td2l0Y2hpbmcgPSB0cnVlO1xuICAgIHRoaXMuc2xpZGVyT3B0aW9ucy5wdXNoUmFuZ2UgPSB0cnVlO1xuICAgIHRoaXMuc2xpZGVyT3B0aW9ucy5oaWRlTGltaXRMYWJlbHMgPSB0cnVlO1xuICB9XG5cbiAgLyoqIEFkZHMgdmFsdWUgdG8gYnJlYWtwb2ludHMgYXJyYXkuXG4gICAqICBDaGVja3MgZmlyc3QgaWYgdmFsdWUgaXMgYWxyZWFkeSByZWdpc3RlcmVkLiovXG4gIHByaXZhdGUgX2FkZEJyZWFrcG9pbnRWYWx1ZSh2YWx1ZTogbnVtYmVyKTogdm9pZCB7XG4gICAgaWYgKHRoaXMuYnJlYWtwb2ludHMuaW5kZXhPZih2YWx1ZSkgPT09IC0xKSB7XG4gICAgICB0aGlzLmJyZWFrcG9pbnRzLnB1c2godmFsdWUpO1xuICAgIH1cbiAgfVxuXG5cbn1cblxuXG4iXX0=