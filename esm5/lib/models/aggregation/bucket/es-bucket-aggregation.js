/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { EsBucket } from './es-bucket';
var EsBucketAggregation = /** @class */ (function () {
    function EsBucketAggregation() {
        /**
         * List of buckets
         */
        this.buckets = [];
    }
    /**
     * @param {?} aggregation
     * @return {?}
     */
    EsBucketAggregation.prototype.initAggregation = /**
     * @param {?} aggregation
     * @return {?}
     */
    function (aggregation) {
        this.buckets = aggregation.buckets.map(function (item) {
            /** @type {?} */
            var result = new EsBucket();
            result.initBucket(item);
            return result;
        });
    };
    /**
     * @param {?} aggregation
     * @return {?}
     */
    EsBucketAggregation.prototype.updateAggregationCounts = /**
     * @param {?} aggregation
     * @return {?}
     */
    function (aggregation) {
        this.buckets.forEach(function (bucket) {
            /** @type {?} */
            var maxDocCount = Math.max.apply(null, aggregation.buckets.map(function (item) { return item.count; }));
            /** @type {?} */
            var matching = aggregation.buckets.filter(function (item) { return bucket.key === item.key; });
            if (matching.length <= 1) {
                bucket.updateBucketCounts(matching[0], maxDocCount);
            }
            else {
                console.error('Multiple Elasticsearch buckets found!');
            }
        });
    };
    return EsBucketAggregation;
}());
export { EsBucketAggregation };
if (false) {
    /**
     * List of buckets
     * @type {?}
     */
    EsBucketAggregation.prototype.buckets;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXMtYnVja2V0LWFnZ3JlZ2F0aW9uLmpzIiwic291cmNlUm9vdCI6Im5nOi8vYm9vdHN0YXJ0LWZpbHRlci8iLCJzb3VyY2VzIjpbImxpYi9tb2RlbHMvYWdncmVnYXRpb24vYnVja2V0L2VzLWJ1Y2tldC1hZ2dyZWdhdGlvbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLFFBQVEsRUFBQyxNQUFNLGFBQWEsQ0FBQztBQUlyQyxJQUFBOzs7Ozt1QkFHd0IsRUFBRTs7Ozs7O0lBR3hCLDZDQUFlOzs7O0lBQWYsVUFBZ0IsV0FBZ0M7UUFDOUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxXQUFXLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFBLElBQUk7O1lBQ3pDLElBQU0sTUFBTSxHQUFHLElBQUksUUFBUSxFQUFFLENBQUM7WUFDOUIsTUFBTSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN4QixNQUFNLENBQUMsTUFBTSxDQUFDO1NBQ2YsQ0FBQyxDQUFDO0tBQ0o7Ozs7O0lBRUQscURBQXVCOzs7O0lBQXZCLFVBQXdCLFdBQWdDO1FBQ3RELElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLFVBQUEsTUFBTTs7WUFDekIsSUFBTSxXQUFXLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLFdBQVcsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLEtBQUssRUFBVixDQUFVLENBQUMsQ0FBQyxDQUFDOztZQUN0RixJQUFNLFFBQVEsR0FBRyxXQUFXLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLE1BQU0sQ0FBQyxHQUFHLEtBQUssSUFBSSxDQUFDLEdBQUcsRUFBdkIsQ0FBdUIsQ0FBQyxDQUFDO1lBQzdFLEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDekIsTUFBTSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBRSxXQUFXLENBQUMsQ0FBQzthQUNyRDtZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNOLE9BQU8sQ0FBQyxLQUFLLENBQUMsdUNBQXVDLENBQUMsQ0FBQzthQUN4RDtTQUNGLENBQUMsQ0FBQztLQUNKOzhCQTVCSDtJQThCQyxDQUFBO0FBMUJELCtCQTBCQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7RXNCdWNrZXR9IGZyb20gJy4vZXMtYnVja2V0JztcbmltcG9ydCB7RXNBZ2dyZWdhdGlvbn0gZnJvbSAnLi4vZXMtYWdncmVnYXRpb24nO1xuaW1wb3J0IHthfSBmcm9tICdAYW5ndWxhci9jb3JlL3NyYy9yZW5kZXIzJztcblxuZXhwb3J0IGNsYXNzIEVzQnVja2V0QWdncmVnYXRpb24gaW1wbGVtZW50cyBFc0FnZ3JlZ2F0aW9uIHtcblxuICAvKiogTGlzdCBvZiBidWNrZXRzICovXG4gIGJ1Y2tldHM6IEVzQnVja2V0W10gPSBbXTtcblxuXG4gIGluaXRBZ2dyZWdhdGlvbihhZ2dyZWdhdGlvbjogRXNCdWNrZXRBZ2dyZWdhdGlvbik6IHZvaWQge1xuICAgIHRoaXMuYnVja2V0cyA9IGFnZ3JlZ2F0aW9uLmJ1Y2tldHMubWFwKGl0ZW0gPT4ge1xuICAgICAgY29uc3QgcmVzdWx0ID0gbmV3IEVzQnVja2V0KCk7XG4gICAgICByZXN1bHQuaW5pdEJ1Y2tldChpdGVtKTtcbiAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgfSk7XG4gIH1cblxuICB1cGRhdGVBZ2dyZWdhdGlvbkNvdW50cyhhZ2dyZWdhdGlvbjogRXNCdWNrZXRBZ2dyZWdhdGlvbik6IHZvaWQge1xuICAgIHRoaXMuYnVja2V0cy5mb3JFYWNoKGJ1Y2tldCA9PiB7XG4gICAgICBjb25zdCBtYXhEb2NDb3VudCA9IE1hdGgubWF4LmFwcGx5KG51bGwsIGFnZ3JlZ2F0aW9uLmJ1Y2tldHMubWFwKGl0ZW0gPT4gaXRlbS5jb3VudCkpO1xuICAgICAgY29uc3QgbWF0Y2hpbmcgPSBhZ2dyZWdhdGlvbi5idWNrZXRzLmZpbHRlcihpdGVtID0+IGJ1Y2tldC5rZXkgPT09IGl0ZW0ua2V5KTtcbiAgICAgIGlmIChtYXRjaGluZy5sZW5ndGggPD0gMSkge1xuICAgICAgICBidWNrZXQudXBkYXRlQnVja2V0Q291bnRzKG1hdGNoaW5nWzBdLCBtYXhEb2NDb3VudCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBjb25zb2xlLmVycm9yKCdNdWx0aXBsZSBFbGFzdGljc2VhcmNoIGJ1Y2tldHMgZm91bmQhJyk7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cblxufVxuIl19