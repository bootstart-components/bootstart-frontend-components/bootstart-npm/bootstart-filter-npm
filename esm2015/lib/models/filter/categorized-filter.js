/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * Categorized filter used to build Elasticsearch query.
 * The key is associated with a level and optional parents in case of optional parents.
 */
export class CategorizedFilter {
    /**
     * @param {?} esBucketFlatNode
     */
    constructor(esBucketFlatNode) {
        /**
         * Optional parents filters if this filter is a leaf.
         */
        this.parents = [];
        this.key = esBucketFlatNode.key;
        this.level = esBucketFlatNode.level;
    }
}
if (false) {
    /**
     * The key of the filter.
     * @type {?}
     */
    CategorizedFilter.prototype.key;
    /**
     * Level of the tree filter in checklist component.
     * @type {?}
     */
    CategorizedFilter.prototype.level;
    /**
     * Optional parents filters if this filter is a leaf.
     * @type {?}
     */
    CategorizedFilter.prototype.parents;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2F0ZWdvcml6ZWQtZmlsdGVyLmpzIiwic291cmNlUm9vdCI6Im5nOi8vYm9vdHN0YXJ0LWZpbHRlci8iLCJzb3VyY2VzIjpbImxpYi9tb2RlbHMvZmlsdGVyL2NhdGVnb3JpemVkLWZpbHRlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQU1BLE1BQU07Ozs7SUFVSixZQUFZLGdCQUFrQzs7Ozt1QkFGZixFQUFFO1FBRy9CLElBQUksQ0FBQyxHQUFHLEdBQUcsZ0JBQWdCLENBQUMsR0FBRyxDQUFDO1FBQ2hDLElBQUksQ0FBQyxLQUFLLEdBQUcsZ0JBQWdCLENBQUMsS0FBSyxDQUFDO0tBQ3JDO0NBQ0YiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0VzQnVja2V0RmxhdE5vZGV9IGZyb20gJy4uLy4uL21vZGVscy9hZ2dyZWdhdGlvbi9idWNrZXQvZXMtYnVja2V0LWZsYXQtbm9kZSc7XG5cbi8qKlxuICogQ2F0ZWdvcml6ZWQgZmlsdGVyIHVzZWQgdG8gYnVpbGQgRWxhc3RpY3NlYXJjaCBxdWVyeS5cbiAqIFRoZSBrZXkgaXMgYXNzb2NpYXRlZCB3aXRoIGEgbGV2ZWwgYW5kIG9wdGlvbmFsIHBhcmVudHMgaW4gY2FzZSBvZiBvcHRpb25hbCBwYXJlbnRzLlxuICovXG5leHBvcnQgY2xhc3MgQ2F0ZWdvcml6ZWRGaWx0ZXIge1xuICAvKiogVGhlIGtleSBvZiB0aGUgZmlsdGVyLiAqL1xuICBrZXk6IHN0cmluZztcblxuICAvKiogTGV2ZWwgb2YgdGhlIHRyZWUgZmlsdGVyIGluIGNoZWNrbGlzdCBjb21wb25lbnQuICovXG4gIGxldmVsOiBudW1iZXI7XG5cbiAgLyoqIE9wdGlvbmFsIHBhcmVudHMgZmlsdGVycyBpZiB0aGlzIGZpbHRlciBpcyBhIGxlYWYuICovXG4gIHBhcmVudHM6IENhdGVnb3JpemVkRmlsdGVyW10gPSBbXTtcblxuICBjb25zdHJ1Y3Rvcihlc0J1Y2tldEZsYXROb2RlOiBFc0J1Y2tldEZsYXROb2RlKSB7XG4gICAgdGhpcy5rZXkgPSBlc0J1Y2tldEZsYXROb2RlLmtleTtcbiAgICB0aGlzLmxldmVsID0gZXNCdWNrZXRGbGF0Tm9kZS5sZXZlbDtcbiAgfVxufVxuIl19