/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * Categorized filter used to build Elasticsearch query.
 * The key is associated with a level and optional parents in case of optional parents.
 */
var /**
 * Categorized filter used to build Elasticsearch query.
 * The key is associated with a level and optional parents in case of optional parents.
 */
CategorizedFilter = /** @class */ (function () {
    function CategorizedFilter(esBucketFlatNode) {
        /**
         * Optional parents filters if this filter is a leaf.
         */
        this.parents = [];
        this.key = esBucketFlatNode.key;
        this.level = esBucketFlatNode.level;
    }
    return CategorizedFilter;
}());
/**
 * Categorized filter used to build Elasticsearch query.
 * The key is associated with a level and optional parents in case of optional parents.
 */
export { CategorizedFilter };
if (false) {
    /**
     * The key of the filter.
     * @type {?}
     */
    CategorizedFilter.prototype.key;
    /**
     * Level of the tree filter in checklist component.
     * @type {?}
     */
    CategorizedFilter.prototype.level;
    /**
     * Optional parents filters if this filter is a leaf.
     * @type {?}
     */
    CategorizedFilter.prototype.parents;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2F0ZWdvcml6ZWQtZmlsdGVyLmpzIiwic291cmNlUm9vdCI6Im5nOi8vYm9vdHN0YXJ0LWZpbHRlci8iLCJzb3VyY2VzIjpbImxpYi9tb2RlbHMvZmlsdGVyL2NhdGVnb3JpemVkLWZpbHRlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQU1BOzs7O0FBQUE7SUFVRSwyQkFBWSxnQkFBa0M7Ozs7dUJBRmYsRUFBRTtRQUcvQixJQUFJLENBQUMsR0FBRyxHQUFHLGdCQUFnQixDQUFDLEdBQUcsQ0FBQztRQUNoQyxJQUFJLENBQUMsS0FBSyxHQUFHLGdCQUFnQixDQUFDLEtBQUssQ0FBQztLQUNyQzs0QkFuQkg7SUFvQkMsQ0FBQTs7Ozs7QUFkRCw2QkFjQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7RXNCdWNrZXRGbGF0Tm9kZX0gZnJvbSAnLi4vLi4vbW9kZWxzL2FnZ3JlZ2F0aW9uL2J1Y2tldC9lcy1idWNrZXQtZmxhdC1ub2RlJztcblxuLyoqXG4gKiBDYXRlZ29yaXplZCBmaWx0ZXIgdXNlZCB0byBidWlsZCBFbGFzdGljc2VhcmNoIHF1ZXJ5LlxuICogVGhlIGtleSBpcyBhc3NvY2lhdGVkIHdpdGggYSBsZXZlbCBhbmQgb3B0aW9uYWwgcGFyZW50cyBpbiBjYXNlIG9mIG9wdGlvbmFsIHBhcmVudHMuXG4gKi9cbmV4cG9ydCBjbGFzcyBDYXRlZ29yaXplZEZpbHRlciB7XG4gIC8qKiBUaGUga2V5IG9mIHRoZSBmaWx0ZXIuICovXG4gIGtleTogc3RyaW5nO1xuXG4gIC8qKiBMZXZlbCBvZiB0aGUgdHJlZSBmaWx0ZXIgaW4gY2hlY2tsaXN0IGNvbXBvbmVudC4gKi9cbiAgbGV2ZWw6IG51bWJlcjtcblxuICAvKiogT3B0aW9uYWwgcGFyZW50cyBmaWx0ZXJzIGlmIHRoaXMgZmlsdGVyIGlzIGEgbGVhZi4gKi9cbiAgcGFyZW50czogQ2F0ZWdvcml6ZWRGaWx0ZXJbXSA9IFtdO1xuXG4gIGNvbnN0cnVjdG9yKGVzQnVja2V0RmxhdE5vZGU6IEVzQnVja2V0RmxhdE5vZGUpIHtcbiAgICB0aGlzLmtleSA9IGVzQnVja2V0RmxhdE5vZGUua2V5O1xuICAgIHRoaXMubGV2ZWwgPSBlc0J1Y2tldEZsYXROb2RlLmxldmVsO1xuICB9XG59XG4iXX0=