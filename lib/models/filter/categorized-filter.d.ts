import { EsBucketFlatNode } from '../../models/aggregation/bucket/es-bucket-flat-node';
/**
 * Categorized filter used to build Elasticsearch query.
 * The key is associated with a level and optional parents in case of optional parents.
 */
export declare class CategorizedFilter {
    /** The key of the filter. */
    key: string;
    /** Level of the tree filter in checklist component. */
    level: number;
    /** Optional parents filters if this filter is a leaf. */
    parents: CategorizedFilter[];
    constructor(esBucketFlatNode: EsBucketFlatNode);
}
