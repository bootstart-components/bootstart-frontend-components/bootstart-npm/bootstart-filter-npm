/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function EsAggregation() { }
/**
 * Initialize the aggregation given a backend response.
 * Used to keep methods of the aggregation class.
 * \@param aggregation Aggregation obtained in backend response.
 * @type {?}
 */
EsAggregation.prototype.initAggregation;
/**
 * Update of aggregation counts given a backend response.
 * Used to keep methods of the aggregation class.
 * \@param aggregation Aggregation obtained in backend response.
 * @type {?}
 */
EsAggregation.prototype.updateAggregationCounts;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXMtYWdncmVnYXRpb24uanMiLCJzb3VyY2VSb290Ijoibmc6Ly9ib290c3RhcnQtZmlsdGVyLyIsInNvdXJjZXMiOlsibGliL21vZGVscy9hZ2dyZWdhdGlvbi9lcy1hZ2dyZWdhdGlvbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGludGVyZmFjZSBFc0FnZ3JlZ2F0aW9uIHtcblxuICAvKipcbiAgICogSW5pdGlhbGl6ZSB0aGUgYWdncmVnYXRpb24gZ2l2ZW4gYSBiYWNrZW5kIHJlc3BvbnNlLlxuICAgKiBVc2VkIHRvIGtlZXAgbWV0aG9kcyBvZiB0aGUgYWdncmVnYXRpb24gY2xhc3MuXG4gICAqIEBwYXJhbSBhZ2dyZWdhdGlvbiBBZ2dyZWdhdGlvbiBvYnRhaW5lZCBpbiBiYWNrZW5kIHJlc3BvbnNlLlxuICAgKi9cbiAgaW5pdEFnZ3JlZ2F0aW9uKGFnZ3JlZ2F0aW9uOiBFc0FnZ3JlZ2F0aW9uKTogdm9pZDtcblxuXG4gIC8qKlxuICAgKiBVcGRhdGUgb2YgYWdncmVnYXRpb24gY291bnRzIGdpdmVuIGEgYmFja2VuZCByZXNwb25zZS5cbiAgICogVXNlZCB0byBrZWVwIG1ldGhvZHMgb2YgdGhlIGFnZ3JlZ2F0aW9uIGNsYXNzLlxuICAgKiBAcGFyYW0gYWdncmVnYXRpb24gQWdncmVnYXRpb24gb2J0YWluZWQgaW4gYmFja2VuZCByZXNwb25zZS5cbiAgICovXG4gIHVwZGF0ZUFnZ3JlZ2F0aW9uQ291bnRzKGFnZ3JlZ2F0aW9uOiBFc0FnZ3JlZ2F0aW9uKTogdm9pZDtcblxufVxuIl19