export interface EsAggregation {
    /**
     * Initialize the aggregation given a backend response.
     * Used to keep methods of the aggregation class.
     * @param aggregation Aggregation obtained in backend response.
     */
    initAggregation(aggregation: EsAggregation): void;
    /**
     * Update of aggregation counts given a backend response.
     * Used to keep methods of the aggregation class.
     * @param aggregation Aggregation obtained in backend response.
     */
    updateAggregationCounts(aggregation: EsAggregation): void;
}
